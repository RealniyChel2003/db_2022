from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'3e828368c487bc4a5700892f9aa5a5dacc878f73f8a24887a08c061e9986e4c457d117ae5144f56bffedc487bd66ae62',
            'be2136fab283f88d1efa5acb29d786fdc7dd6ac129af0e137e1d87be86f2c9fad7faa05b16c7aeeffcfda95c9b56195f'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)
        sha3_384_hash = hashlib.sha3_384(password.encode('utf-8')).hexdigest()

        if sha3_384_hash in hashes:
            print(f'password = {password}, hash = {sha3_384_hash}')
            hashes.remove(sha3_384_hash)

        if len(hashes) == 0:
            break
