from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '67560873de09d4e2f8ca9e093db171e1853b4d6d11c69f490d06517fb38386e1c62477dfb3eadb033334e7777b61a52a'
salt = 'e137ae03-8e35-44eb-9b1c-45cf317beca7'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha3_384((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
