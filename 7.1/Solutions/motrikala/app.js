require('dotenv').config()
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()

const redis = require('redis')
const db = require('./queries')
const PORT = process.env.APP_PORT

const client = redis.createClient({
    url: `redis://@${process.env.REDIS_HOST ?? '127.0.0.1'}:${process.env.REDIS_PORT}`
})

client.on('connect', function() {
    console.log('Connected!');
  });

client.connect();

app.use(cors())

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'Project' })
})

app.get('/array', async (req, res) => {
    const { sub_direction } = req.body
    const result = await db.searchArray(sub_direction)
    res.json(result)
})

app.get('/jsonb', async (req, res) => {
    const { date_award } = req.body
    const result = await db.searchJsonb(date_award)
    res.json(result)
})

app.get('/materializated', async (req, res) => {
    const result = await db.materializated()
    res.json(result)
})

app.get('/search3', async (req, res) => {
  const { genre, language_book, date_of_publication } = req.body
  const result = await db.search3(genre, language_book, date_of_publication)
  res.json(result)
})

app.get('/search2', async (req, res) => {
  const { direction, language_book } = req.body
  const result = await db.search2(direction, language_book)
  res.json(result)
})

app.get('/search_all', async (req, res) => {
  const result = await db.searchAll()
  res.json(result)
})

app.get('/books', async (req, res) => {
    const result = await db.getBook()
    res.json(result)
})

app.get('/authors', async (req, res) => {
    const result = await ms.getAuthor()
    res.json(result)
})

app.get('/publishers', async (req, res) => {
    const result = await db.getPublisher()
    res.json(result)
})

app.get('/book/:id', async (req, res) => {
    const id = parseInt(req.params.id)

    const getRedis = await client.get(String(id))

    if (getRedis === null) {
        const result = await db.getBookById(id)
        await client.set(String(id), JSON.stringify(result), {
            EX: 100
        })
        res.json(result)
    }
    res.json(JSON.parse(getRedis))
})

app.get('/author/:id', async (req, res) => {
    const id = parseInt(req.params.id)

    const getRedis = await client.get(String(id))

    if (getRedis === null) {
        const result = await db.getAuthorById(id)
        await client.set(String(id), JSON.stringify(result), {
            EX: 100
        })
        res.json(result)
    }
    res.json(JSON.parse(getRedis))
})

app.post('/create/book', async (req, res) => {
    const { name, genre, date_of_publication, language_book, publisher_id } = req.body
    const id = await db.createBook(name, genre, date_of_publication, language_book, publisher_id)
    res.json({id})
})

app.post('/create/author', async (req, res) => {
    const { name, nickname, year_of_birth, place_of_birth, direction } = req.body
    const result = await db.createAuthor(name, nickname, year_of_birth, place_of_birth, direction)
    res.json(result)
})

app.post('/update/book/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const { name, genre, date_of_publication, language_book, publisher_id } = req.body
    const result = await db.updateBook(name, genre, date_of_publication, language_book, publisher_id, id)
    res.json(result)
})

app.post('/update/author/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const { name, nickname, year_of_birth, place_of_birth, direction } = req.body
    const result = await db.updateAuthor(name, nickname, year_of_birth, place_of_birth, direction, id)
    res.json(result)
})

app.delete('/delete/book/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.deleteBook(id)
    res.json(result)
})

app.delete('/delete/author/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.deleteAuthor(id)
    res.json(result)
})

app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}`)
})
