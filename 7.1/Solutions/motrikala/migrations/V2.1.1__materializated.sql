create 
materialized view view_author_to_book as
select id_author, id_book, a.id as author_id, a.name as name_author, 
	nickname, year_of_birth, place_of_birth, direction, 
	b.id as book_id, b.name as name_book, genre, 
	date_of_publication, language_book, publisher_id 
from author_to_book as atb
	join author as a on a.id = atb.id_author
	join book as b on b.id = atb.id_book;

create unique index view_author_to_book_id on view_author_to_book (author_id, book_id);

