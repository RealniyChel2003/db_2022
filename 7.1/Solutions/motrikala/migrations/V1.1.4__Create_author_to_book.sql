create table author_to_book
(
    id_author int references author not null,
    id_book int references book not null,
    unique (id_author, id_book)
);
