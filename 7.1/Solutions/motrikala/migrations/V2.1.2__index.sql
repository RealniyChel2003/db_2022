create index search_view_atb on view_author_to_book using btree(direction, language_book);

create index search_book on book using btree(genre, language_book, date_of_publication);