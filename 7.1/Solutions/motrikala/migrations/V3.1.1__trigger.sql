create function refresh_author_to_book()
    returns trigger as
$$
begin
    refresh materialized view concurrently view_author_to_book;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_atb_table
    after insert or update or delete
    on author
    for each row
execute function refresh_author_to_book();