create table publisher
(
    id int generated always as identity primary key,
    name text not null,
    country text not null,
    year_of_foundation int
);