create extension pg_cron;

select cron.schedule('refresh_author_to_book', '* * * * *',
                     $$ refresh materialized view concurrently view_author_to_book $$);
