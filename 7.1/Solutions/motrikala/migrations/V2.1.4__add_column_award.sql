alter table author add column award jsonb;

update author set award=
('[
	{
		"title": "Национальная медаль по литературе",
		"date": 1973,
		"degree": "Лауреат"
	},
	{
		"title": "Премия «Ледяная игла»",
		"date": 2022,
		"degree": "Номинант"
	}
]')
where author.name = 'В.В.Набоков';

update author set award=
('[
	{
		"title": "World Fantasy Award",
		"date": 1977,
		"degree": "Лауреат"
	},
	{
		"title": "Balrog Awards",
		"date": 1979,
		"degree": "Лауреат"
	}
]')
where author.name = 'Р.Д.Брэдберри';

update author set award=
('[
	{
		"title": "James Tait Black Memorial Prize",
		"date": 1939,
		"degree": "Лауреат"
	},
	{
		"title": "American Academy of Arts and Letters Award of Merit",
		"date": 1959,
		"degree": "Номинант"
	}
]')
where author.name = 'О.Л.Хаксли';

create index author_award on author using gin (award);
