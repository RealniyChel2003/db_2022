create table book
(
    id int generated always as identity primary key,
    name text not null,
    genre text not null,
    date_of_publication int,
    language_book text,
    publisher_id int references publisher not null
);