alter table author add column sub_direction text[];

update author set sub_direction=('{сатира, биография}')
where author.name = 'В.В.Набоков';

update author set sub_direction=('{роман, повесть, рассказ}')
where author.name = 'Р.Д.Брэдберри';

update author set sub_direction=('{рассказ, новелла, роман, научная фантастика}')
where author.name = 'О.Л.Хаксли';

update author set sub_direction=('{повесть, роман, очерк, фельетон}')
where author.name = 'А.И.Куприн';

create index author_sub_direction on author using gin (sub_direction);
