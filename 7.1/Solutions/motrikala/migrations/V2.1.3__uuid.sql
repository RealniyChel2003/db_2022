begin;

create extension if not exists "uuid-ossp";

drop materialized view view_author_to_book;

alter table author_to_book drop constraint author_to_book_id_book_fkey;
alter table author_to_book rename column id_book to old_id_book;
alter table author_to_book add column id_book uuid;

alter table book drop constraint book_pkey;
alter table book rename column id to old_id;
alter table book add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from book
            loop
                update author_to_book set id_book = row.id where old_id_book = row.old_id;
            end loop;
    end
$$;

alter table book drop column old_id;
alter table book add primary key (id);

alter table author_to_book drop column old_id_book;
alter table author_to_book add constraint fk_book_id foreign key (id_book) references book;
alter table author_to_book alter column id_book set not null;
alter table author_to_book add constraint uq_id_book_to_id_author unique (id_book, id_author);

create
materialized view view_author_to_book as
select id_author, id_book, a.id as author_id, a.name as name_author,
	nickname, year_of_birth, place_of_birth, direction,
	b.id as book_id, b.name as name_book, genre,
	date_of_publication, language_book, publisher_id
from author_to_book as atb
	join author as a on a.id = atb.id_author
	join book as b on b.id = atb.id_book;

create unique index view_author_to_book_id on view_author_to_book (author_id, book_id);

create index search_view_atb on view_author_to_book using btree(direction, language_book);

commit;