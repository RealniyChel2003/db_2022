const Pool = require('pg').Pool
const pool = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST ?? 'localhost',
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT
})

const ms = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.SLAVE_HOST,
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.SLAVE_PORT
})

//GET ARRAY
async function searchArray(sub_direction) {
    try {
        const result = await pool.query(
            'select *from author where sub_direction && array [$1]',
            [sub_direction]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

//GET JSONB
async function searchJsonb(date_award) {
    try {
        const result = await pool.query(
            `select * from author where award @> '[{"date": ${JSON.stringify(date_award)}}]'`
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function materializated() {
    try {
        const result = await pool.query(
            'select * from view_author_to_book'
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

// GET SEARCH 3
async function search3(genre, language_book, date_of_publication) {
    try {
        const result = await pool.query(
            'SELECT * FROM book WHERE genre=$1 AND language_book=$2 AND date_of_publication=$3',
            [genre, language_book, date_of_publication]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

//GET SEARCH 2
async function search2(direction, language_book) {
    try {
        const result = await pool.query(
            'SELECT name_author, direction, name_book, genre, language_book FROM view_author_to_book \
            WHERE direction=$1 AND language_book=$2',
            [direction, language_book]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

//GET ALL SEARCH
async function searchAll() {
    try {
        const result = await pool.query(
            'SELECT * FROM book as b \
	            join author_to_book as atb on b.id = atb.id_book \
	            join author as a on a.id = atb.id_author \
	            join publisher as p on p.id = b.publisher_id'
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

// GET
const getBook = async () => {
    await pool.query('select pg_sleep(5)')
    try {
        const result = await pool.query('SELECT * FROM book')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

// REPLICA
const getAuthor = async () => {
    try {
        const result = await ms.query('SELECT * FROM author')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

const getPublisher = async () => {
    try {
        const result = await pool.query('SELECT * FROM publisher')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

// GET ID
const getBookById = async id => {
    try {
        const result = await pool.query('SELECT * FROM book WHERE id = $1', [id])
        return result.rows[0]
    } catch (error) {
        console.log(error)
    }
}

const getAuthorById = async id => {
    try {
        const result = await pool.query('SELECT * FROM author WHERE id = $1', [id])
        return result.rows[0]
    } catch (error) {
        console.log(error)
    }
}

// POST
async function createBook(name, genre, date_of_publication, language_book, publisher_id) {
    try {
        await pool.query('select pg_sleep(5)')
        const res = await pool.query(
            'INSERT INTO book (name, genre, date_of_publication, language_book, \
                publisher_id) VALUES ($1, $2, $3, $4, $5) RETURNING id',
            [name, genre, date_of_publication, language_book, publisher_id]
        )
        return res.rows[0].id
    } catch (error) {
        console.log(error)
    }
}

async function createAuthor(name, nickname, year_of_birth, place_of_birth, direction) {
    try {
        await pool.query(
            'INSERT INTO author (name, nickname, year_of_birth, place_of_birth, \
                 direction) VALUES ($1, $2, $3, $4, $5) RETURNING *',
            [name, nickname, year_of_birth, place_of_birth, direction]
        )
        return `Author create with name: ${name}`
    } catch (error) {
        console.log(error)
    }
}

// PUT
async function updateBook(name, genre, date_of_publication, language_book, publisher_id, id) {
    try {
        await pool.query(
            'UPDATE book SET name = $1, genre = $2, date_of_publication = $3, \
                language_book = $4, publisher_id = $5 WHERE id = $6',
            [name, genre, date_of_publication, language_book, publisher_id, id]
        )
        return `Book update with name: ${name}`
    } catch (error) {
        console.log(error)
    }
}

async function updateAuthor(name, nickname, year_of_birth, place_of_birth, direction, id) {
    try {
        await pool.query(
            'UPDATE author SET name = $1, nickname = $2, year_of_birth = $3, \
            place_of_birth = $4, direction = $5 WHERE id = $6',
            [name, nickname, year_of_birth, place_of_birth, direction, id]
        )
        return `Author update with name: ${name}`
    } catch (error) {
        console.log(error)
    }
}

//DELETE
const deleteBook = async id => {
    try {
        await pool.query('DELETE FROM book WHERE id = $1', [id])
        return `Book deleted with id: ${id}`
    } catch (error) {
        console.log(error)
    }
}

const deleteAuthor = async id => {
    try {
        await pool.query('DELETE FROM author WHERE id = $1', [id])
        return `Author deleted with id: ${id}`
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getBook,
    getAuthor,
    getBookById,
    getPublisher,
    getAuthorById,
    createBook,
    createAuthor,
    updateBook,
    updateAuthor,
    deleteBook,
    deleteAuthor,
    search3,
    search2,
    searchAll, 
    materializated,
    searchJsonb,
    searchArray
}
