from itertools import product
import hashlib
from Crypto.Hash import SHAKE256

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

def combinations_password(hashes, salt: str):
    
    pass_length = 0

    while len(hashes) > 0:
        pass_length += 1

        combinations = product(alphabet, repeat=pass_length)
    
        for combination in combinations:
            password = ''.join(combination)
                
            shake = SHAKE256.new()
            shake.update((password if salt == '' else password + salt).encode('utf-8'))
            hash = shake.read(32).hex()

            #shake = hashlib.shake_256(password.encode('utf-8')).hexdigest(32)

            if hash in hashes:
                print(f'password = {password}, hash = {shake}')
                hashes.remove(hash)

            if len(hashes) == 0:
                break
    return 'Выполнился'

# первое задание
# ответ: password = Pp6, hash = 08abc54bbb614232e662aa8ba4b55c4c943ca009d1c1590711676d8c5fa79b7e
#        password = Z!e, hash = a2246f0bd735831229274b08174235ca17bb6da27867b1627fd64b602c54902d

hashes = {'a2246f0bd735831229274b08174235ca17bb6da27867b1627fd64b602c54902d', '08abc54bbb614232e662aa8ba4b55c4c943ca009d1c1590711676d8c5fa79b7e'} 

print(combinations_password(hashes, ''))



# второе задание
# ответ: password = 9CCV, hash = <Crypto.Hash.SHAKE256.SHAKE256_XOF object at 0x7fd7812ee910>

hashes = {'76c5cc5983b866cc975f25a43795535b3cafb9884acf9eccd5d1f01099e5ee3f'}
salt = 'e6165e22-ce44-43f2-ab80-f0f5ef39c4a4'

print(combinations_password(hashes, salt))




