create extension pg_cron;

-- refresh представления каждую минуту:
select cron.schedule('car_and_buyer', '* * * * *',
                     $$ refresh materialized view concurrently car_and_buyer $$);

-- удаление задачи:
-- select cron.unschedule('car_and_buyer');
