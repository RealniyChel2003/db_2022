create function refresh_car_and_buyer()
    returns trigger as
$$
begin
    refresh materialized view concurrently car_and_buyer;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_car_table
    after insert or update or delete
    on car_to_buyer
    for each row
execute function refresh_car_and_buyer();