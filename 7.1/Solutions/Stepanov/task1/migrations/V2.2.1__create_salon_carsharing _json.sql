alter table salon_carsharing
	add jsonFormat jsonb;

insert into salon_carsharing(jsonFormat)
	values ('[
				{
					"title": "Салон ашота",
					"location": "Анапа",
					"category": "Русский автопром"
				},
				{
					"title": "Салон Максима",
					"location": "Сочи",
					"category": "Автоваз"
				}

			]');
		
create index salon_carsharing_jsonFormat on salon_carsharing using gin (jsonFormat);
