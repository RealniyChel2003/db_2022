begin;

create extension if not exists "uuid-ossp";

drop materialized view if exists car_and_buyer;
drop index if exists car_and_buyer_id;

alter table car_to_buyer 
    drop constraint car_to_buyer_buyer_id_fkey;
   
alter table car_to_buyer
    rename column buyer_id to old_buyer_id;
   
alter table car_to_buyer
    add column buyer_id uuid;
   
   

alter table buyer
    drop constraint buyer_pkey;
   
alter table buyer
    rename column id to old_id;

alter table buyer
    add column id uuid default uuid_generate_v4();


   
do
$$
    declare
        row record;
    begin
        for row in select * from buyer
            loop
                update car_to_buyer set buyer_id = row.id where old_buyer_id = row.old_id;
            end loop;
    end
$$;



alter table buyer
    drop column old_id;
   
alter table buyer
    add primary key (id);
   

   
alter table car_to_buyer
    drop column old_buyer_id;

alter table car_to_buyer
    add constraint fk_buyer_id foreign key (buyer_id) references buyer;
   
create materialized view car_and_buyer as
	select buyer_id, buyer_name, buyer_age, buyer_phone, car.car_id, car_name, vehicle_condition, price
		from buyer 
			join car_to_buyer on car_to_buyer.buyer_id = buyer.id
			join car on car.car_id = car_to_buyer.car_id;


create unique index car_and_buyer_id on car_and_buyer(buyer_id, car_id);

create index car_and_buyer_search_by_buyer_age_vehicle_condition on car_and_buyer
	using btree (buyer_age, vehicle_condition);
   
   
commit;