alter table salon_carsharing
	add review text[];


update salon_carsharing set review=('{хороший салон, салон нормальный, но есть минусы, плохой салон, ко мне приставали}') where salon_carsharing.id = 1;

update salon_carsharing set review=('{салон годный, в принципе пойдет, но есть минусы, плохой салон, есть минусы}') where salon_carsharing.id = 2;

create index salon_carsharing_review on salon_carsharing using gin (review);
