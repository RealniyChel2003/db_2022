# frozen_string_literal: true

class Config
  class << self
    def init
      data
    end

    # rubocop:disable Security/YAMLLoad
    def data
      @data ||= begin
        config_file = Rails.root.join('config/config.yml')

        if config_file.exist?
          yaml_text = ERB.new(IO.read(config_file)).result
          YAML.load(yaml_text)[Rails.env].with_indifferent_access
        else
          {}.with_indifferent_access
        end
      end
    end
    # rubocop:enable Security/YAMLLoad
  end
end
