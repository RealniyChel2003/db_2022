Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'index#index'

  # root 'sessions#home'

  get '/login', to: 'sessions#login'
  post '/login', to: 'sessions#create'
  post '/logout', to: 'sessions#destroy'
  get '/logout', to: 'sessions#destroy'

  resources :categories
  resources :products
  resources :users
  
  get '/profile', to: 'users#show'
  get '/sign_up', to: 'users#new'
  post '/cart', to: 'cart#add_product'
  get '/cart', to: 'cart#show'
  post '/order', to: 'orders#create'
  get '/orders/:id', to: 'orders#show'
  get :concurrent_refresh, to: "concurrent_refresh#index"
end
