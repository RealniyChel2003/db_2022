from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = 'ef68e89bfc2dbb09d30fa4dfe8754e60f46275b3'
salt = '51d5c853-4ce6-4204-af37-4218a1a38a00'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha1((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break

# password = 2P*#
