from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'e393087bf2aab81caefb6b4c5dd293001e81018e', '9cfc369387ee7ae21774d3498f5ad0848fc5caa6'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)
        md5hash = hashlib.sha1(password.encode('utf-8')).hexdigest()

        if md5hash in hashes:
            print(f'password = {password}, hash = {md5hash}')
            hashes.remove(md5hash)

        if len(hashes) == 0:
            break

# password = Rk, hash = 9cfc369387ee7ae21774d3498f5ad0848fc5caa6
# password = (Vy, hash = e393087bf2aab81caefb6b4c5dd293001e81018e
