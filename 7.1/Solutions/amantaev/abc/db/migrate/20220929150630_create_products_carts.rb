class CreateProductsCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :products_carts do |t|
      t.integer :amount, default: 1
      t.references :product, foreign_key: true
      t.references :cart, foreign_key: true

      t.timestamps
    end
  end
end
