class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :title
      t.text :subtitle
      t.float :price, default: 0.0
      t.float :volume
      t.integer :in_stock, default: 0
      t.boolean :is_active, default: true
      t.boolean :is_new, default: true
      t.string :image

      t.index ["is_active", "is_new", "price"], name: "index_products_on_is_active_and_is_new_and_price"
      t.index ["is_active", "in_stock"], name: "index_products_on_is_active_and_in_stock"

      t.timestamps
    end
  end
end
