class AddPerfumersToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :perfumers, :text, array: true
    add_index :products, :perfumers
  end
end
