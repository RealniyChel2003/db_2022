class CreateTopProducts < ActiveRecord::Migration[5.2]
  def change
    create_view :top_products, materialized: true

    add_index :top_products, [:is_active, :price]
    add_index :top_products, :id, unique: true
  end
end
