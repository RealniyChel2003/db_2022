class AddFeaturesToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :features, :jsonb
    add_index :products, :features
  end
end
