class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :public_id
      t.float :price
      t.text :comment
      t.string :status

      t.index ["created_at"], name: "index_orders_on_created_at"
      t.index ["public_id"], name: "index_orders_on_public_id"

      t.timestamps
    end
  end
end
