class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.references :product, foreign_key: { on_delete: :cascade }
      t.references :order, foreign_key: { on_delete: :cascade }
      t.integer :count

      t.timestamps
    end
  end
end
