begin;

-- переход на uuid будет у users

create extension if not exists "uuid-ossp";

alter table order_items
drop constraint order_items_user_id_fkey;

alter table order_items
    rename column user_id to old_user_id;

alter table order_items
    add column user_id uuid;


alter table carts
drop constraint carts_user_id_fkey;

alter table carts
    rename column user_id to old_user_id;

alter table carts
    add column user_id uuid;


alter table users
drop constraint users_pkey;

alter table users
    rename column id to old_id;

alter table users
    add column id uuid default uuid_generate_v4();

do
$$
    declare
row record;
begin
for row in select * from users
                             loop
update order_items set user_id = row.id where old_user_id = row.old_id;
update carts set user_id = row.id where old_user_id = row.old_id;
end loop;
end
$$;

alter table users
drop column old_id;
alter table users
    add primary key (id);

alter table order_items
drop column old_user_id;
alter table order_items
    add constraint fk_user_id foreign key (user_id) references users;
alter table order_items
    alter column user_id set not null;
alter table order_items
    add constraint uq_user_id_to_order_id unique (user_id, order_id);


alter table carts
drop column old_user_id;

alter table carts
    add constraint fk_user_id foreign key (user_id) references users;

commit;
