SELECT products.*
FROM products
         JOIN products_categories ON products.id = products_categories.product_id
         JOIN categories ON categories.id = products_categories.category_id
WHERE is_active = true
  AND is_new = true
  AND in_stock > 0
GROUP BY products.id
