class ConcurrentRefreshController < ApplicationController
  def index
    ActiveRecord::Base.connection.execute("REFRESH MATERIALIZED VIEW concurrently top_products")
  end
end
