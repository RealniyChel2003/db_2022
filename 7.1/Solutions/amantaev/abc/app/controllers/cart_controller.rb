class CartController < ApplicationController
  def add_product
    cart = @user.cart
    if (product_cart = cart.products_cart.where(product_id: params[:product_id]).first)
      product_cart.update(amount: product_cart.amount + 1)
    else
      @user.cart.products_cart.create(product: Product.find_by(id: params[:product_id]))
    end
  end

  def show
    if @user.blank?
      redirect_to '/sign_up', status: :see_other
    else
      @cart = @user.cart
    end
  end
end
