class SessionsController < ApplicationController
  def create
    if session[:user_id].present?
      redirect_to '/', status: :see_other
      return
    end

    @user = User.find_by(email: params[:email])

    if @user.present? && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to '/profile'
    else
      message = "Something went wrong!"
      redirect_to login_path, notice: message
    end
  end

  def login
    redirect_to '/', status: :see_other if session[:user_id].present?
  end

  def destroy
    session[:user_id] = nil

    redirect_to '/', status: :see_other
  end
end
