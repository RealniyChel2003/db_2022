class UsersController < ApplicationController
  def show
    if session[:user_id].blank?
      redirect_to controller: :sessions, action: :create
    else
      @user = User.find(session[:user_id])
    end
  end

  def new
    if session[:user_id].present?
      redirect_to '/', status: :see_other
    else
      @user = User.new
    end
  end

  def create
    @user = User.new(user_params)

    if @user.save
      session[:user_id] = @user.id
      redirect_to @user
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :mobile, :password)
  end
end
