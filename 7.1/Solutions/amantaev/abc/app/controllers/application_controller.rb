class ApplicationController < ActionController::Base
  before_action do
    @user ||= User.find_by(id: session[:user_id])
    if @user.nil?
      session[:user_id] = nil
    end
  end
end
