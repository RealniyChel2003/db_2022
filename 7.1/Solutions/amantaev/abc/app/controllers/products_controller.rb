class ProductsController < ApplicationController
  # http_basic_authenticate_with name: Config.data[:legacy][:user_name], password: Config.data[:legacy][:password], except: [:index, :show]

  def index
    @products = Product
                  .where(product_params)
                  .where('features @> ?', feature_params)

    @products = @products.where('perfumers && array[?]', params[:perfumer]) if params[:perfumer].present?
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_create_params)

    if @product.save
      redirect_to @product
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_create_params)
      redirect_to @product
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def product_params
    params.permit(:is_active, :is_new)
          .merge(volume_param)
          .merge(in_stock_param)
  end

  def volume_param
    return if params[:volume].nil?
    return if params[:volume].split(',').size > 2

    volume = params[:volume].split(',')
    {
      volume: volume.first..volume.last
    }
  end

  def in_stock_param
    return if params[:in_stock].nil?
    return if params[:in_stock].split(',').size > 2

    in_stock = params[:in_stock].split(',')
    if params[:in_stock].last == ','
      return { in_stock: in_stock.first.to_i..Float::INFINITY }
    else
      return { in_stock: 0..in_stock.first }
    end
    {
      in_stock: in_stock.first..in_stock.last
    }
  end

  def feature_params
    par = []
    par << {
      name: 'Пол',
      value: params[:gender]
    } if params[:gender].present?
    par << {
      name: 'Страна',
      value: params[:country]
    } if params[:country].present?

    par.to_json
  end

  def product_create_params
    params.require(:product).permit(:title, :subtitle, :price, :volume, :in_stock, :is_active, :is_new)
  end
end
