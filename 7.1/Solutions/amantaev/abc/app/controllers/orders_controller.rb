class OrdersController < ApplicationController
  def show
    @order = Order.find(params[:id])
  end
  def create
    @order = Order.create(user: @user, products: @user.cart.products, status: 'created')
  end
end
