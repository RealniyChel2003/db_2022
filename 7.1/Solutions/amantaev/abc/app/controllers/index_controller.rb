class IndexController < ApplicationController
  CATEGORIES_LIMIT = 3
  PRODUCTS_LIMIT = 3

  def index
    @categories = Category.joins(:products).group('categories.id').order('COUNT(products.*) DESC').limit(CATEGORIES_LIMIT)
    @products = Product.where(is_active: true).where(is_new: true).where('in_stock > ?', 0).order('created_at').limit(PRODUCTS_LIMIT)
  end
end
