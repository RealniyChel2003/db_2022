class Order < ApplicationRecord
  STATUSES = %w[created confirmed received]

  belongs_to :user
  has_many :order_items, dependent: :delete_all
  has_many :products, through: :order_items

  validates :status, presence: true, inclusion: { in: STATUSES }

  before_create do
    generate_public_id
  end

  after_create do
    set_price
  end

  private
  def generate_public_id
    public_id = "#{DateTime.current.year}-#{DateTime.current.month}-"
    rnd = Random.rand(1_000)
    while Order.find_by(public_id: public_id + rnd.to_s).present?
      rnd = Random.rand(1_000)
    end
    self.public_id = public_id + rnd.to_s
  end

  def set_price
    update_column(:price, products.pluck(:price).inject(0, :+))
  end
end
