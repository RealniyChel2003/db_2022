class Cart < ApplicationRecord
  belongs_to :user

  has_many :products_cart
  has_many :products, through: :products_cart
end
