class Product < ApplicationRecord
  has_many :products_categories
  has_many :categories, through: :products_categories

  has_many :order_items
  has_many :orders, through: :order_items

  has_many :products_cart
  has_many :cart, through: :products_cart
end
