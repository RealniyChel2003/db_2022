class User < ApplicationRecord
  has_secure_password

  has_many :orders
  has_one :cart, dependent: :destroy

  after_create do
    create_cart
  end

  validates :email, presence: true, uniqueness: true
  validates :mobile, uniqueness: true
end
