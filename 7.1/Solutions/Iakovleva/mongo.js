db.movieDetails.find({title: "Iron Man"})

db.movieDetails.updateOne(
    {title: 'Iron Man'},
    {$set: {'imbd.rating': 80} }
)

db.movieDetails.updateOne(
    {title: "Iron Man"},
    {$set: {soundtracks: 
            [{ title: "Heads Will Roll", authors: "Yeah Yeah Yeahs"},
             { title: "Djadja", authors: "Aya Nakamura"},
             { title: "About You", authors: "Alice Yeng"}
    ]}}
)

db.movieDetails.updateOne(
    { title: 'Iron Man', "soundtracks.title": "Heads Will Roll" },
    { $set: {'soundtracks.$.runtime': 17} }
)

db.movieDetails.updateOne(
    { title: 'Iron Man', writers: "Don Heck"},
    { $set: {'writers.$': "Donald L. Heck"} }
)


db.movieDetails.updateOne(
    {title: "Iron Man"},
    {$push: {genres: {$each: ["Spiderman", "Fantasy"]}}}
)


db.movieDetails.updateOne(
    {title: "Iron Man"},
    {$pull: {writers: {$in: ["Mark Fergus", "Hawk Ostby"]}}}
)

db.movieDetails.updateOne(
    {title: 'Iron Man' },
    {$pull: {soundtracks: {title: 'About You'}}}
)
