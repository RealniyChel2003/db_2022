from flask import Flask, request
import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2.extras import Json
import json
from redis import Redis
import os
from deserealization import deserealization
import logging


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', 
                               port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), 
                               user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), 
                               cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_pg_replication():
    pg_conn = psycopg2.connect(host=os.getenv('REPLICA_HOST') or '127.0.0.1', 
                               port=os.getenv('SLAVE_PORT'),
                               database=os.getenv('POSTGRES_DB'), 
                               user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), 
                               cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', 
                 port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), 
                 decode_responses=True)


# curl --location --request GET 'http://127.0.0.1:{APP_PORT}/cars?offset=0&limit=100'
@app.route('/cars')
def get_cars():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'cars:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_cars = redis_conn.get(redis_key)
        if redis_cars is None:
            query = """
            select car.id as car_id, brand, model, color, car.number as car_number, 
            client.id as client_id, name, client.number as client_number, birthdate 
            from car
            left join car_to_client on car.id = car_to_client.car_id
            left join client on client.id = car_to_client.client_id
            offset %s
            limit %s
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                rows = cur.fetchall()
            cars = deserealization(rows)
            redis_cars = json.dumps(cars, default=vars, ensure_ascii=False, indent=2)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, redis_cars, ex=30)
        return redis_cars, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


# curl --location --request POST 'http://127.0.0.1:{APP_PORT}/cars/create' --header 'Content-Type: application/json' --data-raw '{"brand": "", "model": "", "color": "", "number": ""}'
@app.route('/cars/create', methods=['POST'])
def create_car():
    try:
        body = request.json
        brand = body['brand']
        model = body['model']
        color = body['color']
        number = body['number']
        query = """
        insert into car (brand, model, color, number)
        values (%s, %s, %s, %s)
        returning brand, model, color, number
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (brand, model, color, number))
                result = cur.fetchall()
        return {'message': f'Car: {result[0]["brand"]} {result[0]["model"]} {result[0]["color"]} {result[0]["number"]} created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# curl --location --request POST 'http://127.0.0.1:{APP_PORT}/cars/update' --header 'Content-Type: application/json' --data-raw '{"car_id": , "color": "", "number": ""}'
@app.route('/cars/update', methods=['POST'])
def update_car():
    try:
        body = request.json
        car_id = body['car_id']
        color = body['color']
        number = body['number']
        query = """
        update car
        set color = %s,
        number = %s
        where id = %s
        returning id;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (color, number, car_id))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Car with id = {car_id} updated.'}
        else:
            return {'message': f'Car with id = {car_id} not found.'}, 404
    except Exception as ex:
            logging.error(repr(ex), exc_info=True)
            return {'message': 'Bad Request'}, 400

# curl --location --request DELETE 'http://127.0.0.1:{APP_PORT}/cars/delete' --header 'Content-Type: application/json' --data-raw '{"car_id": 6}'
@app.route('/cars/delete', methods=['DELETE'])
def delete_car():
    try:
        body = request.json
        car_id = body['car_id']
        query = """
        delete from car
        where id = %s
        returning id
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (car_id, ))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Car with id = {car_id} deleted.'}
        else:
            return {'message': f'Car with id = {car_id} not found.'}, 404
    except Exception as ex:
            logging.error(repr(ex), exc_info=True)
            return {'message': 'Bad Request'}, 400

# curl --location --request GET 'http://127.0.0.1:{APP_PORT}/clients/index_search' --header 'Content-Type: application/json' --data-raw '{"name": "", "number": "", "birthdate": ""}'
@app.route('/clients/index_search')
def clients_search():
    try:
        body = request.json
        birthdate = body['birthdate']
        query = """
        select *
        from client
        where birthdate = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (birthdate, ))
            clients_search = cur.fetchall()
        json_clients_search = json.dumps(clients_search, default=vars, ensure_ascii=False, indent=2)
        return json_clients_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# curl --location --request GET 'http://127.0.0.1:{APP_PORT}/mat_view/index_search' --header 'Content-Type: application/json' --data-raw '{"color": "", "brand": ""}'
@app.route('/mat_view/index_search')
def mat_view_search():
    try:
        body = request.json
        color = body['color']
        brand = body['brand']
        query = """
        select *
        from car_to_client_link
        where color = %s
        and brand = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (color, brand))
            mat_view_search = cur.fetchall()
        json_mat_view_search = json.dumps(mat_view_search, default=vars, ensure_ascii=False, indent=2)
        return json_mat_view_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# curl --location --request GET 'http://127.0.0.1:{APP_PORT}/cars/json_ex_owners' --header 'Content-Type: application/json' --data-raw '{"name": ""}'
@app.route('/cars/json_ex_owners')
def json_owners_search():
    try:
        body = request.json
        name = body['name']
        query_param = [{"name": name}]
        query = """
        select *
        from car
        where ex_owners @> %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (Json(query_param), ))
            json_name = cur.fetchall()
        json_json_name = json.dumps(json_name, default=vars, ensure_ascii=False, indent=2)
        return json_json_name, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# curl --location --request GET 'http://127.0.0.1:{APP_PORT}/clients/array_search' --header 'Content-Type: application/json' --data-raw '{"category": ""}'
@app.route('/clients/array_search')
def clients_array_search():
    try:
        body = request.json
        category = body['category']
        query = """
        select *
        from client
        where licenses && array [%s];
        """
        with get_pg_replication() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (category, ))
            array_search = cur.fetchall()
        json_array_search = json.dumps(array_search, default=vars, ensure_ascii=False, indent=2)
        return json_array_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400
