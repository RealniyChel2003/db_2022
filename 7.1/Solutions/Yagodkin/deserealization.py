def deserealization(rows):
    class Car:
        def __init__(self, identify: int, brand: str, model: str, color: str, number: str):
            self.identify = identify
            self.brand = brand
            self.model = model
            self.color = color
            self.number = number
            self.clients: list[Client] = []

    class Client:
        def __init__(self, identify: int, name: str, number: str, birthdate: str):
            self.identify = identify
            self.name = name
            self.number = number
            self.birthdate = birthdate

    cars_dict = {}
    clients_dict = {}

    for row in rows:
        car_id = row['car_id']
        brand = row['brand']
        model = row['model']
        color = row['color']
        car_number = row['car_number']

        car = None
        if car_id in cars_dict:
            car = cars_dict[car_id]
        else:
            car = Car(car_id, brand, model, color, car_number)
            cars_dict[car_id] = car

        client_id = row['client_id']
        name = row['name']
        client_number = row['client_number']
        birthdate = row['birthdate']

        client = None
        if client_id in clients_dict:
            client = clients_dict[client_id]
        else:
            client = Client(client_id, name, client_number, birthdate)
            clients_dict[client_id] = client

        if client_id is not None:
            if client not in car.clients: 
                car.clients.append(client)

    return list(cars_dict.values())
