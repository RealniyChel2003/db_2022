alter table car 
	drop column if exists ex_owners;

alter table car 
	add column ex_owners jsonb;

UPDATE car
    SET ex_owners = (case when number = 'а777уе 05' then '[{
									"name": "Damir Magomedov",
									"number": "8(872)734-45-76",
									"birthdate": "30.10.1986"
								},{
									"name": "Butta Gatagov",
									"number": "8(872)267-26-84",
									"birthdate": "21.11.1992"
								},{
									"name": "Khabib Sokhiev",
									"number": "8(872)535-94-26",
									"birthdate": "07.07.1997"
								}]'::jsonb
                          when number = 'о111оо 77' then '[{
									"name": "Osman Ketia",
									"number": "8(872)565-33-61",
									"birthdate": "11.01.1984"
								}]'::jsonb
                          when number = 'р836пм 76' then '[{
									"name": "Aleksandr Stepanov",
									"number": "8(863)843-12-53",
									"birthdate": "03.12.1969"
								},{
									"name": "Oleg Denisov",
									"number": "8(863)985-67-87",
									"birthdate": "31.10.1978"
								}]'::jsonb
                          when number = 'а321аа 95' then '[
								]'::jsonb
                          when number = 'о121ла 123' then '[{
									"name": "Nurlan Adiev",
									"number": "8(862)677-11-17",
									"birthdate": "10.09.1988"
								}]'::jsonb
                     end);
                 
create index car_ex_owners on car using gin (ex_owners);



alter table client 
	drop column if exists licenses; 
        
alter table client 
	add column licenses text[];

UPDATE client
    SET licenses = (case when name = 'Ramadan Khalilov' then '{A, B}'::text[]
                         when name = 'Edgar Kochkonyan' then '{A, B, BE}'::text[]
                         when name = 'Svetlana Smirnova' then '{B}'::text[]
                         when name = 'Andrey Gorokhov' then '{A, B, BE, C, D}'::text[]
                         when name = 'Rasul Umarov' then '{A, B}'::text[]
                    end);
        
create index client_licenses on client using gin (licenses);
