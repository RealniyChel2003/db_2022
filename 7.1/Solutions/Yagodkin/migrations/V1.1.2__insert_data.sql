insert into car (brand, model, color, number) values
    ('LADA', 'Priora', 'black', 'а777уе 05'),
    ('Porsche', 'Panamera', 'yellow', 'о111оо 77'),
    ('Renault', 'Logan', 'blue', 'р836пм 76'),
    ('BMW', 'X5', 'grey', 'а321аа 95'),
    ('Skoda', 'Rapid', 'black', 'о121ла 123');

insert into client (name, number, birthdate) values
    ('Ramadan Khalilov', '8(871)434-43-34', '05.06.1977'),
    ('Edgar Kochkonyan', '8(862)677-11-17', '13.07.1985'),
    ('Svetlana Smirnova', '8(863)253-57-23', '08.03.1970'),
    ('Andrey Gorokhov', '8(863)754-23-86', '23.02.1968'),
    ('Rasul Umarov', '8(872)544-44-68', '11.11.2001');

insert into car_to_client values
    (1, 5),
    (2, 2),
    (3, 3),
    (3, 4),
    (4, 1),
    (5, 2);

insert into ordering (price, date, time, client) values
    (72600, '06.07.2003', '11:40', 1),
    (43350, '11.08.2012', '14:14', 1),
    (52000, '25.09.2017', '10:50', 2),
    (9750, '15.01.2020', '13:00', 3),
    (33700, '19.04.2022', '09:45', 4),
    (21300, '20.10.2021', '11:10', 5),
    (27000, '16.11.2022', '12:55', 5);
