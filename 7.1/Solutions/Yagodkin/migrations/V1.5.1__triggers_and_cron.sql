drop function if exists refresh_car_to_client_link;
create function refresh_car_to_client_link()
    returns trigger as
$$
begin
    refresh materialized view concurrently car_to_client_link;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_car_table
    after insert or update or delete
    on car
    for each row
execute procedure refresh_car_to_client_link();



create extension pg_cron;

select cron.schedule('refresh_car_to_client_link', '* * * * *',
                     $$ refresh materialized view concurrently car_to_client_link $$);
