drop materialized view if exists car_to_client_link;

create
materialized view car_to_client_link as
select car.id as car_id, brand, model, color, car.number as car_number, 
client.id as client_id, name, client.number as client_number, birthdate 
from car
left join car_to_client on car.id = car_to_client.car_id
left join client on client.id = car_to_client.client_id;

drop index if exists car_to_client_link_id;
create unique index car_to_client_link_id on car_to_client_link (car_id, client_id);

drop index if exists car_to_client_link_btree;
create index car_to_client_link_btree on car_to_client_link
using btree (color, brand);


drop index if exists client_search_by_indexes;
create index client_search_by_indexes on client
using btree (name, number, birthdate);
