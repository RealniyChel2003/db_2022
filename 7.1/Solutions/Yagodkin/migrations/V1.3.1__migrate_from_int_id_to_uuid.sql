begin;

create extension if not exists "uuid-ossp";

alter table car_to_client
    drop constraint car_to_client_car_id_fkey;
alter table car_to_client
    rename column car_id to old_car_id;
alter table car_to_client
    add column car_id uuid;

alter table car
    drop constraint car_pkey;
alter table car
    rename column id to old_id;
alter table car
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from car
            loop
                update car_to_client set car_id = row.id where old_car_id = row.old_id;
            end loop;
    end
$$;

drop materialized view if exists car_to_client_link;

alter table car
    drop column old_id;
alter table car
    add primary key (id);

alter table car_to_client
    drop column old_car_id;
alter table car_to_client
    add constraint fk_car_id foreign key (car_id) references car;
alter table car_to_client
    alter column car_id set not null;
alter table car_to_client
    add constraint uq_car_id_to_client_id unique (car_id, client_id);

alter table car_to_client
    drop constraint car_to_client_client_id_fkey;
alter table car_to_client
    rename column client_id to old_client_id;
alter table car_to_client
    add column client_id uuid;

alter table "ordering"
    drop constraint ordering_client_fkey;
alter table "ordering"
    rename column client to old_client;
alter table "ordering"
    add column client uuid;

alter table client
    drop constraint client_pkey;
alter table client
    rename column id to old_id;
alter table client
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from client
            loop
                update car_to_client set client_id = row.id where old_client_id = row.old_id;
                update "ordering" set client = row.id where old_client = row.old_id;
            end loop;
    end
$$;

alter table client
    drop column old_id;
alter table client
    add primary key (id);

alter table car_to_client
    drop column old_client_id;
alter table car_to_client
    add constraint fk_client_id foreign key (client_id) references client;
alter table car_to_client
    alter column client_id set not null;

alter table "ordering"
    drop column old_client;
alter table "ordering"
    add constraint fk_client_id foreign key (client) references client;

create
materialized view car_to_client_link as
select car.id as car_id, brand, model, color, car.number as car_number, 
client.id as client_id, name, client.number as client_number, birthdate 
from car
left join car_to_client on car.id = car_to_client.car_id
left join client on client.id = car_to_client.client_id;

drop index if exists car_to_client_link_id;
create unique index car_to_client_link_id on car_to_client_link (car_id, client_id);

drop index if exists car_to_client_link_btree;
create index car_to_client_link_btree on car_to_client_link
using btree (color, brand);

commit;
