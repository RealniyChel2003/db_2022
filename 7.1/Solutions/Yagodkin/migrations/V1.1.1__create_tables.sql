drop table if exists car cascade;
drop table if exists client cascade;
drop table if exists car_to_client cascade;
drop table if exists ordering cascade;

create table car 
(
    id int generated always as identity primary key,
    brand text,
    model text,
    color text,
    number text
);

create table client 
(
    id int generated always as identity primary key,
    name text,
    number text,
    birthdate text
);

create table car_to_client 
(
    car_id int not null references car(id),
    client_id int not null references client(id),
    unique(car_id, client_id)
);

create table ordering
(
    id int generated always as identity primary key,
    price int,
    date text,
    time text,
    client int not null references client(id)
);
