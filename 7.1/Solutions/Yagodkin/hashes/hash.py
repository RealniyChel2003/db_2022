from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'ae74fdf78c15ae6ec6f823ef2af9b2401f83ffbfe1e229a40a143a759d9a4107dc5fb944085fe884b893b6c6d6a3be045f90ae08ad8c3a1d2a83aa828141305f', 'fb4eb2e70b1e1f5831fefb387509e5bf05c177fae2077387ab2e10274028b6d190264ffdc72cd91eae6fee3f8c7267b7b34070330504cdc95f1bc40ff2f13886'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        keccak_hash = keccak.new(digest_bits=512)
        keccak_hash.update(password.encode('utf-8'))
        hash = keccak_hash.hexdigest()

        if hash in hashes:
            print(f'password = {password}, hash = {hash}')
            hashes.remove(hash)

        if len(hashes) == 0:
            break
