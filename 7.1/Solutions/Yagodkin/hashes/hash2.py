from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '993b7d7db5db391e1caac81a1c632f819cb24349163fa2f32cc621754d6808e37c6643a90ff1998a6a2dc7093d3bcf12f253a713c3660ad1d8f52cbe4f5ac7bd'
salt = 'caa90efb-481c-4750-8bfe-f88ff776c4f1'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        keccak_hash = keccak.new(digest_bits=512)
        keccak_hash.update((password + salt).encode('utf-8'))
        hash = keccak_hash.hexdigest()
        if hash == hash:
            print(f'password = {password}')
            found = True
            break
