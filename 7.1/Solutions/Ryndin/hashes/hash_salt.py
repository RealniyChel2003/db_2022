from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '42ee49c3d7ac1230ab92467713bb6427b909724445c315caca008f23e7209fbe'
salt = 'bdee9cd5-2653-4cc3-bd27-8b17eadba903'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        if hashlib.shake_128((password + salt).encode('utf-8')).hexdigest(32) == hash:
            print(f'password = {password}')
            found = True
            break

# Results:
# password = 2#gw