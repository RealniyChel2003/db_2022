from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'dae12160367f39bb7070e922180dd34843989e11d2b11f5f005337bcd9c41bdb',
          '1a7a272bdf177ab950625b42a3bdf4648fe2549cdbacc9fc756879132af17658'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)
        shake128_32 = hashlib.shake_128(password.encode('utf-8')).hexdigest(32)

        if shake128_32 in hashes:
            print(f'password = {password}, hash = {shake128_32}')
            hashes.remove(shake128_32)

        if len(hashes) == 0:
            break

# Results:
# password = FE, hash = 1a7a272bdf177ab950625b42a3bdf4648fe2549cdbacc9fc756879132af17658
# password = TO, hash = dae12160367f39bb7070e922180dd34843989e11d2b11f5f005337bcd9c41bdb