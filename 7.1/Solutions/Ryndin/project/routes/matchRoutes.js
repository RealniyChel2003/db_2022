const Router = require('express')
const router = new Router()
const matchController = require('../controllers/matchController')

router.get('/getByEvent',matchController.getByEvent)

module.exports = router