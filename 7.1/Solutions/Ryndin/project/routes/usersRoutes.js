const Router = require('express')
const router = new Router()
const usersController = require('../controllers/usersController')

router.post('/createUser', usersController.createUser)
router.get('/getUsers', usersController.getAllUsers)
router.get('/getUser/:id', usersController.getOneUser)
router.get('/getUserByFavorite', usersController.findByFavorite)
router.put('/editUser', usersController.updateUser)
router.delete('/deleteUser/:id', usersController.deleteUser)


module.exports = router