const Router = require('express')
const router = new Router()
const ticketsController = require('../controllers/ticketsController')

router.post('/createTicket', ticketsController.createTicket)
router.get('/getTickets', ticketsController.getAllTickets)
router.get('/getTicket/:id', ticketsController.getOneTicket)
router.get('/getFullTicket/:id', ticketsController.getFullOneTicket)
router.put('/editTicket', ticketsController.updateTicket)
router.delete('/deleteTicket/:id', ticketsController.deleteTicket)
router.get('/getByThree', ticketsController.getByThree)
router.get('/getMaterialize', ticketsController.getMaterialize)
router.get('/refresh', ticketsController.refresh)


module.exports = router