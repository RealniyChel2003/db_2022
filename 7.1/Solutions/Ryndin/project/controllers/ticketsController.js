const db = require('../db')
const redis_client = require('../redis_db')

class TicketsController {

    async getMaterialize(req, res) {
        try {
            const {place, sector} = req.body
            const matView = await db.query('select * from user_with_match where place = $1 and sector = $2;', [place, sector]);
            res.json(matView.rows)
        } catch (error) {
            console.log(error);
        }
    }

    async refresh(req, res) {
        try {
            await db.query('refresh materialized view concurrently user_with_match;')
            res.send('refresh complete!')
        } catch(error) {
            console.log(error);
        }
    }

    async getByThree(req,res) {
        const {firstTeam, secondTeam, fanId} = req.body
        try {
            const data = await db.query('select * from "match" where first_team = $1 and second_team = $2 and fan_id = $3 ;', [firstTeam,secondTeam,fanId])
            for (let i = 0; i < data.rows.length; i++) {
                const result = {
                    first_team : data.rows[i].first_team,
                    second_team : data.rows[i].second_team,
                    date_and_time: data.rows[i].date_and_time,
                    fan_id : data.rows[i].fan_id
                };
                const json = JSON.stringify(result)
                res.json(JSON.parse(json))
            }
        } catch (error) {
            console.log(error);
        }
    }


    async createTicket(req, res) {
        const {sector, place, matchId, userId} = req.body
        const newTicket = await db.query('insert into ticket(sector, place, match_id, user_id) values ($1,$2,$3,$4) returning *;', [sector, place, matchId, userId])
        res.json(newTicket.rows)
    }

    async getAllTickets(req, res) {
        const tickets = await db.query('select * from ticket')
        res.json(tickets.rows)
    }

    async getOneTicket(req, res) {
        const id = req.params.id
        const redis_key = 'ticket' + id
        const redis_value = await redis_client.get(redis_key)
        if(redis_value === null) {
            const ticket = await db.query('select * from ticket where id = $1;', [id])
            res.json(ticket.rows)
            await redis_client.set('ticket' + id, JSON.stringify(ticket.rows[0]))
        } else {
            res.json(JSON.parse(redis_value))
        }

    }

    async getFullOneTicket(req, res) {
        const id = req.params.id
        const data = await db.query(
            'select ' +
            't.id as ticket_id, t.sector, t.place, ' +
            'm.first_team, m.second_team, m.date_and_time, m.has_fan_id, ' +
            'u.initials, u.mail, u.age ' +
            'from ticket t ' +
            'join "match" m on t.match_id = m.id ' +
            'join "user" u on t.user_id = u.id ' +
            'where t.id = $1', [id]
        )
        res.json(data.rows)
    }

    async updateTicket(req, res) {
        const {id, sector, place, matchId, userId} = req.body
        const editingTicket = await db.query(
            'update ticket set sector = $1, place = $2, match_id = $3, user_id = $4  where id = $5 returning *;', [sector, place, matchId, userId, id])
        res.json(editingTicket.rows)
    }

    async deleteTicket(req, res) {
        const id = req.params.id
        const deletedTicket = await db.query('delete from ticket where id = $1;', [id])
        res.json(deletedTicket.rows[0])
    }
}

module.exports = new TicketsController()