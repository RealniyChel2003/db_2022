const db = require('../db')
const redis_client = require("../redis_db");

class UsersController{

    async createUser(req,res){
        const {initials,mail,age} = req.body
        const newUser = await db.query('insert into "user"(initials,mail,age) values ($1,$2,$3) RETURNING *',[initials,mail,age])
        res.json(newUser.rows[0])
    }

    async getAllUsers(req,res){
        const users = await db.query('select * from "user"')
        res.json(users.rows)
    }

    async getOneUser(req,res){
        const id= req.params.id
        const redis_key = 'user' + id
        const redis_value = await redis_client.get(redis_key)
        if(redis_value === null) {
            const user = await db.query('select * from "user" where id=$1;', [id])
            res.json(user.rows[0])
            await redis_client.set('user' + id, JSON.stringify(user.rows[0]))
        } else {
            res.json(JSON.parse(redis_value))
        }
    }

    async updateUser(req,res){
        const {id,initials,mail,age} = req.body
        const editingUser = await db.query(
            'update "user" set initials = $1, mail = $2, age = $3 where id = $4 RETURNING *;',[initials,mail,age,id]
        )
        res.json(editingUser.rows[0])
    }

    async deleteUser(req,res){
        const id = req.params.id
        const deletedUser = await db.query('delete from "user" where id = ' + id)
        res.json(deletedUser.rows[0])
    }

    async findByFavorite(req, res) {
        const body = req.body;
        const query = `
        select *
        from "user"
        where favorite_team  @> '[${JSON.stringify(body)}]'
        `
        const queryResult = await db.query(query)
        res.json(queryResult.rows)
    }

}

module.exports = new UsersController()
