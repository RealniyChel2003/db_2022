const db = require('../db')

class MatchController {

    async getByEvent(req, res) {
        const {event} = req.body;
        const query = `
        select *
        from "match" 
        where events && array ['${event}'] 
        `
        const queryResult = await db.query(query)
        res.json(queryResult.rows)
    }
}

module.exports = new MatchController()


