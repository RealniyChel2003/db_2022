require('dotenv').config()
const {createClient} = require("redis");

const client = createClient({
    url: `redis://@${process.env.REDIS_HOST ?? '127.0.0.1'}:${process.env.REDIS_PORT}`
})

client.on('error', (err) => console.log('Redis Client Error', err));

client.connect()

module.exports = client

