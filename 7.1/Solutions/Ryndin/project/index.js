require('dotenv').config()

const PORT = process.env.APP_PORT
// const REDIS = process.env.REDIS_PORT

const express = require('express')
const ticketRouter = require('./routes/ticketsRoutes')
const userRouter = require('./routes/usersRoutes')
const matchRouter = require('./routes/matchRoutes')

const app = express()

app.use(express.json())
app.use('/api', ticketRouter)
app.use('/api', userRouter)
app.use('/api', matchRouter)

app.listen(PORT, () => console.log('server starter on port ' + PORT))
