create table ticket(
	id bigint generated always as identity primary key,
	sector text not null,
	place text not null unique,
	match_id int not null references "match" on delete cascade,
	user_id int not null references "user" on delete cascade,
	unique(match_id, user_id)
);
