create table "match"(
	id bigint generated always as identity primary key,
	first_team text not null,
	second_team text not null,
	date_and_time timestamp not null,
	has_fan_id boolean not null
);