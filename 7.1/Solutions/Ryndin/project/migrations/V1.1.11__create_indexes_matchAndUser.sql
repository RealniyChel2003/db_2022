create index user_favorite_team on "user" using gin (favorite_team);

create index match_events on "match" using gin (events);