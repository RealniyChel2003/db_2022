begin;

drop materialized view user_with_match;
drop index if exists user_with_match_id;
drop index if exists match_search_by_teams;

create extension if not exists "uuid-ossp";


alter table ticket
    drop constraint ticket_match_id_fkey;

alter table ticket
    rename column match_id to old_match_id;

alter table ticket
    add column match_id uuid;


alter table "match"
    drop constraint match_pkey;

alter table "match"
    rename column id to old_id;

alter table "match"
    add column id uuid default uuid_generate_v4();


do
$$
    declare
        row record;
    begin
        for row in select * from "match"
            loop
                update ticket  set match_id = row.id where old_match_id = row.old_id;
            end loop;
    end
$$;


alter table "match"
    drop column old_id;

alter table "match"
    add primary key (id);


alter table ticket
    drop column old_match_id;

alter table ticket
    add constraint fk_match_id foreign key (match_id) references "match";

alter table ticket
    alter column match_id set not null;

alter table ticket
    add constraint uq_match_id_to_ticket_id unique (match_id, id);


create
materialized view user_with_match as
select t.id as ticket_id,
	   u.id as user_id, u.initials, u.mail, u.age,
       t.sector, t.place, t.match_id as match_id_uuid,
       m.first_team, m.second_team, m.date_and_time
from "user" u
	join ticket t on u.id = t.user_id
	join "match" m  on m.id = t.match_id;

create unique index user_with_match_id on user_with_match (user_id, ticket_id);

create index match_search_by_teams_and_date on user_with_match
    using btree (sector, place);

create index match_search_by_teams on "match"
    using btree (first_team, second_team, has_fan_id);


commit;
