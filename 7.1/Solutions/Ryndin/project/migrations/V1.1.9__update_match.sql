alter table "match"
add column events text[];

update "match"
set events = '{group Avaria, kettlebell lifting master class}'
where first_team = 'FC Sochi' and second_team = 'Dinamo Moscow';