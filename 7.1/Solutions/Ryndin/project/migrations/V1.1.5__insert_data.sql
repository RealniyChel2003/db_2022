insert into "user" (initials, mail, age) values
('Ryndin S.I', 'SeregaSwapp@mail.ru', 18),
('Danilov G.K', 'gleb22d@mail.ru', 19),
('Kochyan A.E', 'ArutSochi@mail.ru', 19),
('Yagodkin M.S', 'LocalGelir@mail.ru', 19);

insert into "match" (first_team, second_team, date_and_time, has_fan_id) values
('FC Sochi', 'Dinamo Moscow', '2009-06-04 18:25:08', true);

insert into ticket (sector, place, match_id, user_id) values
('C109','25-07',1,1),
('C109','25-08',1,2),
('C109','25-09',1,3),
('C109','25-10',1,4);
