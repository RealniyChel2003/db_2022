create table "user"(
	id bigint generated always as identity primary key,
	initials text not null,
	mail text not null,
	age int not null
);