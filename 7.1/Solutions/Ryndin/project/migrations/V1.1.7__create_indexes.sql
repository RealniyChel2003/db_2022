create unique index user_with_match_id on user_with_match (user_id, ticket_id);

create index match_search_by_teams_and_date on user_with_match
    using btree (sector, place);

create index match_search_by_teams on "match"
    using btree (first_team, second_team, has_fan_id);


