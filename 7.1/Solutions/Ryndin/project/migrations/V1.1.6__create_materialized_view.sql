create
materialized view user_with_match as
select t.id as ticket_id,
	   u.id as user_id, u.initials , u.mail , u.age ,
       t.sector, t.place, t.match_id as match_id_uuid,
       m.first_team, m.second_team, m.date_and_time
from "user" u
	join ticket t on u.id = t.user_id
	join "match" m  on m.id = t.match_id;
