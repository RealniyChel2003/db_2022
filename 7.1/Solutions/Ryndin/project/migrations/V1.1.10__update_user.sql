alter table "user"
add column favorite_team jsonb;

update "user"
set favorite_team  = '[{"name": "Spartak", "player": "Alexander Sobolev"},{"name": "FC Sochi", "player": "Gerorgiy Melkadze"}]'
where initials  = 'Ryndin S.I';

update "user"
set favorite_team  = '[{"name" : "CSKA", "player": "Jesus Polov"},{"name" : "FC Sochi", "player": "Artur Jusupov"}]'
where initials  = 'Danilov G.K';

update "user"
set favorite_team  = '[{"name" : "Arsenal", "player": "Michel Nilton"},{"name" : "Roma", "player": "Robert Kozhikov"}]'
where initials  = 'Kochyan A.E';

update "user"
set favorite_team  = '[{"name" : "Ural","player": "Danil Vetrov"},{"name" : "Orenburg","player": "Oleg Kochkin"}]'
where initials  = 'Yagodkin M.S';