from flask import Flask, request
import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2.extras import Json
import json
from redis import Redis
import os

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', 
                               port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), 
                               user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), 
                               cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', 
                 port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), 
                 decode_responses=True)


@app.route('/authors', methods=['GET'])
def get_authors():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'authors:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_authors = redis_conn.get(redis_key)
        if redis_authors is None:
            query = """
            select a.id as authors_id, "name", last_name, "e-mail", phone, 
                r.id as recipe_id, title, serves, ingredients, directions
            from author a
                left join recipe r on r.authors_id = a.id
            offset %s
            limit %s
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                authors = cur.fetchall()
            json_authors = json.dumps(authors, default=str, ensure_ascii=False, indent=2)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, json_authors, ex=5)
        return json_authors, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/authors/create', methods=['POST'])
def create_author():
    try:
        body = request.json
        name = body['name']
        last_name = body['last_name']
        email = body['email']
        phone = body['phone']
        query = """
        insert into author("name", last_name, "e-mail", phone)
        values (%s, %s, %s, %s)
        returning "name", last_name, "e-mail", phone
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (name, last_name, email, phone))
                result = cur.fetchall()
        return {'message': f'Author: {result[0]["name"]} {result[0]["last_name"]}, {result[0]["e-mail"]}, {result[0]["phone"]} created.'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/authors/update', methods=['POST'])
def update_author():
    try:
        body = request.json
        author_id = body['author_id']
        new_email = body['new_email']
        new_phone = body['new_phone']
        query = """
        update author
        set "e-mail" = %s,
        phone = %s
        where id = %s
        returning name, last_name;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (new_email, new_phone, author_id))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Author {affected_rows[0]["name"]} {affected_rows[0]["last_name"]} with id = {author_id} updated.'}
        else:
            return {'message': f'Author with id = {author_id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/authors/delete', methods=['DELETE'])
def delete_author():
    try:
        body = request.json
        author_id = body['author_id']
        query = """
        delete from author
        where id = %s
        returning id
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (author_id, ))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Author with id = {author_id} deleted.'}
        else:
            return {'message': f'Author with id = {author_id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/recipes/search')
def recipes_search():
    try:
        body = request.json
        title = body['title']
        cook_time_from = body['cook_time_from']
        cook_time_to = body['cook_time_to']
        ingredients = body['ingredients']
        query = """
        select *
        from recipe
        where title = %s
        and cook_time >= %s
        and cook_time <= %s
        and ingredients = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (title, cook_time_from, cook_time_to, ingredients))
            recipes_search = cur.fetchall()
        json_recipes_search = json.dumps(recipes_search, default=str, ensure_ascii=False, indent=2)
        return json_recipes_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/materialized_view/search')
def mat_view_search():
    try:
        body = request.json
        prep_time_from = body['prep_time_from']
        prep_time_to = body['prep_time_to']
        dishes = body['dishes']
        slug = body['slug']
        query = """
        select *
        from recipe_with_category
        where prep_time >= %s
        and prep_time <= %s
        and dishes = %s
        and slug = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (prep_time_from, prep_time_to, dishes, slug))
            mat_view_search = cur.fetchall()
        json_mat_view_search = json.dumps(mat_view_search, default=str, ensure_ascii=False, indent=2)
        return json_mat_view_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400
