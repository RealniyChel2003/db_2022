drop table if exists author cascade;
drop table if exists recipe cascade;
drop table if exists category cascade;
drop table if exists recipe_to_category cascade;

create table author(
    "name" text,
    last_name text,
    "e-mail" text,
    phone text,
    id int generated always as identity primary key
);

create table recipe(
    title text,
    serves text,
    prep_time int,
    cook_time int,
    ingredients text,
    directions text,
    authors_id int not null references author,
    id int generated always as identity primary key
);

create table category(
    dishes text,
    slug text,
    description text,
    id int generated always as identity primary key
);

create table recipe_to_category(
    recipes_id int not null references recipe,
    categorys_id int not null references category
);

insert into author("name", last_name, "e-mail", phone)
values ('name_1', 'last_name_1', 'e-mail_1', 'phone_1'),
       ('name_2', 'last_name_2', 'e-mail_2', 'phone_2'),
       ('name_3', 'last_name_3', 'e-mail_3', 'phone_3'),
       ('name_4', 'last_name_4', 'e-mail_4', 'phone_4');

insert into recipe(title, serves, prep_time, cook_time, ingredients, directions, authors_id)
values ('title_1', 'serves_1', 1, 1, 'ingrediets_1', 'directions_1', 1),
       ('title_2', 'serves_2', 2, 2, 'ingrediets_2', 'directions_2', 1),
       ('title_3', 'serves_3', 3, 3, 'ingrediets_3', 'directions_3', 1),
       ('title_4', 'serves_4', 4, 4, 'ingrediets_4', 'directions_4', 1),
       ('title_5', 'serves_5', 5, 5, 'ingrediets_5', 'directions_5', 1),

       ('title_6', 'serves_6', 6, 6, 'ingrediets_6', 'directions_6', 2),
       ('title_7', 'serves_7', 7, 7, 'ingrediets_7', 'directions_7', 2),
       ('title_8', 'serves_8', 8, 8, 'ingrediets_8', 'directions_8', 2),
       ('title_9', 'serves_9', 9, 9, 'ingrediets_9', 'directions_9', 2),
       ('title_10', 'serves_10', 10, 10, 'ingrediets_10', 'directions_10', 2),

       ('title_11', 'serves_11', 11, 11, 'ingrediets_11', 'directions_11', 3),
       ('title_12', 'serves_12', 12, 12, 'ingrediets_12', 'directions_12', 3),
       ('title_13', 'serves_13', 13, 13, 'ingrediets_13', 'directions_13', 3),
       ('title_14', 'serves_14', 14, 14, 'ingrediets_14', 'directions_14', 3),
       ('title_15', 'serves_15', 15, 15, 'ingrediets_15', 'directions_15', 3),

       ('title_16', 'serves_16', 16, 16, 'ingrediets_16', 'directions_16', 4),
       ('title_17', 'serves_17', 17, 17, 'ingrediets_17', 'directions_17', 4),
       ('title_18', 'serves_18', 18, 18, 'ingrediets_18', 'directions_18', 4),
       ('title_19', 'serves_19', 19, 19, 'ingrediets_19', 'directions_19', 4),
       ('title_20', 'serves_20', 20, 20, 'ingrediets_20', 'directions_20', 4);

insert into category(dishes, slug, description)
values ('dishes_1', 'slug_1', 'description_1'),
       ('dishes_2', 'slug_2', 'description_2'),
       ('dishes_3', 'slug_3', 'description_3'),
       ('dishes_4', 'slug_4', 'description_4'),
       ('dishes_5', 'slug_5', 'description_5'),

       ('dishes_6', 'slug_6', 'description_6'),
       ('dishes_7', 'slug_7', 'description_7'),
       ('dishes_8', 'slug_8', 'description_8'),
       ('dishes_9', 'slug_9', 'description_9'),
       ('dishes_10', 'slug_10', 'description_10'),

       ('dishes_11', 'slug_11', 'description_11'),
       ('dishes_12', 'slug_12', 'description_12'),
       ('dishes_13', 'slug_13', 'description_13'),
       ('dishes_14', 'slug_14', 'description_14'),
       ('dishes_15', 'slug_15', 'description_15'),

       ('dishes_16', 'slug_16', 'description_16'),
       ('dishes_17', 'slug_17', 'description_17'),
       ('dishes_18', 'slug_18', 'description_18'),
       ('dishes_19', 'slug_19', 'description_19'),
       ('dishes_20', 'slug_20', 'description_20'),

       ('dishes_21', 'slug_21', 'description_21'),
       ('dishes_22', 'slug_22', 'description_22'),
       ('dishes_23', 'slug_23', 'description_23'),
       ('dishes_24', 'slug_24', 'description_24'),
       ('dishes_25', 'slug_25', 'description_25'),

       ('dishes_26', 'slug_26', 'description_26'),
       ('dishes_27', 'slug_27', 'description_27'),
       ('dishes_28', 'slug_28', 'description_28'),
       ('dishes_29', 'slug_29', 'description_29'),
       ('dishes_30', 'slug_30', 'description_30'),

       ('dishes_31', 'slug_31', 'description_31'),
       ('dishes_32', 'slug_32', 'description_32'),
       ('dishes_33', 'slug_33', 'description_33'),
       ('dishes_34', 'slug_34', 'description_34'),
       ('dishes_35', 'slug_35', 'description_35'),

       ('dishes_36', 'slug_36', 'description_36'),
       ('dishes_37', 'slug_37', 'description_37'),
       ('dishes_38', 'slug_38', 'description_38'),
       ('dishes_39', 'slug_39', 'description_39'),
       ('dishes_40', 'slug_40', 'description_40');

insert into recipe_to_category(recipes_id, categorys_id)
values (1, 1),
       (1, 2),
       (2, 3),
       (2, 4),
       (3, 5),
       (3, 6),
       (4, 7),
       (4, 8),
       (5, 9),
       (5, 10),

       (6, 11),
       (6, 12),
       (7, 13),
       (7, 14),
       (8, 15),
       (8, 16),
       (9, 17),
       (9, 18),
       (10, 19),
       (10, 20),

       (11, 21),
       (11, 22),
       (12, 23),
       (12, 24),
       (13, 25),
       (13, 26),
       (14, 27),
       (14, 28),
       (15, 29),
       (15, 30),

       (16, 31),
       (16, 32),
       (17, 33),
       (17, 34),
       (18, 35),
       (18, 36),
       (19, 37),
       (19, 38),
       (20, 39),
       (20, 40);
