drop index if exists recipe_search;

create index recipe_search on recipe using btree (title, cook_time, ingredients);


drop materialized view if exists recipe_with_category;

create
materialized view recipe_with_category as
select title, serves, prep_time, cook_time, ingredients, directions, authors_id, dishes, slug, description, r.id as recipe_id, c.id as categorys_id
from recipe r
    join recipe_to_category rc on r.id = rc.recipes_id
    join category c on rc.categorys_id = c.id;

drop index if exists recipe_with_category_id;
create unique index recipe_with_category_id on recipe_with_category (recipe_id, categorys_id);

drop index if exists recipe_with_category_btree;
create index recipe_with_category_btree on recipe_with_category
using btree (prep_time, dishes, slug);
