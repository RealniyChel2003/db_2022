begin;

create extension if not exists "uuid-ossp";

drop materialized view if exists recipe_with_category;

---

alter table recipe_to_category
    drop constraint recipe_to_category_categorys_id_fkey;
alter table recipe_to_category
    rename column categorys_id to old_categorys_id;
alter table recipe_to_category
    add column categorys_id uuid;

alter table category
    drop constraint category_pkey;
alter table category
    rename column id to old_id;
alter table category
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from category
            loop
                update recipe_to_category set categorys_id = row.id where old_categorys_id = row.old_id;
            end loop;
    end
$$;

alter table category
    drop column old_id;
alter table category
    add primary key (id);
   
alter table recipe_to_category
    drop column old_categorys_id;
alter table recipe_to_category
    add constraint fk_categorys_id foreign key (categorys_id) references category;
alter table recipe_to_category
    alter column categorys_id set not null;
alter table recipe_to_category
    add constraint uq_categorys_id_to_recipes_id unique (categorys_id, recipes_id);

---
   
alter table recipe_to_category
    drop constraint recipe_to_category_recipes_id_fkey;
alter table recipe_to_category
    rename column recipes_id to old_recipes_id;
alter table recipe_to_category
    add column recipes_id uuid;

alter table recipe
    drop constraint recipe_pkey;
alter table recipe
    rename column id to old_id;
alter table recipe
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from recipe
            loop
                update recipe_to_category set recipes_id = row.id where old_recipes_id = row.old_id;
            end loop;
    end
$$;

alter table recipe
    drop column old_id;
alter table recipe
    add primary key (id);

alter table recipe_to_category
    drop column old_recipes_id;
alter table recipe_to_category
    add constraint fk_recipes_id foreign key (recipes_id) references recipe;
alter table recipe_to_category
    alter column recipes_id set not null;
   
---

alter table recipe
    drop constraint recipe_authors_id_fkey;
alter table recipe
    rename column authors_id to old_authors_id;
alter table recipe
    add column authors_id uuid;

alter table author
    drop constraint author_pkey;
alter table author
    rename column id to old_id;
alter table author
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from author
            loop
                update recipe set authors_id = row.id where old_authors_id = row.old_id;
            end loop;
    end
$$;

alter table author
    drop column old_id;
alter table author
    add primary key (id);

alter table recipe
    drop column old_authors_id;
alter table recipe
    add constraint fk_authors_id foreign key (authors_id) references author;
alter table recipe
    alter column authors_id set not null;
   
---
   
create
materialized view recipe_with_category as
select title, serves, prep_time, cook_time, ingredients, directions, authors_id, dishes, slug, description, r.id as recipe_id, c.id as categorys_id
from recipe r
    join recipe_to_category rc on r.id = rc.recipes_id
    join category c on rc.categorys_id = c.id;

drop index if exists recipe_with_category_id;
create unique index recipe_with_category_id on recipe_with_category (recipe_id, categorys_id);

drop index if exists recipe_with_category_btree;
create index recipe_with_category_btree on recipe_with_category
using btree (prep_time, dishes, slug);

commit;
