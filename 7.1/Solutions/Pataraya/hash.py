from hashlib import sha3_512
from itertools import product

hashes = [
    "eaf23bef2a0744d6889f71526a30e3fd97dbb325e49e7c9b736b38219c67066d1d7878af61a10eea056afa5eb76ca842da9606b28762badf2fdeea081b0f9495",
    "8932856f406e80b3e7770543c4cea1be541b513784bf14d6f57eed73b75f5da0546c74290902472342a1223840aa7fd92ff1db9a93d3b25d65219c2cf36d8307"
]

found = False
pass_length = 0
alph = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

while len(hashes) > 0:
    pass_length += 1
    combinations = product(alph, repeat=pass_length)
    for comb in combinations:
        password = ''.join(comb)
        if sha3_512(password.encode("utf-8")).hexdigest() == hashes[0]:
            print(f"pass for {hashes[0]}: " + password)
            hashes.pop(0)
            pass_length = 0
            break