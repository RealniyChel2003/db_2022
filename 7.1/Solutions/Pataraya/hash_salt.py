from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = 'ce6cd0357d076952b2de39e37253c0eae23ff244baeb44d598703bacda3e6f732cbefe46e3f1f42590ed589a06c12067f58bc617e8237406236836e6b6dd69fd'
salt = '311ecf8c-15cc-4474-9b6a-65d00db203c4'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha3_512((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break