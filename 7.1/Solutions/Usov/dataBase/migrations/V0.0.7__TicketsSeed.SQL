INSERT INTO tickets(film_id, places, price)
values(
        (
            select films.id
            from films
            where films.title = 'The Martian'
        ),
        15,
        1500
    ),
    (
        (
            select films.id
            from films
            where films.title = 'Hessians MC'
        ),
        62,
        1200
    ),
    (
        (
            select films.id
            from films
            where films.title = 'Toy Story 3'
        ),
        23,
        2000
    ),
    (
        (
            select films.id
            from films
            where films.title = 'Once Upon a Time in the West'
        ),
        11,
        1500
    );