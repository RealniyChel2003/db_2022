from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '31ebbea38968166d1b5d3bd7d12f2cf2c97a5444d89506151ad55377'
salt = '0919aff9-1f2a-41d3-a71f-51a9cacc92a9'

found = False
pass_length = 0

if __name__ == '__main__':
    while not found:
        pass_length += 1

        combinations = product(alphabet, repeat=pass_length)

        for combination in combinations:
            password = ''.join(combination)

            if hashlib.sha3_224((password + salt).encode('utf-8')).hexdigest() == hash:
                print(f'password = {password}')
                found = True
                break
