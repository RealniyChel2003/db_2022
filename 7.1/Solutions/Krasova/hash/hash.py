from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'aa4ae0f853d445bf994457fb0ac021efa18061a82a908f30cd44f5c3', '087ef03944114f6e0e3a3460f950aa46f4f171dfd202fc3345853876'}

pass_length = 0


if __name__ == '__main__':
    while len(hashes) > 0:
        pass_length += 1

        combinations = product(alphabet, repeat=pass_length)

        for combination in combinations:
            password = ''.join(combination)
            sha3_224_hash = hashlib.sha3_224(password.encode('utf-8')).hexdigest()

            if sha3_224_hash in hashes:
                print(f'password = {password}, hash = {sha3_224_hash}')
                hashes.remove(sha3_224_hash)

            if len(hashes) == 0:
                break
