namespace FSharpApiSample.Controllers

open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open FSharpApiSample.Services

[<ApiController>]
[<Route("api/suppliers")>]
type SuppliersController(logger: ILogger<SuppliersController>, repository: SuppliersRepository) =
    inherit ControllerBase()

    [<HttpGet>]
    member x.GetSuppliersByCity(city: string) = task {
        try
            let! suppliers = repository.GetSuppliersByCity(city)
            return suppliers |> ActionResult<seq<Suppliers>>
        with
        | err ->
            logger.LogError("GetSuppliersByCity: {err}", err)
            return x.BadRequest() |> ActionResult<seq<Suppliers>>
    }
