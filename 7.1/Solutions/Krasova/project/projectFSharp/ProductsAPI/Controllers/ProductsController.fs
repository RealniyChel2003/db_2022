namespace FSharpApiSample.Controllers

open System.Text.Json
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System
open StackExchange.Redis
open FSharpApiSample.Services

type CreateProductRequest = {
    name: string
    supplier_id: int
    price: int
}

type DeleteProductRequest = {
    id: int
}

[<ApiController>]
[<Route("api/products")>]
type ProductsController(logger: ILogger<ProductsController>,
                        redisConnection: IDatabase, repository: ProductsRepository) =
    inherit ControllerBase()


    [<HttpGet>]
    member x.GetProducts() = task {
        try
            let redisKey = $"/products"
            let! redisValue = redisConnection.StringGetAsync redisKey
            
            if redisValue.HasValue then
                let products = redisValue.ToString() |>JsonSerializer.Deserialize<Product[]>
                return products |> ActionResult<seq<Product>>
            
            else
                let! products = repository.GetAllProducts()
                do! redisConnection
                        .StringSetAsync(redisKey, JsonSerializer.Serialize products, TimeSpan.FromSeconds 30) :> Task

                return products |> ActionResult<seq<Product>>
        with
        | err ->
            logger.LogError("GetProducts: {err}", err)
            return x.BadRequest() |> ActionResult<seq<Product>>
    }
    
    
    [<HttpPost("create")>]
    member x.CreateProduct([<FromBody>] body : CreateProductRequest) = task {
        try
            let! result = repository.CreateProduct(body.name, body.supplier_id, body.price)        
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("CreateProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }

    
    [<HttpPost("update")>]
    member x.UpdateProduct([<FromBody>] body : Product) = task {
        try
            let! result = repository.UpdateProduct(body.id, body.name, body.supplier_id, body.price)         
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("UpdateProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }
    
    
    [<HttpDelete("delete")>]
    member x.DeleteProduct([<FromBody>] body : DeleteProductRequest) = task {
        try
            let! result = repository.DeleteProduct(body.id)             
            return result |> ActionResult<Product>
        with
        | err ->
            logger.LogError("DeleteProduct: {err}", err)
            return x.BadRequest() |> ActionResult<Product>
    }