namespace FSharpApiSample.Controllers

open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open FSharpApiSample.Services

[<ApiController>]
[<Route("api/employees")>]
type EmployeesController(logger: ILogger<EmployeesController>, repository: EmployeesRepository) =
    inherit ControllerBase()

    [<HttpGet>]
    member x.GetEmployeesByUniversityName(university_name: string) = task {
        try
            let! employees = repository.GetEmployeesByUniversityName(university_name)
            return employees |> ActionResult<seq<Employees>>
        with
        | err ->
            logger.LogError("GetEmployeesByUniversityName: {err}", err)
            return x.BadRequest() |> ActionResult<seq<Employees>>
    }
