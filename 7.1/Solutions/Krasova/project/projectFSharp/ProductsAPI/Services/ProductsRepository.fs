namespace FSharpApiSample.Services

open System.Data
open Microsoft.Extensions.Logging
open Dapper

[<CLIMutable>]
type Product = {
    id: int
    name: string
    supplier_id: int
    price: int
}

type ProductsRepository(logger: ILogger<ProductsRepository>, pgConnection :IDbConnection) =
    
    member _.GetAllProducts() = task {
        try
            return! pgConnection.QueryAsync<Product>("Select * from products;")
        with
        | err ->
            logger.LogError("GetAllProducts: {err}", err)
            return failwith err.Message
    }
    
    member _.CreateProduct(name: string, supplierId: int, price: int) = task {
        try
            let query = """
insert into products (name, supplier_id, price) 
values (@name, @supplierId, @price) returning id, name, supplier_id, price;
"""
            let parameters = dict [("name", box name); ("supplierId", supplierId); ("price", price)]
            let! res = pgConnection.QueryAsync<Product>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("CreateProduct: {err}", err)
            return failwith err.Message
    }
    
    member _.UpdateProduct(id: int, name: string, supplierId: int, price: int) = task {
        try
            let query = """
update products 
set name = @name, supplier_id = @supplierId, price = @price 
where id = @id 
returning id, name, supplier_id, price;
"""
            let parameters =  dict [("name", box name); ("supplierId", supplierId); ("price", price); ("id", id)]
            let! res = pgConnection.QueryAsync<Product>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("UpdateProduct: {err}", err)
            return failwith err.Message
    }
    
    member _.DeleteProduct(id: int) = task {
        try
            let query = """
delete from products 
where id = @id
returning id, name, supplier_id, price;
"""
            let parameters =  dict [("id", box id)]
            let! res = pgConnection.QueryAsync<Product>(query, parameters)
            return Seq.head res
        with
        | err ->
            logger.LogError("DeleteProduct: {err}", err)
            return failwith err.Message
    }