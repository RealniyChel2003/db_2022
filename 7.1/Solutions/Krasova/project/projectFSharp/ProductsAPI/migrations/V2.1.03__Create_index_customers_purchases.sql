create index customers_purchases_search_by_min_sum_min_count_purchases on customers_purchases
    using btree (min_sum, min_purchases_count);
