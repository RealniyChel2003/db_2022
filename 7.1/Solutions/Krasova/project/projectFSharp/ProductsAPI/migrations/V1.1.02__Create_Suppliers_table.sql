create table Suppliers
(
    id int generated always as identity primary key,
    name text not null unique
);