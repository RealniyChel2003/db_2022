create extension pg_cron;

select cron.schedule('refresh_customers_purchases', '* * * * *',
                     $$ refresh materialized view concurrently customers_purchases $$);
