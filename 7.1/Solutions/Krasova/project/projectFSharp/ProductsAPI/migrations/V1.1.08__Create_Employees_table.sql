create table Employees
(
    id int generated always as identity primary key,
    shop_id int not null references Shops,
    first_name text not null,
    last_name text not null,
    patronymic text
);
