create table Purchases
(
    id int generated always as identity primary key,
    regular_customers_id int not null references Regular_customers,
    shop_id int not null references Shops
);
