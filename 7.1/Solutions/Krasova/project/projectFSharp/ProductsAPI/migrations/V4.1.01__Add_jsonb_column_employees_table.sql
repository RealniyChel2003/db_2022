alter table Employees add column education jsonb;

update Employees set education = ('[
  {
    "name": "СПбГУ",
    "completion_date": 2015,
    "degree": "bachelor"
  },
  {
    "name": "МГУ",
    "completion_date": 2020,
    "degree": "magister"
  }
]') where Employees.id = 1;

update Employees set education = ('[
  {
    "name": "АГУ",
    "completion_date": 2016,
    "degree": "bachelor"
  },
  {
    "name": "МГУ",
    "completion_date": 2021,
    "degree": "magister"
  }
]') where Employees.id = 2;

update Employees set education = ('[
  {
    "name": "НГУ",
    "completion_date": 2014,
    "degree": "bachelor"
  },
  {
    "name": "НГТУ",
    "completion_date": 2021,
    "degree": "magister"
  }
]') where Employees.id = 3;

create index employee_education on Employees using gin (education);
