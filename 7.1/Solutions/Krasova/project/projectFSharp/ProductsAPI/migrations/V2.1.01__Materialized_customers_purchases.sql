CREATE MATERIALIZED VIEW customers_purchases as with statics_client as (
  select  first_name,
    count (Purchases.regular_customers_id) as count_purchases
  from Regular_customers
    left join Purchases on Purchases.regular_customers_id = Regular_customers.id
  group by    first_name
), statics_client_2 as (
  select  Regular_customers.id,
    first_name,
    last_name,
    patronymic,
    sum(Products.price)
  from Regular_customers
    left join Purchases on Purchases.regular_customers_id = Regular_customers.id
    left join Purchases_to_products on Purchases_to_products.purchase_id = Purchases.id
    left join Products on Products.id = Purchases_to_products.product_id
  group by    Regular_customers.id,
        first_name,
        last_name,
        patronymic
)
select  statics_client_2.id,
        statics_client_2.first_name,
        statics_client_2.last_name,
        statics_client_2.patronymic,
        statics_client.count_purchases as min_purchases_count,
        statics_client_2.sum as min_sum
from statics_client
         left join statics_client_2 on statics_client_2.first_name = statics_client.first_name;


create unique index customers_purchases_id on customers_purchases (id);