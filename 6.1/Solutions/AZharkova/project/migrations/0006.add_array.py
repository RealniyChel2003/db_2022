from yoyo import step


steps = [
   step(
       """
alter table info_calls 
	drop column if exists coordinates; 
        
alter table info_calls 
	add column coordinates text[];

UPDATE info_calls
    SET coordinates = (case when date_call = '01.11.2021' then '{55.7522200, 37.6155600, Москва}'::text[]
	                       when date_call = '11.06.2018' then '{54.5293000, 36.2754200, Калуга}'::text[]
	                       when date_call = '28.02.2020' then '{43.4289600, 39.9239100, Адлер}'::text[]
	                       when date_call = '13.05.2021' then '{59.9386300, 30.3141300, Санкт-Петербург}'::text[]
	                       when date_call = '12.07.2020' then '{51.6720400, 51.6720400, Воронеж}'::text[]
	                       when date_call = '13.07.2020' then '{55.7522200, 37.6155600, Москва}'::text[]
                      end);
        
drop index if exists info_calls_coordinates;
create index info_calls_coordinates on info_calls using gin (coordinates);
    """
   )
]
