from yoyo import step


steps = [
   step(
       """
drop table if exists client, info_calls cascade;

create table client
(
    id       int generated always as identity primary key,
    name     text,
    phone    text,
    address  text,
    birthday text,
    email    text
);

create table info_calls
(
    id              int primary key generated always as identity,
    who_calls_id    int references client not null,
    whom_calls_id   int references client not null,
    unique (whom_calls_id, who_calls_id),
    call_duration   int,
    date_call       text,
    client_category text
);
       """
   )
]
