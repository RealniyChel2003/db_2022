from yoyo import step


steps = [
   step(
       """
insert into client(name, phone, address, birthday, email)
values ('Anastasia', '8910540942', 'Moscow', '03.04.2003', 'nastya.zharkova.03@bk.ru'),
       ('Katya', '89125437865', 'Sochi', '28.11.2003', 'ekaterina03@gmail.com'),
       ('Egor', '89567841745', 'Volgograd', '11.06.1999', 'e_gor2000@yandex.ru'),
       ('Lera', '89106739257', 'Sochi', '15.02.2004', 'lera.0206@mail.ru'),
       ('Arina', '89564447211', 'Kaluga', '11.07.2003', 'arina_bivsheva19@gmail.com'),
       ('Sasha', '89153450906', 'Moscow', '29.06.2004', 'a_aleksandr2001@bk.ru');

insert into info_calls(who_calls_id, whom_calls_id, call_duration, date_call, client_category)
values (1, 2, 60, '01.11.2021', 'Родственник'),
       (5, 6, 120, '11.06.2018', 'Родственник'),
       (4, 3, 200, '28.02.2020', 'Друг'),
       (2, 4, 50, '13.05.2021', 'Коллега'),
       (3, 1, 120, '12.07.2020', 'Друг'),
       (6, 5, 80, '13.07.2020', 'Коллега');
       """
   )
]
