from yoyo import step


steps = [
   step(
       """
alter table client 
	drop column if exists family;

alter table client 
	add column family jsonb;

UPDATE client
    SET family = (case when name = 'Anastasia' then '[{
                                    "name": "Елена",
                                    "phone": "89802345162",
                                    "family_ties": "Мать",
                                    "employment": "Врач",
                                    "age": "39"
								},
                                {
                                    "name": "Никита",
                                    "phone": "89204765324",
                                    "family_ties": "Брат",
                                    "employment": "Студент",
                                    "age": "16"                                    
                                }]'::jsonb

                       when name = 'Katya' then '[{
                                    "name": "Дмитрий",
                                    "phone": "89623452176",
                                    "family_ties": "Отец",
                                    "employment": "Банкир",
                                    "age": "35"
								},
                                {
                                    "name": "Олеся",
                                    "phone": "89345672132",
                                    "family_ties": "Сестра",
                                    "employment": "Школьник",
                                    "age": "12"                                    
                                }]'::jsonb
                       
                       when name = 'Egor' then '[{
                                    "name": "Светлана",
                                    "phone": "89783674523",
                                    "family_ties": "Тётя",
                                    "employment": "Менеджер по продажам",
                                    "age": "30"
								},
                                {
                                    "name": "Александр",
                                    "phone": "89125673908",
                                    "family_ties": "Брат",
                                    "employment": "Студент",
                                    "age": "21"                                    
                                }]'::jsonb

                       when name = 'Lera' then '[{
                                    "name": "Татьяна",
                                    "phone": "89324107843",
                                    "family_ties": "Бабушка",
                                    "employment": "Пенсионер",
                                    "age": "65"
								},
                                {
                                    "name": "Игорь",
                                    "phone": "89234756721",
                                    "family_ties": "Дедушка",
                                    "employment": "Пенсионер",
                                    "age": "68"                                    
                                }]'::jsonb

                       when name = 'Arina' then '[{
                                    "name": "Игорь",
                                    "phone": "89154362745",
                                    "family_ties": "Муж",
                                    "employment": "Юрист",
                                    "age": "40"
								},
                                {
                                    "name": "Кристина",
                                    "phone": "89104387643",
                                    "family_ties": "Дочь",
                                    "employment": "Школьник",
                                    "age": "9"                                    
                                }]'::jsonb

                       when name = 'Sasha' then '[{
                                    "name": "Анастасия",
                                    "phone": "89564736209",
                                    "family_ties": "Жена",
                                    "employment": "PR-менеджер",
                                    "age": "35"
								},
                                {
                                    "name": "Олег",
                                    "phone": "89576847531",
                                    "family_ties": "Сын",
                                    "employment": "Школьник",
                                    "age": "12"                                    
                                }]'::jsonb

 
                end);
                 
drop index if exists client_family;
create index client_family on client using gin (family);      
       """
   )
]
