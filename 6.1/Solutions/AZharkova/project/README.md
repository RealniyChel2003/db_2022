# Проектная работа

В папке создать файл **.env**, добавить туда:
```
APP_NAME=my-app
APP_PORT=27574

POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_PORT=999
POSTGRES_DB=postgres

SLAVE_HOST=185.255.133.90
SLAVE_PORT=38749

REDIS_PASSWORD=redispglaba2022
REDIS_PORT=26596
```

В папке replica создать файл **.env**, добавить туда:
```
SLAVE_PORT=38749

MASTER_PORT=999
MASTER_HOST=185.255.133.84
MASTER_PASSWORD=postgres
```

Запуск проекта:
```
docker compose up
```

Когда контейнеры запустятся, скопировать в адресную строку браузера:
```
185.255.133.84:27574
```

Чтобы проверить, что всё работает, скопируйте из файла "requests" запросы и выполните их в нужном месте.

Запросы Elastic выполнять в Postman.