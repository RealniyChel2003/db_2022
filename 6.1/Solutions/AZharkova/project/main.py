import json, logging, os, psycopg2
from flask import Flask, request
from psycopg2.extras import Json, RealDictCursor
from redis import Redis

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_pg_con_rep():
    pg_conn_rep = psycopg2.connect(host=os.getenv('SLAVE_HOST'), port=os.getenv('SLAVE_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn_rep.autocommit = True
    return pg_conn_rep

def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


@app.route('/clients')
def get_clients():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'clients:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_clients = redis_conn.get(redis_key)

        if redis_clients is None:
            query = """
            select who.id as who_id, who.name as who_calls_name, who.phone as phone, who.address, 
              whom.id as whom_id, whom.name as whom_calls_name, ic.id as ic_id, call_duration, date_call
            from client who
            join info_calls ic on who.id = ic.who_calls_id
            join client whom on whom.id = ic.whom_calls_id
                order by who.id
                limit %s
                offset %s
            """

            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                rows = cur.fetchall()

            redis_clients = json.dumps(rows, ensure_ascii=False, default=str, indent=2)

            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, redis_clients, ex=30)

        return redis_clients, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/client/create', methods=['POST'])
def create_clients():
    try:
        body = request.json
        name = body['name']
        phone = body['phone']
        address = body['address']
        birthday = body['birthday']
        email = body['email']

        query = f"""
        insert into client (name, phone, address, birthday, email)
        values (%s, %s, %s, %s, %s)
        returning name, phone, address, birthday, email
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, phone, address, birthday, email,))
            rows = cur.fetchall()

        return {'message': f'Client {rows[0]["name"]} with phone = {rows[0]["phone"]} created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/client/update', methods=['POST'])
def update_clients():
    try:
        body = request.json
        name = body['name']
        phone = body['phone']

        query = f"""
        update client
        set name = %s
        where phone = %s
        returning phone
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, phone,))
            rows = cur.fetchall()

        return {'message': f'Client with phone = {rows[0]["phone"]} updated.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/delete', methods=['DELETE'])
def delete_holder():
    try:
        body = request.json
        phone = body['phone']

        query = f"""
        delete from client
        where phone = %s
        returning phone
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (phone,))
            rows = cur.fetchall()

        return {'message': f'Client with phone {rows[0]["phone"]} deleted.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

# connection replica 
@app.route('/clients/histogram')
def get_histogram():
    try:
        query=f"""
        with max_time as (select max(call_duration) as time from info_calls),
            histogram as (select width_bucket(call_duration, 0, (select time from max_time), 9) as bucket,
                          count(*) as frequency
                   from info_calls
                   group by bucket)
        select bucket,
               frequency,
               (bucket - 1) * (select time / 10 from max_time) as range_from,
                bucket * (select time / 10 from max_time)       as range_to
        from histogram
        order by bucket;"""

        with get_pg_con_rep() as pg_conn_rep, pg_conn_rep.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()

        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/index_search', methods=['POST'])
def get_index_search():
    try:
        body = request.json
        address = body['address']

        query = f"""
            select name, birthday, address, phone
            from client
            where address = %s
            """           

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (address,))
            rows = cur.fetchall()
   
        return {'message': f'Clients {rows[0]["name"]} live in {rows[0]["address"]}.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/clients/materialized')
def get_materialized():
    try:
        
        query=f"""
    select *
    from client_with_calls;"""


        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()

        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}
    
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/materialized_index', methods=['POST'])
def get_materialized_index():
    try:
        body = request.json
        address = body['address']
        call_duration = body['call_duration']

        query=f"""
            select  who_id, who_calls_name, phone, address, ic_id, date_call, call_duration
            from client_with_calls
            where address = %s and call_duration = %s
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (address, call_duration,))
                rows = cur.fetchall()

        return {'message': f'Client {rows[0]["who_calls_name"]} with call_duration = {rows[0]["call_duration"]} existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/jsonb', methods =['POST'])
def json_db():
    try:
        body = request.json
        name = body['name']
        query_param = [{"name": name}]

        query=f"""
            select *
            from client
            where family @> %s;
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (Json(query_param),))
                rows = cur.fetchall()

        return {'message': f'Client existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/client/array', methods =['POST'])
def array_db():
    try:
        body = request.json
        coordinates = body['coordinates']

        query=f"""
            select *
            from info_calls
            where coordinates && array [%s];
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (coordinates,))        

        return {'message': f'Client existed.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

if __name__ == '__main__':
    app.run(port=8000, host='127.0.0.1')