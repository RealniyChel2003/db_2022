from itertools import product
from Crypto.Hash import SHA512

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'68627fda0f42e608ba8ad53e8a6a484e041082d45260a52545e63d17', '3cef5035975bfaf1cff079193085478d4fef423b98e4baf63b241d86'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        pas = SHA512.new(truncate="224")
        pas.update(password.encode('utf-8'))
        hash = pas.hexdigest()

        if hash in hashes:
            print(f'password = {password}, hash = {hash}')
            hashes.remove(hash)

        if len(hashes) == 0:
            break

# password = g9, hash = 3cef5035975bfaf1cff079193085478d4fef423b98e4baf63b241d86
# password = v7ry, hash = 68627fda0f42e608ba8ad53e8a6a484e041082d45260a52545e63d17