from itertools import product
from Crypto.Hash import SHA512

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = 'f8d76e03b1f58dc7aec064dedcde7b9bd830c4557eb52e373efdecef'
salt = 'a90a0c82-4e9e-4110-a6c2-6716972c4fd7'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        hash_s = SHA512.new(truncate="224")
        hash_s.update((password + salt).encode('utf-8'))


        if hash_s.hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break

# password = FxsR