from flask import Flask, request
import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2.extras import Json
import json
from redis import Redis
import os


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', 
                               port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), 
                               user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), 
                               cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn

def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', 
                 port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), 
                 decode_responses=True)


# curl --location --request GET 'http://127.0.0.1:12862/cars'
@app.route('/cars')
def get_cars():
    try:
        redis_key = 'cars'
        with get_redis_connection() as redis_conn:
            redis_cars = redis_conn.get(redis_key)
        if redis_cars is None:
            query = """
            select brand, model, cost, body_type, equipment, name, date_birth, capital 
            from cars
            left join service_to_car on cars.id = service_to_car.car_id
            left join service on service.id = service_to_car.service_id
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query)
                cars = cur.fetchall()
            json_cars = json.dumps(cars, default=str, ensure_ascii=False, indent=2)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, json_cars, ex=5)
        return json_cars, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

# curl --location --request POST 'http://127.0.0.1:12862/cars/create' --header 'Content-Type: application/json' --data-raw '{"brand":"", "model":"", "cost":, "body_type":"", "equipment":""}'
@app.route('/cars/create', methods=['POST'])
def create_car():
    try:
        body = request.json
        brand = body['brand']
        model = body['model']
        cost = body['cost']
        body_type = body['body_type']
        equipment = body['equipment']
        query = """
        insert into cars(brand, model, cost, body_type, equipment)
        values (%s, %s, %s, %s, %s)
        returning brand, model, cost, body_type, equipment
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (brand, model, cost, body_type, equipment))
                result = cur.fetchall()
        return {'message': f'Car: {result} created.'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

# curl --location --request POST 'http://127.0.0.1:12862/cars/update' --header 'Content-Type: application/json' --data-raw '{"car_id": , "cost": }'
@app.route('/cars/update', methods=['POST'])
def update_car():
    try:
        body = request.json
        car_id = body['car_id']
        cost = body['cost']
        query = """
        update cars
        set cost = %s
        where id = %s
        returning id;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (cost, car_id))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Car with id = {car_id} updated.'}
        else:
            return {'message': f'Car with id = {car_id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

# curl --location --request DELETE 'http://127.0.0.1:12862/cars/delete' --header 'Content-Type: application/json' --data-raw '{"car_id": 6}'
@app.route('/cars/delete', methods=['DELETE'])
def delete_car():
    try:
        body = request.json
        car_id = body['car_id']
        query = """
        delete from cars
        where id = %s
        returning id
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (car_id, ))
                affected_rows = cur.fetchall()
        if len(affected_rows):
            return {'message': f'Car with id = {car_id} deleted.'}
        else:
            return {'message': f'Car with id = {car_id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

# curl --location --request GET 'http://127.0.0.1:12862/services/index_search' --header 'Content-Type: application/json' --data-raw '{"name": "", "date_birth": "", "capital": ""}'
@app.route('/services/index_search')
def clients_search():
    try:
        body = request.json
        name = body['name']
        date_birth = body['date_birth']
        capital = body['capital']
        query = """
        select *
        from service
        where name = %s
        and date_birth = %s
        and capital = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, date_birth, capital))
            services_search = cur.fetchall()
        json_services_search = json.dumps(services_search, default=str, ensure_ascii=False, indent=2)
        return json_services_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

# curl --location --request GET 'http://127.0.0.1:12862/mat_view/index_search' --header 'Content-Type: application/json' --data-raw '{"brand": "", "model": "", "name": ""}'
@app.route('/mat_view/index_search')
def mat_view_search():
    try:
        body = request.json
        brand = body['brand']
        model = body['model']
        name = body['name']
        query = """
        select *
        from service_to_car_link
        where brand = %s
        and model = %s
        and name = %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (brand, model, name))
            mat_view_search = cur.fetchall()
        json_mat_view_search = json.dumps(mat_view_search, default=str, ensure_ascii=False, indent=2)
        return json_mat_view_search, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400
