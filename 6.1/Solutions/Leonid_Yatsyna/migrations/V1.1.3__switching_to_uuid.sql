begin;

create extension if not exists "uuid-ossp";

drop materialized view if exists service_to_car_link;

alter table service_to_car
    drop constraint service_to_car_car_id_fkey;
alter table service_to_car
    rename column car_id to old_car_id;
alter table service_to_car
    add column car_id uuid;

alter table cars
    drop constraint cars_pkey;
alter table cars
    rename column id to old_id;
alter table cars
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from cars
            loop
                update service_to_car set car_id = row.id where old_car_id = row.old_id;
            end loop;
    end
$$;

alter table cars
    drop column old_id;
alter table cars
    add primary key (id);

alter table service_to_car
    drop column old_car_id;
alter table service_to_car
    add constraint fk_car_id foreign key (car_id) references cars;
alter table service_to_car
    alter column car_id set not null;
alter table service_to_car
    add constraint uq_car_id_to_service_id unique (car_id, service_id);

alter table service_to_car
    drop constraint service_to_car_service_id_fkey;
alter table service_to_car
    rename column service_id to old_service_id;
alter table service_to_car
    add column service_id uuid;

alter table affiliate
    drop constraint affiliate_service_id_fkey;
alter table affiliate
    rename column service_id to old_service_id;
alter table affiliate
    add column service_id uuid;

alter table service
    drop constraint service_pkey;
alter table service
    rename column id to old_id;
alter table service
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from service
            loop
                update service_to_car set service_id = row.id where old_service_id = row.old_id;
                update affiliate set service_id = row.id where old_service_id = row.old_id;
            end loop;
    end
$$;

alter table service
    drop column old_id;
alter table service
    add primary key (id);

alter table service_to_car
    drop column old_service_id;
alter table service_to_car
    add constraint fk_service_id foreign key (service_id) references service;
alter table service_to_car
    alter column service_id set not null;

alter table affiliate
    drop column old_service_id;
alter table affiliate
    add constraint fk_service_id foreign key (service_id) references service;

create
materialized view service_to_car_link as
select c.id as car_id, c.brand, c.model, c.cost, c.body_type, c.equipment, s.id as service_id, s.name, s.date_birth, s.capital 
    from cars as c
    left join service_to_car as stc on c.id = stc.car_id
    left join service as s on s.id = stc.service_id;

drop index if exists service_to_car_link_id;
create unique index service_to_car_link_id on service_to_car_link (car_id, service_id);

drop index if exists service_to_car_link_btree;
create index service_to_car_link_btree on service_to_car_link
using btree (brand, model, name);

commit;
