drop materialized view if exists service_to_car_link;

create
materialized view service_to_car_link as
select c.id as car_id, c.brand, c.model, c.cost, c.body_type, c.equipment, s.id as service_id, s.name, s.date_birth, s.capital 
    from cars as c
    left join service_to_car as stc on c.id = stc.car_id
    left join service as s on s.id = stc.service_id;

drop index if exists service_to_car_link_id;
create unique index service_to_car_link_id on service_to_car_link (car_id, service_id);

drop index if exists service_to_car_link_btree;
create index service_to_car_link_btree on service_to_car_link
using btree (brand, model, name);



drop index if exists service_search_by_indexes;
create index service_search_by_indexes on service
using btree (name, date_birth, capital);
