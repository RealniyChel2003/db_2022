drop table if exists service cascade;
drop table if exists affiliate cascade;
drop table if exists cars cascade;
drop table if exists service_to_car cascade;

create table service
(
     id bigint generated always as identity primary key,
     name text not null,
     date_birth  date not null,
     capital int not null

);

create table affiliate
(
     id bigint generated always as identity primary key,
     name text not null,
     location text not null,
     service_id bigint references service
);

create table cars
(
    id bigint generated always as identity primary key,
    brand text not null,
    model text not null,
    cost int not null,
    body_type text not null,
    equipment text not null
);

create table service_to_car
(
   service_id bigint references service,
   car_id bigint references cars,
   primary key (service_id, car_id)
);


insert into service(name, date_birth, capital)
    values ('Avtoprokat', '2014-05-22', '50000000'),
        ('MyCarRental', '2019-08-01', '36450000'),
        ('Rentalcars', '2017-01-18', '7890000');

insert into affiliate(name, location, service_id)
    values ('affiliate1-Avtoprokat', 'Russia', 1),
        ('affiliate2-Avtoprokat', 'France', 1),
        ('affiliate1-MyCarRental', 'Belarus', 2),
        ('affiliate2-MyCarRental', 'Brazil', 2),
        ('affiliate1-Rentalcars', 'Cambodia', 3),
        ('affiliate2-Rentalcars', 'China', 3);

insert into cars(brand, model, cost, body_type, equipment)
    values ('Audi', 'S8', 15000, 'sedan', 'sport+'),
        ('BMW', 'X5', 8000, 'crossover', 'normal'),
        ('Alfa Romeo', 'Giulietta', 10000, 'coupe', 'sport'),
        ('Cadillac', 'Escalade', 10700, 'crossover', 'comfort+'),
        ('Ford', 'Mondeo', 3000, 'sedan', 'base'),
        ('Jeep', 'Grand Cherokee', 9500, 'crossover', 'comfort'),
        ('Honda', 'Accord', 4500, 'sedan', 'base'),
        ('Kia', 'Stinger', 10000, 'sedan', 'sport+'),
        ('Mini','Cooper Countryman',7500,'coupe','normal'),
        ('Opel', 'Astra', 3500, 'hatchback', 'base'),
        ('Porshe', 'Cayman', 15000, 'coupe', 'sport+'),
        ('Rolls-Royce', 'Phantom', 50000, 'sedan', 'maximum');
               
insert into service_to_car(service_id, car_id)
    values  (1,4),
        (1,5),
        (1,6),
        (1,8),
        (1,3),
        (1,10),
        (1,9),
        (2,4),
        (2,7),
        (2,3),
        (2,11),
        (2,9),
        (2,1),
        (2,2),
        (3,1),
        (3,8),
        (3,12),
        (3,11),
        (3,5),
        (3,3);
