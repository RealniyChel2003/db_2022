from crypt import methods
from pydoc import doc
from flask import Flask, request, jsonify, logging
from psycopg2.extras import RealDictCursor, Json
import psycopg2, json, os
from redis import Redis
import os


from psycopg2.extras import RealDictCursor

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)

pg_conn.autocommit = True


def get_pg_con_rep():
    pg_con_rep = psycopg2.connect(host=os.getenv('SLAVE_HOST') or '127.0.0.1', port=os.getenv('SLAVE_PORT'),
                                database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                                password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_con_rep.autocommit = True
    return pg_con_rep


redis_conn = Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                   password=os.getenv('REDIS_PASSWORD'), decode_responses=True)




class Patient:
    def __init__(self, id: int, name: str, age: int):
        self.id = id
        self.name = name
        self.age = age
        self.doctors: list[Doctor] = []

class Doctor:
    def __init__(self, d_fio: str):
        # self.id = id
        self.d_fio = d_fio
        # self.age = age
        # self.sex = sex
        # self.passport = passport

def des(rows):
    patients_dict = {}
    doctors_dict = {}
    for row in rows:
        patient_id = row['patient_id']
        patient_name = row['patient']
        patient_age = row['patient age']

        patient = None
        if patient_id in patients_dict:
            patient = patients_dict[patient_id]
        else:
            patient = Patient(patient_id,patient_name,patient_age)
            patients_dict[patient_id] = patient
        

        doctor_d_fio = row['doctor']

        
        doctor = None
        if doctor_d_fio in doctors_dict:
            doctor = doctors_dict[doctor_d_fio]
        elif doctor_d_fio is not None:
            doctor = Doctor(doctor_d_fio)
            doctors_dict[doctor_d_fio] = doctor
        
        if doctor_d_fio is not None:
            if doctor not in patient.doctors : patient.doctors.append(doctor)
        # if client_id is not None:
        #     if client not in game.clients : game.clients.append(client)
    return list(patients_dict.values())  


@app.route('/patients')
def get_patients():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'patient:offset={offset},limit={limit}'
        redis_patient = redis_conn.get(redis_key)

        if redis_patient is None:
            cur = pg_conn.cursor()
            query="""
            select patient_id,patient.name as "patient", patient.age as "patient age", doctor.d_fio as "doctor"
            from patient
             left join visit on patient.id = visit.patient_id
             left join doctor on visit.doctor_id = doctor.id"""

            cur.execute(query, (offset, limit))
            patients = cur.fetchall()
            cur.close()
            patient = des(patients)
            redis_patients = json.dumps(patients, default=vars, ensure_ascii=False, indent = 2)
            redis_conn.set(redis_key, redis_patients , ex=30)

        return redis_patients, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/patient/create', methods=['POST'])
def create_patient():
    try:
        body = request.json
        name = body['name']
        age = body['age']
        sex = body['sex']
        passport = body['passport']


        cur = pg_conn.cursor()
        query = f"""
        insert into patient (name, age, sex,passport)
        values (%s, %s, %s, %s)
        returning name, age, sex, passport
        """

        cur.execute(query,(name, age, sex, passport))
        result = cur.fetchall()
        cur.close()
        return {'message': f'patient {result[0]["name"]} that is {result[0]["age"]} years old was created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/patient/update', methods=['POST'])
def update_patient():
    try:
        body = request.json
        name = body['name']
        age = body['age']
        passport = body['passport']

        cur = pg_conn.cursor()
        query = f"""
        update patient
        set age = %s
        where name = %s and passport = %s
        returning name, age,passport
        """

        cur.execute(query, (age, name, passport))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'patient {name} updated.'}
        else:
            return {'message': f'patient {name} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/patient/delete', methods=['DELETE'])
def delete_patient():
    try:
        body = request.json
        name = body['name']
        passport = body['passport']

        cur = pg_conn.cursor()
        query = f"""
        delete from patient
        where name = %s and passport = %s
        returning name, passport 
        """

        cur.execute(query, (name,passport))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'patient  {name} deleted.'}
        else:
            return {'message': f'patient {name} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400

@app.route('/index', methods=['POST'])
def index():
    try:
        body = request.json
        age = body['age']
        sex = body['sex']
        status = body['status']

        cur = pg_conn.cursor()
        query = f"""
        select *
        from patient 
        where age = %s and sex = %s and status = %s;
        """

        cur.execute(query, (age, sex, status))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'patient with {age}, {sex} and {status} find.'}
        else:
            return {'message': f'patient with {age}, {sex} and {status} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


#con rep
@app.route('/matherial')
def matherial():
    try:
        query="""
           select *
            from patient_with_doctor;"""

        with get_pg_con_rep() as pg_con_rep, pg_con_rep.cursor() as cur:
            cur.execute(query)
            patients = cur.fetchall()

        redis_patients = json.dumps(patients, default=vars, ensure_ascii=False, indent = 2)

        return redis_patients, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/matherial_index', methods=['POST'])
def matherial_index():
    try:
        body = request.json
        sex = body['sex']

        cur = pg_conn.cursor()
        query = f"""
        select patient, sex, patient_age, doctor, results
        from patient_with_doctor
        where sex = %s'
        """

        cur.execute(query, (sex))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'patient found.'}
        # else:
        #     return {'message': f'patient not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/jsonb', methods=['POST'])
def json_search():
    try:
        body = request.json
        name = body['name']
        query_param = [{"name": name}]

        cur = pg_conn.cursor()
        query = f"""
        select *
        from patient
        where family @> %s;
        """

        cur.execute(query, (Json(query_param), ))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'patient with family {affected_rows} found.'}
        else:
            return {'message': f'patient not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/array', methods=['POST'])
def array_search():
    try:
        body = request.json
        education = body['education']
        
        cur = pg_conn.cursor()
        query = f"""
        select *
        from doctor
        where education && array [%s];
        """

        cur.execute(query, (education,))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'doctor with education {affected_rows} found'}
        else:
            return {'message': f'doctor not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


if __name__ == '__main__':
    app.run(port=8000, host='127.0.0.1')