from yoyo import step


steps = [
   step(
       """
       
-- начало транзакции
begin;

drop materialized view if exists patient_with_doctor;

--rollback;
-- подключаем функцию для генерации uuid (по-умолчанию отключена)
create extension if not exists "uuid-ossp";

    
-- 1) из таблицы, которая ссылаются на doctor, удаляем constraint с внешним ключом
alter table visit
    drop constraint visit_doctor_id_fkey;

-- 2) переименовываем колонку со старым внешним ключом
alter table visit
    rename column doctor_id to old_doctor_id;

-- 3) добавляем колонку с новым внешним ключом
alter table visit
    add column doctor_id uuid;


-- в таблице holder удаляем constraint primary key
alter table doctor
    drop constraint doctor_pkey;

-- переименовываем колонку со старым ключом
alter table doctor
    rename column id to old_id;

-- добавляем колонку с новым ключом
alter table doctor
    add column id uuid default uuid_generate_v4();

/*
анонимная функция, которая циклом обходит все строки таблицы holder,
для каждой строки добавляет новый id, и всем ссылающимся на эту строку
строкам из других таблиц проставляет новый id
*/
do
$$
    declare
        row record;
    begin
        for row in select * from doctor
            loop
                update visit set doctor_id = row.id where old_doctor_id = row.old_id;
            end loop;
    end
$$;

-- удаляем столбец со старым id
alter table doctor
    drop column old_id;
-- устанавливаем первичный ключ
alter table doctor
    add primary key (id);

-- удаляем столбец со старым внешним ключом
-- удаление этой колонки так же удалит установленное ранее ограничение на уникальность
alter table visit
    drop column old_doctor_id;
-- устанавливаем новый внешний ключ
alter table visit
    add constraint fk_doctor_id foreign key (doctor_id) references doctor;
-- для связи many-to-many ставим соответствующие ограничения
alter table visit
    alter column doctor_id set not null;
alter table visit
    add constraint uq_doctor_id_to_patient_id unique (doctor_id, patient_id);


-------------------материализованное представление-----------------------------------------------------

create
materialized view patient_with_doctor as
select visit.id as visit_id, patient_id, doctor.id as doctor_id, patient.name as patient, patient.age as patient_age, sex, doctor.d_fio as doctor, results
    from patient
             left join visit on patient.id = visit.patient_id
             left join doctor on visit.doctor_id = doctor.id;

--select *
--from patient_with_doctor;

drop index if exists patient_with_doctor_id;

create unique index patient_with_doctor_id on patient_with_doctor (visit_id, doctor_id);

refresh
materialized view concurrently patient_with_doctor;
------------------------------------------------------------------------------------------------------

--индекс materialized view --------------------------------------------------------------

create index materialized_search_by_patient_doctor on patient_with_doctor
    using btree (patient_age, sex, results);

-------------------------------------------------------------------------------------------

-- завершение транзакции
commit;

"""
   )
]
