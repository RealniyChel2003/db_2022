from yoyo import step


steps = [
   step(
       """
        drop function if exists refresh_patient_with_doctor;

        create function refresh_patient_with_doctor()
            returns trigger as
        $$
        begin
            refresh materialized view concurrently patient_with_doctor;

            return new;
        end;
        $$
            language 'plpgsql';

        create trigger update_visit_table
            after insert or update or delete
            on visit
            for each row
        execute function refresh_patient_with_doctor();

       """
   )
]
