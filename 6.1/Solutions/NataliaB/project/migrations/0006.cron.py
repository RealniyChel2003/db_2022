from yoyo import step


steps = [
   step(
       """
    create extension pg_cron;

-- refresh представления каждую минуту:
select cron.schedule('refresh_patient_with_doctor', '* * * * *',
                     $$ refresh materialized view concurrently patient_with_doctor $$);

       """
   )
]
