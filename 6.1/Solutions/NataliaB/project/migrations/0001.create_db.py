from yoyo import step


steps = [
   step(
       """
drop table if exists doctor cascade;
drop table if exists patient cascade;
drop table if exists visit cascade;
drop materialized view if exists patient_with_doctor;



create table doctor
(
    id bigint generated always as identity primary key,
    d_fio text,
    experience int,
    passport int not null unique

);

create table patient
(
    id bigint generated always as identity primary key,
    name text,
    age int,
    sex varchar(10),
    status varchar(20),
    passport int not null UNIQUE
);

create table visit
(
    id bigint generated always as identity primary key,
    patient_id bigint not null references patient,
    doctor_id bigint not null references doctor,
    date timestamp,
    results text
);

insert into doctor (d_fio, experience, passport)
values ('Филатов Владимир Петрович', 15, 03937674),
       ('Боткин Сергей Петрович', 9, 62895748),
       ('Склифосовский Николай Васильевич', 5, 93736829);

insert into patient (name, age, sex, status, passport)
values ('Пупкин Василий Васильевич', 32, 'male', 'single', 12345),
       ('Петров Петр Петрович', 26, 'male', 'married', 54321),
       ('Петрова Полина Павловна', 24, 'female', 'married', 14325),
       ('Иванов Иван Иванович', 46, 'male', 'single', 11423);

insert into visit (patient_id, doctor_id, date, results)
values (1, 1, '2022-04-26 15:00', 'диагноз: хронический панкреатит'),
       (2, 2, '2022-04-18 12:00', 'диагноз: COVID-19'),
       (3, 2, '2022-04-18 13:00', 'диагноз: COVID-19'),
       (4, 3, '2022-05-06 9:00', 'диагноз: атопический дерматит');



------------индекс-------------------

drop index if exists patient_search_by_age_sex_status;
create index patient_search_by_age_sex_status on patient
    using btree (age, sex, status);

--select *
--from patient 
--where age = '32' and sex = 'male' and status = 'single';
--------------------------------------------------

-------------------материализованное представление-----------------------------------------------------

create
materialized view patient_with_doctor as
select visit.id as visit_id, patient_id, doctor.id as doctor_id, patient.name as patient, patient.age as patient_age, sex, doctor.d_fio as doctor, results
    from patient
             left join visit on patient.id = visit.patient_id
             left join doctor on visit.doctor_id = doctor.id;

--select *
--from patient_with_doctor;

drop index if exists patient_with_doctor_id;

create unique index patient_with_doctor_id on patient_with_doctor (visit_id, doctor_id);

refresh
materialized view concurrently patient_with_doctor;
------------------------------------------------------------------------------------------------------

--индекс materialized view --------------------------------------------------------------

create index materialized_search_by_patient_doctor on patient_with_doctor
    using btree (patient_age, sex, results);

--select patient, patient_age, doctor, results
--from patient_with_doctor
--where patient_age = '26' and sex = 'male';
-------------------------------------------------------------------------------------------
       """
   )
]
