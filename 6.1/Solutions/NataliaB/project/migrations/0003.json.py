from yoyo import step


steps = [
   step(
       """
alter table patient 
	drop column if exists family;

alter table patient 
	add column family jsonb;


UPDATE patient
    SET family = (case when name = 'Пупкин Василий Васильевич' then '[{
                                    "name": "Пупкина Елена Викторовна",
                                    "phone": "89802345162",
                                    "family_ties": "Жена",
                                    "age": "30"
								},
                                {
                                    "name": "Пупкина Нина Григорьевна",
                                    "phone": "89204765324",
                                    "family_ties": "Мать",
                                    "age": "54"                                    
                                }]'::jsonb

                       when name = 'Петров Петр Петрович' then '[{
                                    "name": "Петрова Полина Павловна",
                                    "phone": "89623452176",
                                    "family_ties": "Жена",
                                    "age": "24"
								},
                                {
                                    "name": "Петров Никита Петрович",
                                    "phone": "-",
                                    "family_ties": "Сын",
                                    "age": "2"                                    
                                }]'::jsonb

                       when name = 'Петрова Полина Павловна' then '[{
                                    "name": "Петров Петр Петрович",
                                    "phone": "89783674523",
                                    "family_ties": "Муж",
                                    "age": "26"
								},
                                {
                                    "name": "Петров Никита Петрович",
                                    "phone": "-",
                                    "family_ties": "Сын",
                                    "age": "2"                                    
                                }]'::jsonb

                       when name = 'Иванов Иван Иванович' then '[{
                                    "name": "Иванова Анастасия Юрьевна",
                                    "phone": "89564736209",
                                    "family_ties": "Жена",
                                    "age": "40"
								},
                                {
                                    "name": "Иванова Ирина Ивановна",
                                    "phone": "89576847531",
                                    "family_ties": "Дочь",
                                    "age": "17"                                    
                                }]'::jsonb
 
                end);
                 
drop index if exists patient_family;
create index patient_family on patient using gin (family);   

       """
   )
]
