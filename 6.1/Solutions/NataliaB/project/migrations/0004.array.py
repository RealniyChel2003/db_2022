from yoyo import step


steps = [
   step(
       """
alter table doctor 
	drop column if exists education; 
        
alter table doctor 
	add column education text[];

UPDATE doctor
    SET education = (case when d_fio = 'Филатов Владимир Петрович' then '{"Первый МГМУ имени И. М. Сеченова", "Лечебное дело", "Офтальмолог"}'::text[]
	                       when d_fio = 'Боткин Сергей Петрович' then '{"РНИМУ имени Н.И. Пирогова", "Лечебное дело", "Терапевт"}'::text[]
	                       when d_fio = 'Склифосовский Николай Васильевич' then '{"ПСПбГМУ им. акад. И.П. Павлова", "Лечебное дело", "Хирург"}'::text[]
                      end);
        
drop index if exists doctor_education;
create index doctor_education on doctor using gin (education);

       """
   )
]
