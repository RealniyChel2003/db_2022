from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = 'fd215af0920518aded09caa46611f39c30348cffeb29b58dbd5bc607'
salt = '4ab345fe-fbb6-44c1-9b2a-99c50d878599'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha224((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
