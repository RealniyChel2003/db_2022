from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'fa5e4779af6b482abb80d777a7e424639bfb5b9fb3b50191a6142e75', '43878de9fdee525e058e547c9c2a34237d16b6410d8b5d4e7bf14493'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        sha224hash = hashlib.sha224(password.encode('utf-8')).hexdigest()

        if sha224hash in hashes:
            print(f'password = {password}, hash = {sha224hash}')
            hashes.remove(sha224hash)

        if len(hashes) == 0:
            break
