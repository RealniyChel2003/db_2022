drop index if exists worker_search_by_name_age;
create index worker_search_by_name_age on worker
using btree (f_name, s_name, age);
