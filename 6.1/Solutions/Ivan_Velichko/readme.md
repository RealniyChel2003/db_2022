# Сборка приложения:
~ вначале нужно создать файл .env с переменными:
APP_NAME, APP_PORT, POSTGRES_PORT, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD, REDIS_PORT, REDIS_PASSWORD
```bash
# сборка и запуск приложения
docker compose up --build
# остановить и удалить
docker compose down
# запустить существующее приложение
docker compose start
# остановить приложение
docker compose stop
# удалить приложение
docker compose rm
```

# Запросы:
```bash
# получить все компании с работниками
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/companies?offset=0&limit=100'
# обновить название компании по id
curl --location --request POST 'http://127.0.0.1:{APP_PORT}/companies/update' --header 'Content-Type: application/json' --data-raw '{"id": , "title": ""}'
# удалить компанию по id
curl --location --request DELETE 'http://127.0.0.1:{APP_PORT}/companies/delete' --header 'Content-Type: application/json' --data-raw '{"id": }'
# создание компании
curl --location --request POST 'http://127.0.0.1:{APP_PORT}/companies/create' --header 'Content-Type: application/json' --data-raw '{"title": "", "founded": "", "field": ""}'
# получение возрастной гистограммы работников
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/workers/age_histogram'
# быстрая выборка работников(по диапазону возрастов) с помощью индексов
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/workers/index_search' --header 'Content-Type: application/json' --data-raw '{"age_from": "", "age_to": ""}'
# быстрая выборка из материализованного представления с помощью индексов (по возрасту работника, дате основания и стоимости компании)
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/mat_view/index_search' --header 'Content-Type: application/json' --data-raw '{"age_from": "", "age_to": "", "founded_from": "", "founded_to": "", "ent_value_from": "", "ent_value_to": ""}'
# поиск работников по должности (поиск по колонке типа jsonb)
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/workers/json_position' --header 'Content-Type: application/json' --data-raw '{"position": ""}'
# поиск компаний по филиалу в городе (поиск по колонке типа text[])
curl --location --request GET 'http://127.0.0.1:{APP_PORT}/companies/array_search' --header 'Content-Type: application/json' --data-raw '{"city": ""}'
```