from yoyo import step


steps = [
   step(
    """
    create function refresh_games_mater()
        returns trigger as
            $$
            begin
                refresh materialized view concurrently games_mater;

                return new;
            end;
            $$
        language 'plpgsql';

    create trigger update_refresh_games_mater
        after insert or update or delete
        on games
        for each row
    execute function refresh_games_mater();

   """)
]