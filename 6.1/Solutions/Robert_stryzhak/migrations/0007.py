from yoyo import step


steps = [
   step(
       """
        drop index if exists third_index;
         create  index third_index on reviews ("mark", "commentator", "body");
        """
   )
]
