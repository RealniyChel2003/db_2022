
from yoyo import step


steps = [
   step("""

update games
    set series = '{Forza 5, Forza 4, Forza 3}'
where name = 'Forza';

update games
    set series = '{World of Tanks, Мир танков, World of warplanes}'
where name = 'wot';

update games
    set series = '{1.18, 1.19, 1.15}'
where name = 'maincampf';

update games
    set series = '{Modern Warfare, Modern Warfare 2, Black Ops Cold War, Warzone, MObile}'
where name = 'call of duty';

create index games_series on games using gin (series);
   """)
]