from yoyo import step


steps = [
   step(
       """
       create
materialized view games_mater as
select games.name as "Gamename", games.id as "Game_id",price,developer as "GameDev", "genre",reviews.id as "rev_id",reviews.body as "Review", reviews.mark as "rev_mark", c.name as "Cient Name", c.id as "Client_id" , c.age as "Client age" from games
            left join reviews on "game" = games.id
            left join client2game  c2g on games.id = c2g.game_id
            left join client c on c.id = reviews.commentator
group by c.name, games.id, developer, "genre", reviews.id, reviews.body, reviews.mark, games.name, c.id
        """
   )
]
