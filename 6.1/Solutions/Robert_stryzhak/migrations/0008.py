from yoyo import step


steps = [
   step(
       """
        
begin;
DROP MATERIALIZED VIEW if exists games_mater;
create extension if not exists "uuid-ossp";

alter table reviews
    drop constraint reviews_commentator_fkey;
alter table reviews
    rename column commentator to old_commentator;
alter table reviews
    add column commentator uuid;

alter table reviews
    drop constraint reviews_game_fkey;
alter table reviews
    rename column game to old_game;
alter table reviews
    add column game uuid;


alter table client2game
    drop constraint client2game_game_id_fkey;
alter table client2game
    rename column game_id to old_game_id;
alter table client2game
    add column game_id uuid;

alter table client2game
    drop constraint client2game_id_client_fkey;
alter table client2game
    rename column id_client to old_id_client;
alter table client2game
    add column id_client uuid;

alter table games
    drop constraint games_pkey;

alter table games
    rename column id to old_id;

alter table games
    add column id uuid default uuid_generate_v4();

alter table client
    drop constraint client_pkey;

alter table client
    rename column id to old_id;

alter table client
    add column id uuid default uuid_generate_v4();


alter table reviews
    drop constraint reviews_pkey;

alter table reviews
    rename column id to old_id;

alter table reviews
    add column id uuid default uuid_generate_v4();


do
$$
    declare
        row record;
    begin
        for row in select * from games
            loop
                update client2game set game_id = row.id where old_game_id = row.old_id;
                update reviews set game = row.id where reviews.old_game = row.old_id;
            end loop;
    end
$$;
do
$$
    declare
        row record;
    begin
        for row in select * from client
            loop
                update client2game set id_client = row.id where client2game.old_id_client = row.old_id;
                update reviews set commentator = row.id where reviews.old_commentator = row.old_id;
            end loop;
    end
$$;




alter table games
    drop column old_id cascade ;

alter table games
    add primary key (id);

alter table client
    drop column old_id cascade ;

alter table client
    add primary key (id);

alter table reviews
    drop column old_id cascade ;

alter table reviews
    add primary key (id);

alter table reviews
    drop column old_commentator;
alter table reviews
    add constraint fk_commentator foreign key (commentator) references client;

alter table reviews
    drop column old_game;

alter table reviews
    add constraint fk_game foreign key (game) references games;

alter table client2game
    drop column old_game_id;
alter table client2game
    drop column old_id_client;

alter table client2game
    add constraint fk_id_client foreign key (id_client) references client;
alter table client2game
    add constraint fk_game_id foreign key (game_id) references games;



create
materialized view games_mater as
select games.name as "Gamename", games.id as "Game_id",price,developer as "GameDev", "genre",reviews.id as "rev_id",reviews.body as "Review", reviews.mark as "rev_mark", c.name as "Cient Name", c.id as "Client_id" , c.age as "Client age" from games
            left join reviews on "game" = games.id
            left join client2game  c2g on games.id = c2g.game_id
            left join client c on c.id = reviews.commentator
group by c.name, games.id, developer, "genre", reviews.id, reviews.body, reviews.mark, games.name, c.id;



        create  index third_index on reviews ("mark", "commentator", "body");
        create  index new_index on games_mater ("rev_mark", "Client age");         
        create unique index game_nam on games_mater ("Client_id", "Game_id", "rev_id");

        set enable_seqscan = false;
commit;
        """
   )
]
