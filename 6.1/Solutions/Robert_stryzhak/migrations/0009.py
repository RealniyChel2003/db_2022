
from yoyo import step


steps = [
   step("""
   
    ALTER TABLE client
    ADD  column if not exists old_games jsonb;

update client
    set old_games = '[{"name": "Forza 3", "hours": 100, "difficult": "Suicide"}, {"name": "Call of duty MW", "hours": 2000, "difficult": "Hard"}, {"name": "Dota 2", "hours": 10000, "difficult": "None"}, {"name": "Wolfenshtein", "hours": 1234, "difficult": "ddd"}]'
where name = 'robert' and age ='12';

update client
    set old_games = '[{"name": "Elden Ring.", "hours": 1030, "difficult": "Suicide"}, {"name": "Dark Souls", "hours": 2000, "difficult": "Hard"}, {"name": "Cuphead", "hours": 40, "difficult": "Super Hard"}]'
where name = 'ars' and age ='15';

update client
    set old_games = '[{"name": "Elden Ring.", "hours": 10, "difficult": "Easy"}, {"name": "The Binding of Isaac", "hours": 200, "difficult": "Hard"}, {"name": "Cuphead", "hours": 400, "difficult": "Super Hard"}]'
where name = 'Germ' and age ='26';

update client
    set old_games = '[{"name": "Super Meat Boy Forever", "hours": 150, "difficult": "Extreme"}, {"name": "Sekiro: Shadows Die Twice", "hours": 1234, "difficult": "Really hard"}, {"name": "The Binding of Isaac: Rebirth", "hours": 100, "difficult": "Super Hard"}]'
where name = 'andrei' and age = '17';

update client
    set old_games = '[{"name": "S.T.A.L.K.E.R.: Shadow of Chernobyl", "hours": 700, "difficult": "Medium"}, {"name": "Sekiro: Shadows Die Twice", "hours": 14, "difficult": "Really easy"}, {"name": "Fallout 4", "hours": 240, "difficult": "Medium"}, {"name": "God of war", "hours": 60, "difficult": "Extreme"}]'
where name = 'arseniy' and age = '10';

update client
    set old_games = '[{"name": "CS:GO", "hours": 7000, "difficult": "None"}, {"name": "Dota 2", "hours": 14000, "difficult": "None"}]'
where name = 'keril' and age = '20';

update client
    set old_games = '[{"name": "WOT", "hours": 500, "difficult": "None"}, {"name": "Forza 3", "hours": 200, "difficult": "Hard"}, {"name": "NFS: MW", "hours": 400, "difficult": "Hard"}, {"name": "NFS: Hot pursit", "hours": 100, "difficult": "Hard"}]'
where name = 'magic aboba' and age = '120';

create  index played_games on client(id,name,old_games); 

create index client_old_games on client using gin (old_games);



   """)
]
