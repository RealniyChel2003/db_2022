from yoyo import step


steps = [
   step(
       """
       create extension pg_cron;

-- refresh представления каждую минуту:
select cron.schedule('refresh_games_mater', '* * * * *',
                     $$ refresh materialized view concurrently games_mater $$);

       """
   )
]
