from flask import Flask, jsonify, request
import psycopg2
from redis import Redis
import json
import os
from psycopg2.extras import RealDictCursor

from deserialization import deserialize_artist

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1',
                               port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'),
                               user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'),
                               cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn


def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1',
                 port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'),
                 decode_responses=True)


@app.route('/artists')
def get_artists():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'artists:offset={offset},limit={limit}'
        with get_redis_connection() as redis_conn:
            redis_artists = redis_conn.get(redis_key)

        if redis_artists is None:
            query = """
            with limited_artists as (select * from artists offset %s limit %s)
            select limited_artists.id, limited_artists.name as artist_name, limited_artists.birthday as artist_birthday, musical_composition.title as musical_composition_title,
            musical_composition.duration as musical_composition_duration, musical_composition.genre as musical_composition_genre,
            tours.name as tour_name, tours.city as tour_city, tours.date as tours_date
            from limited_artists
                left outer join tours on limited_artists.id = tours.artist_id
                left join artists_to_composition on limited_artists.id = artists_to_composition.artist_id
                left outer join musical_composition on musical_composition.id = artists_to_composition.composition_id;
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset, limit))
                rows = cur.fetchall()
            json_res = json.dumps(rows, ensure_ascii=False, indent=2, default=str)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, json_res, ex=1)
            return json_res, 200, {'content-type': 'text/json'}

        else:
            return jsonify(json.loads(redis_artists))

    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/artists/create', methods=['POST'])
def create_artists():
    try:
        body = request.json
        name = body['name']
        birthday = body['birthday']
        query = """
            insert into artists (name, birthday)
            values (%s, %s)
            returning name, birthday;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, birthday))
            result = cur.fetchall()
        return {'message': f'Artists {result[0]["name"]} with birthday = {result[0]["birthday"]} created.'}
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/artists/update', methods=['POST'])
def update_artists():
    try:
        body = request.json
        name = body['name']
        birthday = body['birthday']

        query = f"""
        update artists
        set name = %s
        where birthday = %s
        returning birthday
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, birthday))
            affected_rows = cur.fetchall()

        if len(affected_rows):
            return {'message': f'Artists with birthday = {birthday} updated.'}
        else:
            return {'message': f'Artists with birthday = {birthday} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/artists/delete', methods=['DELETE'])
def delete_artists():
    try:
        body = request.json
        birthday = body['birthday']

        query = f"""
        delete from artists
        where birthday = %s
        returning birthday
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (birthday,))
            affected_rows = cur.fetchall()

        if len(affected_rows):
            return {'message': f'Artists with birthday = {birthday} deleted.'}
        else:
            return {'message': f'Artists with birthday = {birthday} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/index_composition')
def get_index():
    try:
        body = request.json
        duration = body['duration']
        genre = body['genre']
        title = body['title']
        query = """
                SELECT * FROM musical_composition WHERE genre = %s AND title = %s AND duration = %s;
                """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (genre, title, duration))
            res = cur.fetchall()
        json_res = json.dumps(res, ensure_ascii=False, indent=2, default=str)
        return json_res, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/index_view')
def get_index_view():
    try:
        body = request.json
        name = body['name']
        genre = body['genre']
        title = body['title']
        print(name, title, genre)
        query = f"""
                SELECT * FROM artists_with_composition WHERE name = %s AND genre = %s AND title = %s;
                """
        print(query)
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, genre, title))
            res = cur.fetchall()
        json_res = json.dumps(res, ensure_ascii=False, indent=2, default=str)
        return json_res, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

#  добавляю api метод поиска по jsonb (education)

@app.route('/json_search_education')
def get_index_education():
    try:
        body = request.json
        university = body['university']
        query_param = [{"name": university}]
        query = """
            select *
            from artists
            where education @> %s;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (Json(query_param), ))
            res = cur.fetchall()

        json_res = json.dumps(res, ensure_ascii=False, indent=2, default=str)
        return json_res, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

#  добавляю api метод поиска по массивам (instruments)

@app.route('/array_search_instrument')
def get_index_instruments():
    try:
        body = request.json
        instrument = body['instrument']
        query = """
            select *
            from artists
            where instruments && array [%s];
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (instrument, ))
            res = cur.fetchall()
        json_res = json.dumps(res, ensure_ascii=False, indent=2, default=str)
        return json_res, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

if __name__ == "__main__":
    app.run(debug=True)
