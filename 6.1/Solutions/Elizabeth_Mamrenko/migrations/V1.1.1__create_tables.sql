drop table if exists artists cascade;
drop table if exists musical_composition cascade;
drop table if exists tours cascade;
drop table if exists artists_to_composition cascade;

create table if not exists artists
(
    id       int generated always as identity primary key,
    name     text,
    birthday timestamptz
);

create table if not exists musical_composition
(
    id       int generated always as identity primary key,
    title    text,
    duration int,
    genre    text
);

create table if not exists artists_to_composition
(
    artist_id      int references artists on delete cascade not null,
    composition_id int references musical_composition       not null,
    unique (artist_id, composition_id)
);

create table if not exists tours
(
    id        int generated always as identity primary key,
    name      text,
    city      text,
    date      timestamptz,
    artist_id int references artists on delete cascade
);
