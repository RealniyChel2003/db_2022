begin;

create extension if not exists "uuid-ossp";


alter table artists_to_composition
    drop constraint artists_to_composition_artist_id_fkey;

alter table artists_to_composition
    rename column artist_id to old_artist_id;

alter table artists_to_composition
    add column artist_id uuid;

alter table tours
    drop constraint tours_artist_id_fkey;

alter table tours
    rename column artist_id to old_artist_id;

alter table tours
    add column artist_id uuid;


alter table artists
    drop constraint artists_pkey;

alter table artists
    rename column id to old_id;

alter table artists
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from artists
            loop
                update artists_to_composition set artist_id = row.id where old_artist_id = row.old_id;
                update tours set artist_id = row.id where old_artist_id = row.old_id;
            end loop;
    end
$$;
select * from artists;
select * from artists_to_composition;

alter table artists
    drop column old_id cascade;

alter table artists
    add primary key (id);

alter table artists_to_composition
    drop column old_artist_id;

alter table artists_to_composition
    add constraint fk_artist_id foreign key (artist_id) references artists;

alter table artists_to_composition
    alter column artist_id set not null;
alter table artists_to_composition
    add constraint uq_artist_id_to_composition_id unique (artist_id, composition_id);

alter table tours
    drop column old_artist_id;

alter table tours
    add constraint fk_artist_id foreign key (artist_id) references artists;


create materialized view artists_with_composition as
select
    artists.id artists_id,
    artists.name,
    artists.birthday,
    musical_composition.id musical_composition_id,
    musical_composition.title,
    musical_composition.duration,
    musical_composition.genre
from artists
    left join artists_to_composition on artists.id = artist_id
    left join musical_composition on musical_composition.id = composition_id;

create unique index artists_with_composition_id on artists_with_composition (artists_id, musical_composition_id);
create index artists_with_composition_btree on artists_with_composition
using btree (name, genre, title);

-- завершение транзакции
commit;
