drop index if exists index_composition;
create index index_composition on musical_composition
    using btree (title, duration, genre);


drop index if exists artists_with_composition_id;
drop index if exists artists_with_composition;

drop materialized view if exists artists_with_composition;
create materialized view artists_with_composition as
select
    artists.id artists_id,
    artists.name,
    artists.birthday,
    musical_composition.id musical_composition_id,
    musical_composition.title,
    musical_composition.duration,
    musical_composition.genre
from artists
    left join artists_to_composition on artists.id = artist_id
    left join musical_composition on musical_composition.id = composition_id;

-- Вот индекс. Тут начинается
drop index if exists artists_with_composition_id;
create unique index artists_with_composition_id on artists_with_composition (artists_id, musical_composition_id);
-- Тут заканчивается


-- добавить индекс по материализованному представлению на два свойства, чтобы можно было по ним искать
create index artists_with_composition_btree on artists_with_composition
    using btree (name, genre, title);

