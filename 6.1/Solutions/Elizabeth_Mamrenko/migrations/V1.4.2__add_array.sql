alter table artists
	drop column if exists instruments;

alter table artists
	add column instruments text[];

UPDATE artists
    SET instruments = (case when name = 'Луи Армстронг' then '{trumpet, drums}'::text[]
                       when name = 'Сергей Рахманинов' then '{guitar, piano}'::text[]
                       when name = 'Эминем' then '{violin, cello}'::text[]
                end);

create index artists_instruments on artists using gin (instruments);