alter table artists
	drop column if exists education;

alter table artists
	add column education jsonb;

UPDATE artists
    SET education = (case when name = 'Луи Армстронг' then '[
                                    {"name": "МГК имени Чайковского",
                                    "degree": "bachelor",
                                    "completion_date": 1917
                                    }]'::jsonb
                     when name = 'Сергей Рахманинов' then '[
									{"name": "КФУ",
                                    "degree": "magister",
                                    "completion_date": 2014
                                    }]'::jsonb
                     when name = 'Эминем' then '[{
									"name": "СПбГУ",
									"degree": "bachelor",
									"completion_date": 2015
								}]'::jsonb
                end);

create index artists_education on artists using gin (education);