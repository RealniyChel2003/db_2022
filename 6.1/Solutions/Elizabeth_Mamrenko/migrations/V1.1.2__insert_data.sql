insert into artists (name, birthday)
values ('Луи Армстронг', '1901-08-04 10:00:00+03'),
       ('Сергей Рахманинов', '1873-04-1 14:00:00+03'),
       ('Эминем', '1972-10-17 10:00:00+03');

insert into musical_composition (title, duration, genre)
values ('What A Wonderful World', 144, 'jazz'),
       ('Сирень', 150, 'classical music'),
       ('Rap God', 360, 'rep');

insert into artists_to_composition(artist_id, composition_id)
values (1, 1),
       (2, 2),
       (3, 3);

insert into tours(name, city, date, artist_id)
values ('Выступление', 'Голливуд', '1930-01-05 20:00:00+03', 1),
       ('Сольный вечер', 'Лос-Анджелес', '1930-01-05 20:00:00+03', 2),
       ('Лучший репер 21 века', 'Нью-Йорк', '2001-10-21 19:00:00+03', 3);