# Это домашние задания студентки группы 2.11.6.1 Елизаветы Мамренко

# Основные команды
Сборка и запуск приложения:
```
docker compose up --build
```

Остановить и удалить:
```
docker compose down
```

Запустить существующее приложение:
```
docker compose start
```

Остановить приложение:
```
docker compose stop
```
Удалить приложение:
```
docker compose rm
```

# Проверить, что все работает:

Для метода /artists
```
curl --location --request GET 'http://127.0.0.1:12862/artists?offset=0&limit=100'
```
Для метода /artists/create 
```
curl --location --request POST 'http://127.0.0.1:12862/artists/create' --header 'Content-Type: application/json' --data-raw '{name": "Bach", "birthday": "1785-04-01 11:00:00+00:00"}'
```
Для метода /artists/update
```
curl --location --request POST 'http://127.0.0.1:12862/artists/update' --header 'Content-Type: application/json' --data-raw '{"name": "Bach", "birthday": "1901-08-04 10:00:00+03"}'
```
Для метода /artists/delete
```
curl --location --request DELETE 'http://127.0.0.1:12862/artists/delete' --header 'Content-Type: application/json' --data-raw '{"birthday": "1785-04-01 11:00:00+00:00"}'
```
Для метода /index_composition
```
curl --location --request GET 'http://127.0.0.1:12862/index_composition' --header 'Content-Type: application/json' --data-raw '{"title": "Rap God", "duration": "360", "genre": "rep"}'
```
Для метода /index_view
```
curl --location --request GET 'http://127.0.0.1:12862/index_view' --header 'Content-Type: application/json' --data-raw '{"name": "Сергей Рахманинов", "genre": "classical music", "title": "Сирень"}'
```
Для метода /json_search_education:
```
curl --location --request GET 'http://127.0.0.1:12862/json_search_education' --header 'Content-Type: application/json' --data-raw '{"university": "МГК имени Чайковского"}'
```
Для метода /array_search_instrument:
```
curl --location --request GET 'http://127.0.0.1:12862/array_search_instrument' --header 'Content-Type: application/json' --data-raw '{"instrument": "piano"}'
```
