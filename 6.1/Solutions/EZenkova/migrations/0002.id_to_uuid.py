from yoyo import step

steps = [
   step(
       """
------------------------------------------------------------------------------------------
-- начало транзакции
begin;

-- подключаем функцию для генерации uuid (по умолчанию отключена)
create extension if not exists "uuid-ossp";


-- 1) из таблицы, которая ссылаются на "user", удаляем constraint с внешним ключом
alter table video_and_user
    drop constraint video_and_user_user_id_fkey;


-- 2) переименовываем колонку со старым внешним ключом
alter table video_and_user
    rename column user_id to old_user_id;


-- 3) добавляем колонку с новым внешним ключом
alter table video_and_user
    add column user_id uuid;


-- проделываем пункты 1-2-3 для всех таблиц, которые ссылаются на "user"
 alter table comments
     drop constraint comments_user_id_fkey;

  alter table comments
      rename column user_id to old_user_id;

  alter table comments
      add column user_id uuid;


-- в таблице user удаляем constraint primary key
 alter table "user"
     drop constraint user_pkey;


-- переименовываем колонку со старым ключом
 alter table "user"
     rename column id to old_id;


-- добавляем колонку с новым ключом
  alter table "user"
      add column id uuid default uuid_generate_v4();


/*
анонимная функция, которая циклом обходит все строки таблицы "user",
для каждой строки добавляет новый id, и всем ссылающимся на эту строку
строкам из других таблиц проставляет новый id
*/
do
$$
    declare
        row record;
    begin
        for row in select * from "user"
            loop
                update video_and_user set user_id = row.id where old_user_id = row.old_id;
                update comments set user_id = row.id where old_user_id = row.old_id;
            end loop;
    end
$$;

drop materialized view if exists video_with_user;

-- удаляем столбец со старым id
alter table "user"
    drop column old_id;
-- устанавливаем первичный ключ
alter table "user"
    add primary key (id);


-- удаляем столбец со старым внешним ключом
-- удаление этой колонки так же удалит установленное ранее ограничение на уникальность
alter table video_and_user
    drop column old_user_id;


-- устанавливаем новый внешний ключ
alter table video_and_user
    add constraint fk_user_id foreign key (user_id) references "user";


-- для связи many-to-many ставим соответствующие ограничения
alter table video_and_user
    alter column user_id set not null;

alter table video_and_user
    add constraint uq_user_id_to_video_id unique (user_id, video_id);


-- удаляем столбец со старым внешним ключом
alter table comments
    drop column old_user_id;
        
-- устанавливаем новый внешний ключ   
alter table comments
    add constraint fk_user_id foreign key (user_id) references "user";


-- материализованное представление -------------------------------------------------------------------------------
  
create materialized view video_with_user as
select "user".id, nickname, video_and_user.user_id as uid, title, comment, comments.id as comment_id
from "user" 
join video_and_user on "user".id = video_and_user.user_id
join video on video_and_user.video_id = video.id
join comments on video.id = comments.video_id;

 select *
 from video_with_user;

drop index if exists video_with_user_id;

select * from video_with_user;

create unique index video_with_user_id on video_with_user (id, uid, comment_id);

refresh
materialized view concurrently video_with_user;

------------------------------------------------------------------------------------------

-- завершение транзакции
commit;
        """
    )
]