from yoyo import step


steps = [
   step(
       """
   create extension pg_cron;

   -- refresh представления каждую минуту:
   select cron.schedule('refresh_video_with_user', '* * * * *',
                        $$ refresh materialized view concurrently video_with_user $$);
    """
   )
]
