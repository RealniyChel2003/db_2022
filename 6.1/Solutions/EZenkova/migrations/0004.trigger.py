from yoyo import step


steps = [
   step(
       """
    drop function if exists refresh_video_with_user;
    create function refresh_video_with_user()
        returns trigger as
    $$
    begin
        refresh materialized view concurrently video_with_user;

        return new;
    end;
    $$
        language 'plpgsql';

    create trigger update_user_table
        after insert or update or delete
        on "user"
        for each row
    execute function refresh_video_with_user();
    """
   )
]
