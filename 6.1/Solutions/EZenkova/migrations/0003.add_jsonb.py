from yoyo import step


steps = [
   step(
       """
alter table video 
	drop column if exists subtitles;

alter table video 
	add column subtitles jsonb;

UPDATE video
    SET subtitles = (case when title = 'one hour of electronic music' then '[{
                                    "text": "hi",
                                    "language": "english",
                                    "author_of_subtitles": "solovyov"                                  
                                }]'::jsonb

                       when title = 'existentialism in ten minutes' then '[{
                                    "text": "au revoir",
                                    "language": "french",
                                    "author_of_subtitles": "deneuve"                              
                                }]'::jsonb

                       when title = 'philosophy of hedonism' then '[{
                                    "text": "guten tag",
                                    "language": "german",
                                    "author_of_subtitles": "nietzsche"                                
                                }]'::jsonb

                       when title = 'depression in art' then '[{
                                    "text": "finché",
                                    "language": "italian",
                                    "author_of_subtitles": "rossi"                                  
                                }]'::jsonb

                       when title = 'main character syndrome' then '[{
                                    "text": "hola",
                                    "language": "spanish",
                                    "author_of_subtitles": "garcía"                                  
                                }]'::jsonb

 
                end);
                 
drop index if exists video_subtitles;
create index video_subtitles on video using gin (subtitles);      
       """
   )
]
