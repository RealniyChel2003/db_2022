from yoyo import step


steps = [
   step(
       """
alter table "user" 
	add column favorite_videos text[];

UPDATE "user"
    SET favorite_videos = (case when date_of_registration = '01.01.01' then '{anxiety, pain, despair}'::text[]
	                        when date_of_registration = '02.02.02' then '{horror, fear, concern}'::text[]
	                        when date_of_registration = '03.03.03' then '{impotence, sadness, sorrow}'::text[]
	                        when date_of_registration = '04.04.04' then '{melancholy, depression, oppression}'::text[]
	                        when date_of_registration = '05.05.05' then '{enslavement, freedom, death}'::text[]
                       end);


create index user_favorite_videos on "user" using gin (favorite_videos);
    """
   )
]
