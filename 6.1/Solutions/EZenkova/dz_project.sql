drop table if exists video cascade;
drop table if exists "user" cascade;
drop table if exists video_and_user cascade;
drop table if exists comments cascade;

create table video
(
    id bigint generated always as identity primary key,
    title text,  
    general_mood text,
    date_of_publication timestamp,
    views int, 
    subtitles jsonb 
);

create table "user"
(
    id bigint generated always as identity primary key,
    nickname text,
    unread_notifications int,
    date_of_registration text,
    favorite_videos text[] 
);

create table video_and_user
(
    video_id bigint references video not null,
    user_id bigint references "user" not null,
    unique (video_id, user_id)
);

create table comments
(
    id bigint generated always as identity primary key,
    comment text,
    video_id bigint references video not null,
    user_id bigint references "user" not null
);

insert into video (title, general_mood, date_of_publication, views) values   
        ('one hour of electronic music', 'great', '2022-may-20 11:59:59', 1000234),
        ('existentialism in ten minutes', 'disgusting', '2022-april-18 11:59:59', 54321),
        ('philosophy of hedonism', 'fine', '2022-february-28 11:59:59', 123456),
        ('depression in art', 'good', '2022-january-10 11:59:59', 98765),
        ('main character syndrome', 'unusual', '2022-march-15 11:59:59', 677888);

insert into "user" (nickname, unread_notifications, date_of_registration) values   
        ('xxx', 5, '01.01.01'),
        ('duvet', 6, '02.02.02'),
        ('introvert', 7, '03.03.03'),
        ('sad_package', 8, '04.04.04'),
        ('vibe_of_dreams', 9, '05.05.05');
       
insert into video_and_user values
    (1, 1),
    (1, 2),
    (3, 4),
    (3, 5),
    (3, 3),
    (4, 2), 
    (5, 1);
  
insert into comments (comment, video_id, user_id) values
    ('great', 1, 2),
    ('disgusting', 1, 3),
    ('fine', 2, 4),
    ('good', 2, 5),
    ('unusual', 3, 3),
    ('usual', 4, 4);

--индексы ----------------------------------------------------------------

create index video_search_by_title_views_general_mood on video
    using btree (title, views, general_mood);

-- select title, views, general_mood
-- from video
-- where title = 'one hour of electronic music';

---------------------------------------------------------------------------


-- материализованное представление -------------------------------------------------------------------------------

drop materialized view if exists video_with_user;

   
create
materialized view video_with_user as
select "user".id, nickname, video_and_user.user_id as uid, favorite_videos as favvid, title, comment, comments.id as comment_id
from "user" 
join video_and_user on "user".id = video_and_user.user_id
join video on video_and_user.video_id = video.id
join comments on video.id = comments.video_id;

 select *
 from video_with_user;

drop index if exists video_with_user_id;

select * from video_with_user;

create unique index video_with_user_id on video_with_user (id, uid, comment_id);

refresh
materialized view concurrently video_with_user;

-----------------------------------------------------------------------------------------------------------------


--индекс materialized view --------------------------------------------------------------

drop index if exists video_with_user_id;


create index materialized_search_by_id_nickname on video_with_user
    using btree (title, comment); 

 select nickname, favvid, title, comment
 from video_with_user
 where title = 'depression in art' and comment = 'usual';

------------------------------------------------------------------------------------------
