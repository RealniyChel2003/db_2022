from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '6e0d9ac42c70ca82411b41648669e3843c54762dba26a771f4870a84'
salt = 'cb9a9ab9-57dc-4408-8af3-602d0b6a366a'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha3_224((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
