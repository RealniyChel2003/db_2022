from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'5e438fd365209bab2e1b98b7084c9f91420b5d548a602cd61df998fa', 'de326be8e35fc5c91322bc23e9ef28952bf564178ee6259aa2166688'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        md5hash = hashlib.sha3_224(password.encode('utf-8')).hexdigest()

        if md5hash in hashes:
            print(f'password = {password}, hash = {md5hash}')
            hashes.remove(md5hash)

        if len(hashes) == 0:
            break

