from crypt import methods
from flask import Flask, request, jsonify
from psycopg2.extras import RealDictCursor
from redis import Redis
import psycopg2, json, os, logging
from psycopg2.extras import Json
from psycopg2.extensions import AsIs
from deserialization import deserialize_video


app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                           database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'), 
                           password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
pg_conn.autocommit = True


def get_pg_con_rep():
    pg_conn_rep = psycopg2.connect(host=os.getenv('SLAVE_HOST') or '127.0.0.1', port=os.getenv('SLAVE_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn_rep.autocommit = True
    return pg_conn_rep



redis_conn = Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


@app.route('/videos')
def get_video():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'video:offset={offset},limit={limit}'
        redis_video = redis_conn.get(redis_key)

        if redis_video is None:
            cur = pg_conn.cursor()
            query = """
            select video.id, title, video_and_user.user_id, nickname, favorite_videos, comment
            from video 
            join "user" on video.id = "user".id
            join video_and_user on video.id = "user".id
            join comments on video.id = comments.id
            limit %s
            offset %s
            """

            cur.execute(query, (offset, limit))
            rows = cur.fetchall()
            cur.close()
            videos = deserialize_video(rows)

            redis_videos = json.dumps(videos, default=vars, ensure_ascii=False, indent=2)
            redis_conn.set(redis_key, redis_videos, ex=30)

        return redis_videos, 200, {'content-type': 'text/json'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/videos/create', methods=['POST'])
def create_video():
    try:
        body = request.json
        title = body['title']
        general_mood = body['general_mood']
        date_of_publication = body['date_of_publication']
        views = body['views']
        
        cur = pg_conn.cursor()
        query = f"""
        insert into video (title, general_mood, date_of_publication, views)
        values (%s, %s, %s, %s)
        returning date_of_publication
         """

        cur.execute(query, (title, general_mood, date_of_publication, views))
        result = cur.fetchall()
        cur.close()
        return {'message': f'Video {result[0]["title"]} with {result[0]["date_of_publication"]} was created.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/videos/update', methods=['POST'])
def update_video():
    try:
        body = request.json
        title = body['title']
        date_of_publication = body['date_of_publication']

        cur = pg_conn.cursor()
        query = f"""
        update video 
        set title = %s
        where date_of_publication = %s
        returning date_of_publication 
        """

        cur.execute(query, (title, date_of_publication))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'video with title = {title} updated.'}
        else:
            return {'message': f'video with title = {title} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/videos/delete', methods=['DELETE'])
def delete_video():
    try:
        body = request.json
        title = body['title']

        cur = pg_conn.cursor()
        query = f"""
        delete from video
        where title = %s 
        returning title 
         """

        cur.execute(query, (title, ))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'video with title = {title} deleted.'}
        else:
            return {'message': f'video with title = {title} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400

@app.route('/index_search', methods=['POST'])
def get_index_search():
    try:
        body = request.json
        title = body['title']
        views = body['views']
        general_mood = body['general_mood']
        
        cur = pg_conn.cursor()
        query=f"""
            select title, views, general_mood
            from video
            where title = %s and views = %s and general_mood = %s"""


        cur.execute(query, (title, views, general_mood, ))
        affected_rows = cur.fetchall()
        cur.close()

        
        if len(affected_rows):
            return {'message': f'video with title {title} exist.'}
        else:
            return {'message': f'video with title = {title} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400



# connection rep
@app.route('/materialized')
def get_materialized():
    try:

        query=f"""
    select * from video_with_user;"""

        with get_pg_con_rep() as pg_con_rep, pg_con_rep.cursor() as cur:
            cur.execute(query)
            graph = cur.fetchall()
        
        redis_graph = json.dumps(graph, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}
    
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/materialized_index', methods=['MATH-IND'])
def get_materialized_index():
    try:
        body = request.json
        title = body['title']
        comment = body['comment']

        cur = pg_conn.cursor()

        query=f"""
    select nickname, favvid, title, comment
    from video_with_user
    where title = %s and comment = %s;"""


        cur.execute(query, (title, comment,))
        affected_rows = cur.fetchall()
        cur.close()

        
        if len(affected_rows):
            return {'message': f'Video`s title {title} and comment {comment} exist.'}
        else:
            return {'message': f'video with title {title} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/videos/jsonb', methods =['JSON'])
def json_db():
    try:
        cur = pg_conn.cursor()


        body = request.json
        text = body['text']
        query_param = [{"text": text}]
        query=f"""
        select *
        from video
        where subtitles @> %s;
        """

        cur.execute(query, (Json(query_param), ))
        affected_rows = cur.fetchall()
        cur.close()


        if len(affected_rows):
            return {'message': f'Video existed.'}
        else:
            return {'message': f'video not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


@app.route('/videos/array', methods =['ARRAY'])
def array_db():
    try:
        cur = pg_conn.cursor()

        body = request.json
        favorite_videos = body['favorite_videos']
        query=f"""
        select *
        from "user"
        where favorite_videos && array ['%s'];
        """

        cur.execute(query, (AsIs(favorite_videos), ))
        affected_rows = cur.fetchall()
        cur.close()


        if len(affected_rows):
            return {'message': f'Video existed.'}
        else:
            return {'message': f'video not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400


if __name__ == '__main__':
    app.run(port=8000, host='127.0.0.1')
