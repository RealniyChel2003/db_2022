drop table if exists video, "user" cascade;

create table video
(
    id bigint generated always as identity primary key,
    title text,  
    general_mood text,
    date_of_publication timestamp,
    views int, 
    subtitles jsonb 
);

create table "user"
(
    id bigint generated always as identity primary key,
    nickname text,
    unread_notifications int,
    date_of_registration text,
    favorite_videos text[] 
); 

insert into video (id, title, general_mood, date_of_publication, views, subtitles)
values ('one hour of electronic music', 'great', '2022-may-20 11:59:59', '1000234', '[
                                                                                          {
                                                                                            "text": "hi",
                                                                                            "language": "english",
                                                                                            "author_of_subtitles": "solovyov"
                                                                                          }
                                                                                          ]'),
       ('existentialism in ten minutes', 'disgusting', '2022-april-18 11:59:59', '54321', '[
                                                                                          {
                                                                                            "text": "au revoir",
                                                                                            "language": "french",
                                                                                            "author_of_subtitles": "deneuve"
                                                                                          }
                                                                                          ]'),
       ('philosophy of hedonism', 'fine', '2022-february-28 11:59:59', '123456', '[
                                                                                          {
                                                                                            "text": "guten tag",
                                                                                            "language": "german",
                                                                                            "author_of_subtitles": "nietzsche"
                                                                                          }
                                                                                          ]'),
       ('depression in art', 'good', '2022-january-10 11:59:59', '98765', '[
                                                                                          {
                                                                                            "text": "finché",
                                                                                            "language": "italian",
                                                                                            "author_of_subtitles": "rossi"
                                                                                          }
                                                                                          ]'),
       ('main character syndrome', 'unusual', '2022-march-15 11:59:59', '677888', '[
                                                                                          {
                                                                                            "text": "hola",
                                                                                            "language": "spanish",
                                                                                            "author_of_subtitles": "garcía"
                                                                                          }
                                                                                          ]');

insert into "user" (nickname, unread_notifications, date_of_registration, favorite_videos)
values  ('xxx', 5, '01.01.01', '{anxiety, pain, despair}'),
        ('duvet', 6, '02.02.02', '{horror, fear, concern}'),
        ('introvert', 7, '03.03.03', '{impotence, sadness, sorrow}'),
        ('sad_package', 8, '04.04.04', '{melancholy, depression, oppression}'),
        ('vibe_of_dreams', 9, '05.05.05', '{enslavement, freedom, death}');

--jsonb----------------
drop index if exists video_subtitles;
create index video_subtitles on video using gin (subtitles);

set enable_seqscan = false;

-- explain
-- select *
-- from video
-- where family @> '[{"title": "one hour of electronic music"}]';
-----------------------

--массивы--------------
drop index if exists "user"_favorite_videos;
create index "user"_favorite_videos on "user" using gin (favorite_videos);

set enable_seqscan = false;

explain
select *
from "user"
where favorite_videos && array ['{anxiety, pain, despair}'];
-----------------------