# import psycopg2

from typing import List
from psycopg2.extras import RealDictCursor

# from pprint import pprint
from typing import OrderedDict

# connection = psycopg2.connect(database='postgres',
#                         user='postgres',
#                         password='changeme',
#                         host='localhost',
#                         port=999,
#                         cursor_factory=RealDictCursor)

class Video:
    def __init__(self, id: int, title: str):
        self.id = id
        self.title = title
        self.user: list[User] = []
        self.comments: list[Comments] = []
        self.video_to_user: list[Video_to_User] = []

class User:
    def __init__(self, nickname: str, favorite_videos: int):
        self.nickname = nickname
        self.favorite_videos = favorite_videos

class Video_to_User:
    def __init__(self, user_id: int):
        self.user_id = user_id

class Comments:
    def __init__(self, comment: str):
        self.comment = comment

# cursor = connection.cursor()

# query = """
# select video.id, title, video_and_user.user_id, nickname, favorite_videos, comment
# from video 
# join "user" on video.id = "user".id
# join video_and_user on video.id = "user".id
# join comments on video.id = comments.id
# """

# cursor.execute(query)
# rows = cursor.fetchall()

def deserialize_video(rows: list[OrderedDict]):
    video_dict = {}
    user_dict = {}
    vid_to_us_dict = {}
    comments_dict = {}

    for row in rows:
        video_id = row['id']
        video_title = row['title']

        video = None
        if video_title in video_dict:
            video = video_dict[video_title]
        else:
            video = Video(video_id, video_title)
            video_dict[video_title] = video

        user_nickname = row['nickname']
        user_favorite_videos = row['favorite_videos']

        user = None
        if user_nickname in user_dict:
            user = user_dict[user_nickname]
        elif user_nickname is not None:
            user = User(user_nickname, user_favorite_videos)
            user_dict[user_nickname] = user

        us_id = row['user_id']

        video_to_user = None
        if us_id in vid_to_us_dict:
            video_to_user = vid_to_us_dict[us_id]
        elif us_id is not None:
            video_to_user = Video_to_User(us_id)
            vid_to_us_dict[us_id] = video_to_user
        
        comment = row['comment']

        comments = None
        if comment in comments_dict:
            comments = comments_dict[comment]
        elif comment is not None:
            comments = Comments(comment)
            comments_dict[comment] = comments

        if user_nickname is not None:
            if user not in video.user: video.user.append(user)

        if video_to_user is not None:
            if video_to_user not in video.video_to_user: video.video_to_user.append(video_to_user)

        if comment is not None:
          if comments not in video.comments: video.comments.append(comments)

    return list(video_dict.values())

# connection.close()

# print(deserialize_video(rows))