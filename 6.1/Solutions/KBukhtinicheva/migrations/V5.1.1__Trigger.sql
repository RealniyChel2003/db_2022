drop function if exists refresh_view_artists_to_paintings cascade ;
create function refresh_view_artists_to_paintings()
    returns trigger as
$$
begin
    refresh materialized view concurrently view_artists_to_paintings;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_artists_table
    after insert or update or delete
    on artists
    for each row
execute function refresh_view_artists_to_paintings();