create extension pg_cron;
select cron.schedule('refresh_view_artists_to_paintings', '* * * * *',
                     $$ refresh materialized view concurrently view_artists_to_paintings  $$);

