create table if not exists artists
(
    id int generated always as identity primary key ,
    name text not null,
    age int,
    country text,
    period text,
    education text,
    nickname text
);

create table if not exists paintings
(
    id int generated always as identity primary key ,
    title text not null,
    price int,
    style text

);

create table if not exists artists_to_paintings
(
    artists_id int references artists(id),
    paintings_id int references paintings(id),
    primary key(artists_id,paintings_id)
);

create table if not exists reviews
(
    id int generated always as identity primary key ,
    pic_id bigint references paintings,
    comment text not null,
    mark int
);