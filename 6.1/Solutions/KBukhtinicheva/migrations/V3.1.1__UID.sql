begin;
create extension if not exists "uuid-ossp";

alter table artists_to_paintings
    drop constraint artists_to_paintings_artists_id_fkey;
alter table artists_to_paintings
    rename column artists_id to old_artists_id;
alter table artists_to_paintings
    add column artists_id uuid;

alter table artists
    drop constraint artists_pkey;
alter table artists
    rename column id to old_id;
alter table artists
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from artists
            loop
                update artists_to_paintings set artists_id = row.id where old_artists_id = row.old_id;
            end loop;
    end
$$;

drop materialized view if exists view_artists_to_paintings;

alter table artists
    drop column old_id;
alter table artists
    add primary key (id);

alter table artists_to_paintings
    drop column old_artists_id;
alter table artists_to_paintings
    add constraint fk_artists_id foreign key (artists_id) references artists;
alter table artists_to_paintings
    alter column artists_id set not null;
alter table artists_to_paintings
    add constraint uq_artists_id_to_paintings_id unique (artists_id, paintings_id);

alter table artists_to_paintings
    drop constraint artists_to_paintings_paintings_id_fkey;
alter table artists_to_paintings
    rename column paintings_id to old_paintings_id;
alter table artists_to_paintings
    add column paintings_id uuid;

alter table reviews
    drop constraint reviews_pic_id_fkey;
alter table reviews
    rename column pic_id to old_pic_id;
alter table reviews
    add column pic_id uuid;

alter table paintings
    drop constraint paintings_pkey;
alter table paintings
    rename column id to old_id;
alter table paintings
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from paintings
            loop
                update artists_to_paintings set paintings_id = row.id where old_paintings_id = row.old_id;
                update reviews set pic_id = row.id where old_pic_id = row.old_id;
            end loop;
    end
$$;

alter table paintings
    drop column old_id;
alter table paintings
    add primary key (id);

alter table artists_to_paintings
    drop column old_paintings_id;
alter table artists_to_paintings
    add constraint fk_paintings_id foreign key (paintings_id) references paintings;
alter table artists_to_paintings
    alter column paintings_id set not null;

alter table reviews
    drop column old_pic_id;
alter table reviews
    add constraint fk_paintings_id foreign key (pic_id) references paintings;

alter table reviews
    drop constraint reviews_pkey;
alter table reviews
    drop column id;
alter table reviews
    add column id uuid default uuid_generate_v4();
alter table reviews
    add primary key (id);

create
materialized view view_artists_to_paintings as
select artists_id as artist_id, paintings_id as painting_id , name, age, country, period, education, nickname, title, price, style
from artists
  left join artists_to_paintings as atp on artists.id = atp.artists_id
  left join paintings on paintings.id = atp.paintings_id;


drop index if exists view_artists_to_paintings_id;
create unique index view_artists_to_paintings_id on view_artists_to_paintings (artist_id, painting_id);

drop index if exists artists_search_by_education_age_country;
create index artists_search_by_education_age_country on artists
    using btree (education, age, country);

commit;