insert into artists(name, age, country, period, education, nickname)
values ('Mone',46,'France','impressionism','academie_Suisse','ooo'),
       ('Picasso',92,'Spane','Surrialism','academia_Fernando','iiii');

insert into paintings(title, price, style)
values ('city',10000,Null),
       ('sochi',156,Null),
       ('sea',7890,'nature');

insert into artists_to_paintings(artists_id, paintings_id)
values (1,1),
       (1,2),
       (2,3);

insert into reviews(pic_id, comment, mark)
values (1,'good',5),
       (1,'super',34567),
       (2,'bad',0);