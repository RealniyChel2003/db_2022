drop materialized view if exists view_artists_to_paintings;
create
materialized view view_artists_to_paintings as
select artists_id as artist_id, paintings_id as painting_id , name, age, country, period, education, nickname, title, price, style
from artists
  left join artists_to_paintings as atp on artists.id = atp.artists_id
  left join paintings on paintings.id = atp.paintings_id;

  create unique index view_artists_to_paintings_id on view_artists_to_paintings (artist_id, painting_id);