create index search_view_artists_to_paintings_search_by_price_age on view_artists_to_paintings
    using btree (price, age);

create index artists_search_by_education_age_country on artists
    using btree (education, age, country);