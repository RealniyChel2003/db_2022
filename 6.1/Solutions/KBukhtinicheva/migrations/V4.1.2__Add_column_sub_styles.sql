alter table artists add column sub_style text[];

update artists set sub_style=('{модернизм, реализм}')
where artists.name = 'Mone';

update artists set sub_style=('{кубизм, экспрессианизм}')
where artists.name = 'Picasso';

create index artists_sub_direction on artists using gin (sub_style);
