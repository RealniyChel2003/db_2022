alter table artists add column exhibitions jsonb;

update artists set exhibitions=
('[
	{
		"title": "Клод Мане. Магия Воды и Света",
		"date": 2022,
		"place": "Сад культуры и отдыха"
	},
	{
		"title": "Мане- гений импрессионизма",
		"date": 2019,
		"place": "Музей изобразительного искусства"
	}
]')
where artists.name = 'Mone';

update artists set exhibitions =
('[
	{
		"title": "Искусство наций",
		"date": 2023,
		"place": "Музей имени Баумана"
	},
	{
		"title": "Picasso & Abstraction",
		"date": 2001,
		"place": "The Royal Museums of Fine Arts"
	}
]')
where artists.name = 'Picasso';



create index artists_exhibitions on artists using gin (exhibitions);