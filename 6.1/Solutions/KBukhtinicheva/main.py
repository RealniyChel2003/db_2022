from flask import Flask, jsonify, request
import psycopg2
from redis import Redis
import json
import os
from psycopg2.extras import RealDictCursor
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
# pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
#                            database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
#                            password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
# pg_conn.autocommit = True
# redis_conn = Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
#                    password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


def get_pg_connection():
    print(os.getenv('POSTGRES_PORT'))
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn


def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


@app.route('/artists')
def get_artists():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'artists:offset={offset},limit={limit}'
       # redis_artists = redis_conn.get(redis_key)
        with get_redis_connection() as redis_conn:
            redis_artists = redis_conn.get(redis_key)

        if redis_artists is None:
            #cur = pg_conn.cursor()
            query = """
            select *
            from artists
            offset %s
            limit %s
            """

            # cur.execute(query, (offset, limit))
            # artists = cur.fetchall()
            # cur.close()
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (offset,limit))
                artists = cur.fetchall()

            # redis_conn.set(redis_key, json.dumps(artists), ex=30)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, json.dumps(artists), ex=30)
        else:
            artists = json.loads(redis_artists)

        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


# {
#     "name": "matiss",
#     "age":12,
#     "country":"asdasdasd",
#     "period":"asdasdasd",
#     "education":"asdsd",
#     "nickname": "artbomba"
# }


@app.route('/artists/create', methods=['POST'])
def create_artists():
    try:
        body = request.json

        name = body['name']
        age = body['age']
        country = body['country']
        period = body['period']
        education = body['education']
        nickname = body['nickname']

        #cur = pg_conn.cursor()
        query = f"""
        insert into artists (name,age,country,period,education,nickname)
        values (%s, %s, %s, %s, %s, %s)
        returning name,age,country,period,education,nickname
        """

        # cur.execute(query, (name, age, country, period, education, nickname))
        # result = cur.fetchall()
        # cur.close()
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, age, country, period, education,nickname))
            result = cur.fetchall()


        return {'message': f'artist  {result[0]["name"]}  is added'}
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/artists/update', methods=['POST'])
def update_artists():
    try:
        body = request.json
        id = body["id"]
        name = body['name']
        age = body['age']
        country = body['country']
        period = body['period']
        education = body['education']
        nickname = body['nickname']

        #cur = pg_conn.cursor()

        query = f"""
        update artists
        set name = %s , nickname = %s,country = %s
        where id = %s
        returning name, nickname, country, id
        """

        # cur.execute(query, (name, nickname, country, id))
        # affected_rows = cur.fetchall()
        # cur.close()

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (name, nickname, country, id))
            affected_rows = cur.fetchall()


        if len(affected_rows):
            return {'message': f'product {id} changed nickname to {nickname}.'}
        else:
            return {'message': f'product {id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/artists/delete', methods=['DELETE'])
def delete_artists():
    try:
        body = request.json
        id = body['id']

        #cur = pg_conn.cursor()

        query = f"""
        delete from artists
        where id = %s
        returning id
        """

        # cur.execute(query, (id,))
        # affected_rows = cur.fetchall()
        # cur.close()

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (id,))
            affected_rows = cur.fetchall()


        if len(affected_rows):
            return {'message': f'product with id =  {id} deleted.'}
        else:
            return {'message': f'product with id = {id} not found.'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


# @app.route('/artists/indexes')
# def get_indexes():
#     try:
#         artist_id = request.args.get('artist_id')
#         painting_id = request.args.get('painting_id')
#
#         query = """
#         select * from view_artists_to_paintings
#         where artist_id = %s and painting_id = %s;
#         """
#         with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
#             cur.execute(query, (artist_id, painting_id ))
#             reviews = cur.fetchall()
#
#         if len(reviews):
#             return jsonify(reviews)
#         else:
#             return {}, 404
#     except Exception as ex:
#         return {'message': repr(ex)}, 400


# http://127.0.0.1:5000/artists/get_some_paintings?age_max=100&age_min=1&price_max=10000&price_min=10&
@app.route('/artists/get_some_paintings')
def get_indexes():
    try:
        age_max = request.args.get('age_max')
        age_min = request.args.get('age_min')
        price_max = request.args.get('price_max')
        price_min = request.args.get('price_min')
        # redis_key = f'artists:age_max={age_max},age_min={age_min},price_max={price_max},price_min={price_min}'
        # redis_artists = redis_conn.get(redis_key)
        query = """
                    select * from view_artists_to_paintings
                    where age > %s and age < %s
                    and price < %s and price > %s
                    """
        # if redis_artists is None:
        # cur = pg_conn.cursor()

            # cur.execute(query, (age_min, age_max, price_max, price_min))
            # artists = cur.fetchall()
            # cur.close()
            # redis_conn.set(redis_key, json.dumps(artists), ex=30)
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (age_min, age_max, price_max, price_min))
            artists = cur.fetchall()
        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


@app.route('/materialized')
def get_materialized():
    try:
        #cur = pg_conn.cursor()
        query = """
        select * from view_artists_to_paintings;
        """
        # cur.execute(query)
        # artists = cur.fetchall()
        # cur.close()
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query)
                artists = cur.fetchall()


        # redis_conn.set(json.dumps(artists), ex=30)

        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/jsonb', methods =['POST'])
def search_jsonb():
    try:
        body = request.json
        date = body['date']
        query_param = [{"date": date}]

        query=f"""
            select *
            from artists
            where exhibitions @> %s;
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (json.dumps(query_param),))
                artists = cur.fetchall()

        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

@app.route('/array', methods =['POST'])
def search_array():
    try:
        body = request.json
        sub_style = body['sub_style']

        query=f"""
            select *
            from artists
            where sub_style && array [%s];
            """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (sub_style,))
            artists = cur.fetchall()

        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400


if __name__ == '__main__':
    app.run()
