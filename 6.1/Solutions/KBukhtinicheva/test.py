from flask import Flask, jsonify, request
import psycopg2
from redis import Redis
import json
import os
from psycopg2.extras import RealDictCursor
from dotenv import load_dotenv

@app.route('/artists')
def get_artists():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'artists:offset={offset},limit={limit}'
        redis_artists = redis_conn.get(redis_key)
        if redis_artists is None:
            cur = pg_conn.cursor()
            query = """
            select *
            from artists
            offset %s
            limit %s
            """

            cur.execute(query, (offset, limit))
            artists = cur.fetchall()
            cur.close()

            redis_conn.set(redis_key, json.dumps(artists), ex=30)
        else:
            artists = json.loads(redis_artists)

        if len(artists):
            return jsonify(artists)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400