from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'58e4b17dcf2f6bae7710401ca626b0a2e1ce2c8323ea396ae7ea9429b0e2227db6ab87e4d6ec758ef6cee4b2ca651f99', '4f7ec46f96bf369c0054d386677caeadeebdc2cf258796d0d1d78d89ceb831cb90b3164b41a5f892d6238975b48865ea'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)
        sha384hash = hashlib.sha384(password.encode('utf-8')).hexdigest()

        if sha384hash in hashes:
            print(f'password = {password}, hash = {sha384hash}')
            hashes.remove(sha384hash)

        if len(hashes) == 0:
            break

#password = VI, hash = 4f7ec46f96bf369c0054d386677caeadeebdc2cf258796d0d1d78d89ceb831cb90b3164b41a5f892d6238975b48865ea
#password = x6L, hash = 58e4b17dcf2f6bae7710401ca626b0a2e1ce2c8323ea396ae7ea9429b0e2227db6ab87e4d6ec758ef6cee4b2ca651f99