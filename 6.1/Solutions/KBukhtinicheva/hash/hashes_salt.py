from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = 'fcd62c03ae951f69be3f5170f544c55d279ee62d6fff54e5170e5f3349748ac334b6456a73ed276d9230edb391bafe96'
salt = 'ba903bbf-6554-4fe6-b0c5-076411349058'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)

    for combination in combinations:
        password = ''.join(combination)

        if hashlib.sha384((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break

#password = Q7Cz