from unittest import result
from flask import Flask, jsonify, request
import psycopg2
from redis import Redis
import json
import os
from psycopg2.extras import RealDictCursor

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

# pg_conn = psycopg2.connect(database='test_db', user='postgres', password='postgres', host='localhost', port=38746,
# #                            cursor_factory=RealDictCursor)
# pg_conn.autocommit = True

from deserialization import deserialize_users

# redis_conn = Redis(port=26596, password='redis', decode_responses=True)

def get_pg_connection():
    pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    pg_conn.autocommit = True
    return pg_conn


def get_redis_connection():
    return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                 password=os.getenv('REDIS_PASSWORD'), decode_responses=True)

# curl 'http://127.0.0.1:36963/users'

@app.route('/users')
def get_users():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        with get_redis_connection() as redis_conn:
            redis_key = f'users:offset={offset},limit={limit}'
            redis_users = redis_conn.get(redis_key)
        if redis_users is None:
            query = """
            select uid, username, createdate, sex, A2U.articleid, aid, articlename, content, source_, publicdate, cid, commentcontent, commentpublicdate, likes, dislikes
            from Users
                left join Articles2Users A2U on Users.uid = A2U.userid
                left join Articles on A2U.articleid = Articles.aid
                left join Comments on users.uid = Comments.userid and articles.aid = comments.articleid;
            """
            with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query)
                rows = cur.fetchall()

            users = deserialize_users(rows)
            redis_users = json.dumps(users,default=vars,ensure_ascii=False, indent = 2)
            with get_redis_connection() as redis_conn:
                redis_conn.set(redis_key, redis_users, ex=30)
        
        return redis_users, 200, {'content-type': 'text/json'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

# form
# curl --location --request POST 'http://127.0.0.1:36963/article/create' --header 'Content-Type: application/json' --data-raw '{"articlename":"","content":"","source_":"","publicdate":""}'

# example
# curl --location --request POST 'http://127.0.0.1:36963/article/create' --header 'Content-Type: application/json' --data-raw '{"articlename":"new_test_article","content":"new_test_article_text","source_":"habr.com","publicdate":"2022-11-05 22:22:22"}'
@app.route('/article/create', methods=['POST'])
def create_article():
    try:
        body = request.json
        articlename = body['articlename']
        content = body['content']
        source_ = body['source_']
        publicdate = body['publicdate']

        query = f"""
        insert into Articles (articlename, content, source_, publicdate)
        values (%s, %s, %s, %s)
        returning articlename, content, source_, publicdate
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (articlename, content, source_, publicdate))
                result = cur.fetchall()
        cur.close()
        return {'message': f'Article {result[0]["articlename"]} was created'}
    except Exception as ex:
        return {'message': repr(ex)}, 400

# from
# curl --location --request DELETE 'http://127.0.0.1:36963/article/delete' --header 'Content-Type: application/json' --data-raw '{"articlename": ""}'

# example
# curl --location --request DELETE 'http://127.0.0.1:36963/article/delete' --header 'Content-Type: application/json' --data-raw '{"articlename": "new_test_article"}'
@app.route('/article/delete', methods=['DELETE'])
def delete_article():
    try:
        body = request.json
        articlename = body["articlename"]

        query = f"""
        delete from Articles
        where articlename = %s
        returning articlename
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (articlename,))
                result = cur.fetchall()
        cur.close()

        if len(result):
            return {'message': f'article {articlename} deleted.'}
        else:
            return {'message': f'article {articlename} not found'}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

# form
# curl --location --request POST 'http://127.0.0.1:36963/article/update' --header 'Content-Type: application/json' --data-raw '{"articlename": "", "content": ""}'

# example
# curl --location --request POST 'http://127.0.0.1:36963/article/update' --header 'Content-Type: application/json' --data-raw '{"articlename": "new_test_article", "content": "new_test_article_text_updated"}'
@app.route('/article/update', methods=['POST'])
def update_article():
    try:
        body = request.json
        articlename = body['articlename']
        content = body['content']

        query = f"""
        update Articles 
        set content = %s
        where articlename = %s
        returning articlename, content
        """

        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
                cur.execute(query, (content, articlename,))
                result = cur.fetchall()
        cur.close()
        return {'message': f'Article {result[0]["articlename"]} was updated'}
    except Exception as ex:
        return {'message': repr(ex)}, 400


# curl 'http://127.0.0.1:36963/comment/index?commentpublicdate=2022-11-01%2008:00:49&likes=1&dislikes=3'

# commentpublicdate likes dislikes
@app.route('/comment/index')
def get_index():
    try:
        commentpublicdate = request.args.get('commentpublicdate')
        likes = request.args.get('likes')
        dislikes = request.args.get('dislikes')
        query = """
        select uid, username, createdate, sex, aid, articlename, content, source_, publicdate, cid, commentcontent, commentpublicdate, likes, dislikes
        from users_with_all
        where "commentpublicdate" = %s and "likes" = %s  and "dislikes" = %s ;
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (commentpublicdate, likes, dislikes, ))
            result = cur.fetchall()

        if len(result):
            return jsonify(result)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

# api search method by 3 attribute
# request form
# curl 'http://127.0.0.1:36963/article/result?articlename=&source_=&publicdate='

# example
# curl 'http://127.0.0.1:36963/article/result?articlename=how%20to%20install%20windows%20xp&source_=habr.com&publicdate=2022-10-01%2010:00:01'

@app.route('/article/result')
def get_search():
    try:
        articlename = request.args.get('articlename')
        source_ = request.args.get('source_')
        publicdate = request.args.get('publicdate')
        query = """
        select *
        from Articles
        where articlename = %s and source_ = %s and publicdate = %s
        """
        with get_pg_connection() as pg_conn, pg_conn.cursor() as cur:
            cur.execute(query, (articlename, source_, publicdate, ))
            result = cur.fetchall()

        if len(result):
            return jsonify(result)
        else:
            return {}, 404
    except Exception as ex:
        return {'message': repr(ex)}, 400

if __name__ == '__main__':
    app.run()
