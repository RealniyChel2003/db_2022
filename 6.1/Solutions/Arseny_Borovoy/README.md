# Здравствуйте, это домашние задания Борового Арсения 6.1
+ 
+ Чтобы развернуть локально используйте `docker-compose up --build`
+ 
+ Запросить Comment по 3 столбцам из индекса
+ `http://127.0.0.1:36963/comment/index/commentpublicdate=2022-11-01%2008:00:49&likes=1&dislikes=3`
+ Или через curl
+ `curl 'http://127.0.0.1:36963/comment/index?commentpublicdate=2022-11-01%2008:00:49&likes=1&dislikes=3'`