import json, logging, os, psycopg2
from flask import Flask, request
from psycopg2.extras import RealDictCursor
from psycopg2.extras import Json
import psycopg2, json, os, logging
from psycopg2.extras import RealDictCursor
from redis import Redis

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
                           database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                           password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)

pg_conn.autocommit = True

def get_pg_con_rep():
    pg_conn_rep = psycopg2.connect(host=os.getenv('SLAVE_HOST'), port=os.getenv('SLAVE_PORT'),
                               database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
                               password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
    
    pg_conn_rep.autocommit = True
    return pg_conn_rep


redis_conn = Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
                   password=os.getenv('REDIS_PASSWORD'), decode_responses=True)



@app.route('/orders')
def get_reviews():
    try:
        offset = request.args.get('offset')
        limit = request.args.get('limit')
        redis_key = f'orders:offset={offset},limit={limit}'
        redis_orders = redis_conn.get(redis_key)

        if redis_orders is None:
            cur = pg_conn.cursor()
            query = """
            select *
            from orders
            offset %s
            limit %s
            """

            cur.execute(query, (offset, limit))
            orders = cur.fetchall()
            cur.close()

            redis_conn.set(redis_key, json.dumps(orders), ex=30)
        else:
            orders = json.loads(redis_orders)

        if len(orders):
            return jsonify(orders)
        else:
            return {}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400

# create table orders
# (
#     id bigint generated always as identity primary key,
#     body text,
#     destination text,
#     time timestamp,
#     comment text
# );

@app.route('/orders/create', methods=['POST'])
def create_holder():
    try:
        body = request.json
        meal = body['meal']
        destination = body['destination']
        comment = body['comment']


        cur = pg_conn.cursor()
        query = f"""
        insert into orders (meal, destination,comment)
        values (%s, %s, %s)
        returning meal, destination,comment
        """

        cur.execute(query, (meal, destination,comment))
        result = cur.fetchall()
        cur.close()
        return {'message': f'order that is  {result[0]["meal"]}  was created and will be delivered.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/orders/update', methods=['POST'])
def update_orders():
    try:
        body = request.json
        # meal = body['meal']
        destination = body['destination']
        id = body['id']
        # comment = body['comment']


        cur = pg_conn.cursor()
        query = f"""
        update orders
        set destination = %s
        where id = %s
        returning destination, id
        """

        cur.execute(query, (destination, id))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'order {id} updated.'}  
        else:
            return {'message': f'order  {id} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400


@app.route('/orders/delete', methods=['DELETE'])
def delete_orders():
    try:
        body = request.json
        id = body['id']

        cur = pg_conn.cursor()
        query = f"""
        delete from orders
        where id = %s
        returning id
        """

        cur.execute(query, (id,))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'orders  {id} deleted.'}
        else:
            return {'message': f'orders {id} not found.'}, 404
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400

@app.route('/index_search', methods=['POST'])
def get_index_search():
    try:
        body = request.json
        meal = body['meal']
        destination = body['destination']
        courier_id = body['courier_id']

        cur = pg_conn.cursor()

        query = f"""
        select meal, destination, comment, courier_id
        from orders
        where meal = %s or destination = %s or courier_id = %s;
            """           

        cur.execute(query, (meal, destination, courier_id,))
        res = cur.fetchall()
        cur.close()

        if len(res):
            return jsonify(res)
        else:
            return {'message': f'Meal {meal} not found.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400
    

# con rep #####
@app.route('/matherial')
def get_matherial():
    try:
        query = f"""
        select *
        from math_orders;
        """           


        with get_pg_con_rep() as pg_conn_rep, pg_conn_rep.cursor() as cur:
            cur.execute(query)
            orders = cur.fetchall()
            

        redis_graph = json.dumps(orders, default=vars, ensure_ascii=False, indent = 2)

        return redis_graph, 200, {'content-type': 'text/json'}

    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400

@app.route('/math_index', methods=['POST'])
def get_matherial_index():
    try:
        body = request.json
        name = body['name']
        orders_id = body['orders_id']
        

        cur = pg_conn.cursor()

        query = f"""
        select name, meal, comment, orders_id
        from math_orders
        where name = %s and orders_id = %s;
            """           

        cur.execute(query, (name, orders_id,))
        affected_rows = cur.fetchall()
        cur.close()

        if len(affected_rows):
            return {'message': f'{name} has a courier {orders_id}.'}
        else:
            return {'message': f'{name} not found.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400




@app.route('/orders/recipients', methods=['GET'])
def get_recipients():
    try:
        body = request.json
        firstname = body['firstname']
        query_param = [{"firstname": firstname}]

        cur = pg_conn.cursor()

        query = f"""
        select *
        from orders
        where recipients @> %s;
            """           

        cur.execute(query, (Json(query_param), ))
        res = cur.fetchall()
        cur.close()

        if len(res):
            return jsonify(res)
        else:
            return {'message': f'Not found.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400

@app.route('/delivery/partners', methods=['GET'])
def get_partners():
    try:
        body = request.json
        partner = body['partner']
        cur = pg_conn.cursor()

        query = f"""
        select *
        from delivery
        where partners && array [%s];
            """           

        cur.execute(query, (partner, ))
        res = cur.fetchall()
        cur.close()

        if len(res):
            return jsonify(res)
        else:
            return {'message': f'Not found.'}
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': repr(ex)}, 400
