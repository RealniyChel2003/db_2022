from yoyo import step

steps = [
    step(
        """
---Миграция с int на uuid для id таблицы holder:---
--begin;
drop materialized view if exists math_orders;

create extension if not exists "uuid-ossp";

---
alter table delivery_to_courier
    drop constraint delivery_to_courier_courier_id_fkey;

alter table orders
    drop constraint orders_courier_id_fkey;
---

---
alter table delivery_to_courier
    rename column courier_id to old_courier_id;

alter table orders
    rename column courier_id to old_courier_id;
---

---
alter table delivery_to_courier
    add column courier_id uuid;

alter table orders
    add column courier_id uuid;
---

--
alter table courier
    drop constraint courier_pkey;
--
alter table courier
    rename column id to old_id;
--
alter table courier
    add column id uuid default uuid_generate_v4();

---
do
$$
    declare
        row record;
    begin
        for row in select * from courier
            loop
                update delivery_to_courier set courier_id = row.id where old_courier_id = row.old_id;
                update orders set courier_id = row.id where old_courier_id = row.old_id;
            end loop;
    end
$$;


---
alter table courier
    drop column old_id;
---
alter table courier
    add primary key (id);
---

---
alter table delivery_to_courier
    drop column old_courier_id;


----

alter table delivery_to_courier
    add constraint fk_courier_id foreign key (courier_id) references courier;

---
alter table delivery_to_courier
    alter column courier_id set not null;
alter table delivery_to_courier
    add constraint uq_courier_id_to_delivery_id unique (courier_id, delivery_id);

------
alter table orders
    drop column old_courier_id;
----

---
alter table orders
    add constraint fk_courier_id foreign key (courier_id) references courier;
---



-------------
create
    materialized view math_orders as
select delivery.name as delivery_name, courier.name as courier_name, motto, transport, age, sex, experience, delivery_id, courier_id
from delivery
         join delivery_to_courier on delivery_to_courier.delivery_id = delivery.id
    --join delivery on delivery.id = delivery.id;
         join courier on courier.id = courier_id;


create unique index math_orders_id on math_orders (courier_id, delivery_id);

----------------------------------------------------------------

-----------------------
create index materialized_search_by_id_name on math_orders
    using btree (delivery_name, sex, delivery_id);
-----------------------

commit;
        """
    )
]