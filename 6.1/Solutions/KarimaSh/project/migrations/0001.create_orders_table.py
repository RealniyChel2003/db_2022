from yoyo import step

steps = [
    step(
        """
drop table if exists delivery cascade;
drop table if exists courier cascade;
drop table if exists orders cascade;
drop table if exists delivery_to_courier cascade;

create table delivery
(
    id bigint generated always as identity primary key,
    name text,
    motto text

);

create table courier
(
    id bigint generated always as identity primary key,
    name text,
    transport text,
    age int,
    sex text,
    experience text
);

create table orders
(
    id bigint generated always as identity primary key,
    meal text,
    destination text,
    comment text,
    courier_id bigint references courier
);

create table delivery_to_courier
(
    delivery_id int not null references delivery,
    courier_id int not null references courier,
    unique (delivery_id , courier_id)
);

insert into delivery (name,motto) values
('CDEK','СДЭК - верный товарищ, Сквозь бури, лавины, Всегда ты доставишь, Груз хоть до вершины!'),
('Delivery Club','Ваша еда уже в пути!'),
('Додо Пицца','ДОДО последнего кусочка');

insert into courier (name,transport,age,sex,experience) values
('Захаров Лев Саввич', 'car', 19, 'man', '1 year'),
('Исаева Софья Ильинична', 'scooter', 22, 'woman','9 months'),
('Медведев Артём Витальевич', 'bicycle', 31, 'man', '1 months');

insert into orders (meal, destination, comment, courier_id) values
('Посылка из США','Казань', 'Пожалуйста,будьте аккуратнее', 1),
('Суши и вок','Воскресенская 14/3','Положите,пожалуйста,приборы для 4 пермон', 2),
('Пицца','Сириус-Арена','Не кладите халапеньо,у меня на него аллергия', 3);

insert into delivery_to_courier(delivery_id, courier_id)
values (1, 2),
       (2, 1),
       (3, 3);





----------------индекс------------------------------------------
drop index if exists orders_search_by_date_duration_price;

create index orders_search_by_date_duration_price on orders
    using btree (destination, meal , comment);

-- select meal, destination, comment, courier_id
-- from orders
-- -- where meal = 'Пицца' and destination = 'Сириус-Арена' and courier_id = 3;
-- where meal = 'Пицца' or destination = 'Казань' or courier_id = 2;

---- добавить where
----------------------------------------------------------------



--------материиализованное представление------------------------------------------
drop materialized view if exists math_orders;

create
    materialized view math_orders as
select delivery.name as delivery_name, courier.name as courier_name, motto, transport, age, sex, experience, delivery_id, courier_id
from delivery
         join delivery_to_courier on delivery_to_courier.delivery_id = delivery.id
         --join delivery on delivery.id = delivery.id;
         join courier on courier.id = courier_id;


-- присоед доствку

--
-- select *
-- from math_orders;

drop index if exists math_orders_id;

create unique index math_orders_id on math_orders (courier_id, delivery_id);

refresh
    materialized view concurrently math_orders;
----------------------------------------------------------------


-- индекс к материализованному представлению------------------------
drop index if exists materialized_search_by_id_name;

create index materialized_search_by_id_name on math_orders
    using btree (delivery_name, sex, delivery_id);

-- select delivery_id, delivery_name, courier_name, age, sex
-- from math_orders
-- where delivery_name = 'Додо Пицца' and delivery_id = '3';
----------------------------------------------------------------      
        """
    )
]