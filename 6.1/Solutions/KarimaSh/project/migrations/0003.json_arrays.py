from yoyo import step

steps = [
    step(
        """

alter table orders
    drop column if exists recipients;

alter table orders
    add column recipients jsonb;

UPDATE orders
SET recipients = (case when meal = 'Посылка из США' then '[{
                  "firstname": "Сафина",
                  "secondname": "Алина",
                  "date_of_birth": "12.03.2000"
                }]'::jsonb
                 when meal = 'Суши и вок' then '[{
                   "firstname":  "Хамитов",
                   "secondname":  "Амир",
                   "date_of_birth": "01.01.1989"
                 }]'::jsonb
                 when meal = 'Пицца' then '[{
                   "firstname":  "Потапов",
                   "secondname":  "Александр",
                   "date_of_birth": "31.05.1996"
                 },
{
                   "firstname":  "Царь",
                   "secondname":  "Иван",
                   "date_of_birth": "26.07.1991"
                 }]'::jsonb
    end);

create index orders_recipients on orders using gin (recipients);


alter table delivery
    drop column if exists partners;

alter table delivery
    add column partners text[];

UPDATE delivery
SET partners = (case when name = 'CDEK' then '{Газпром-Банк, Русское_Радио, Мвидео}'::text[]
                       when name = 'Delivery Club' then '{Vk, Mail, Сбербанк}'::text[]
                       when name = 'Додо Пицца' then '{Макдональдс, Авиапарк, Интернет}'::text[]
        end);

create index delivery_partners on delivery using gin (partners);



        """
    )
]