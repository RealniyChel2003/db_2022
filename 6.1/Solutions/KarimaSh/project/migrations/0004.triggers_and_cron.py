from yoyo import step

steps = [
    step(
        """
        drop function if exists refresh_math_orders cascade;
        create function refresh_math_orders()
            returns trigger as
        $$
        begin
            refresh materialized view concurrently math_orders;

            return new;
        end;
        $$
            language 'plpgsql';

        create trigger update_delivery_table
            after insert or update or delete
            on delivery
            for each row
        execute function refresh_math_orders();

        ---

        create extension pg_cron;

        -- refresh представления каждую минуту:
        select cron.schedule('refresh_math_orders', '* * * * *',
                     $$ refresh materialized view concurrently math_orders $$);
        """
    )
]