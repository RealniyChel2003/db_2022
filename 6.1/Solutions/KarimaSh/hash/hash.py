from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'a250063dc9f0ded3a922da1efeec0e2992568f7dd2b881909a0f3ae947c8d44f3cb5f17fbe8cf09af4a2349b1cd8c6fea53f381146b51e4d704de260699043d3',
'8b2f540f638b5ba18c592849b087695783e953139c0fcfbf4689dd89f20af4f0b6d742b0052932add0214fde0da25faa42d4d19751ab3c166213f1650621b846'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        # keccak_512hash = hashlib.keccak_512(password.encode('utf-8')).hexdigest()
        keccak_512hash = keccak.new(digest_bits=512)
        keccak_512hash.update(password.encode('utf-8'))
        hash = keccak_512hash.hexdigest()

        if hash in hashes:
            print(f'password = {password}, hash = {hash}')
            hashes.remove(hash)

        if len(hashes) == 0:
            break



# password = 7sx, hash = a250063dc9f0ded3a922da1efeec0e2992568f7dd2b881909a0f3ae947c8d44f3cb5f17fbe8cf09af4a2349b1cd8c6fea53f381146b51e4d704de260699043d3
# password = hFz5, hash = 8b2f540f638b5ba18c592849b087695783e953139c0fcfbf4689dd89f20af4f0b6d742b0052932add0214fde0da25faa42d4d19751ab3c166213f1650621b846
