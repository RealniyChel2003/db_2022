from itertools import product
from Crypto.Hash import keccak

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '9a8849c84b8c37b81ef5acae0ff98e4545f8e6ef2845ab54486243df19fe6ed68f05c2380e456db938502da9b61d5df3f765aa118aea271d4e6533e5ecf78c3d'
salt = 'd9369f2b-e9e1-48e3-953e-0bb3668b4116'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        keccak_512hash = keccak.new(digest_bits=512)
        keccak_512hash.update((password + salt).encode('utf-8'))
        if keccak_512hash.hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
        

#password = 3If+