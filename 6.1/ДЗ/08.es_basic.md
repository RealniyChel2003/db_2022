Материалы:
- [Создание индекса, операции CRUD](https://gitlab.com/golodnyuk.iv/db_2022/-/blob/main/%D0%9C%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B%20%D0%BF%D0%BE%20%D0%BA%D1%83%D1%80%D1%81%D1%83/ElasticSearch/01.%20%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%B8%D0%BD%D0%B4%D0%B5%D0%BA%D1%81%D0%B0%2C%20%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8%20CRUD.md)
- [Типы данных ElasticSearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)
- [Elasticvue](https://elasticvue.com/) - GUI для просмотра содержимого индексов и т.п.

1. Создать индекс соответствующий одной из таблиц проектной работы.
2. Написать запросы на:
   - вставку данных
   - получение одного из документов
   - обновление одного из документов
   - удаление документа
