Для приведенных примеров будут использоваться следующие схема и данные:

```shell
curl --location --request PUT 'localhost:41554/equipment' \
--header 'Content-Type: application/json' \
--data-raw '{
    "settings": {
        "index": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        }
    },
    "mappings": {
        "properties": {
            "title": {
                "type": "keyword"
            },
            "count": {
                "type": "long"
            },
            "type": {
                "type": "keyword"
            },
            "weight": {
                "type": "long"
            },
            "price": {
                "type": "scaled_float",
                "scaling_factor": 100
            },
            "holder_phone": {
                "type": "keyword"
            },
            "sizes": {
                "type": "long"
            },
            "brands": {
                "type": "nested",
                "properties": {
                    "name": {
                        "type": "keyword"
                    },
                    "country": {
                        "type": "keyword"
                    }
                }
            }
        }
    }
}'
```

```shell
curl --location --request POST 'localhost:41554/_bulk' \
--header 'Content-Type: application/json' \
--data-raw '{"index":{"_index":"equipment","_id":"Защитный комплект"}}
{"title":"Защитный комплект","count":8,"type":"Фигурное катание","weight":780,"price":2300,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Термос"}}
{"title":"Термос","count":4,"type":"Поход","weight":240,"price":670,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Коньки"}}
{"title":"Коньки","count":7,"type":"Фигурное катание","weight":920,"price":1990,"holder_phone":"0002","sizes":[36,37,40,41,42]}

{"index":{"_index":"equipment","_id":"Автомат"}}
{"title":"Автомат","count":4,"type":"Страйкбол","weight":3100,"price":13900,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Ледоступы (пары)"}}
{"title":"Ледоступы (пары)","count":6,"type":"Альпинизм","weight":93,"price":599,"holder_phone":"0001","sizes":[41,44]}

{"index":{"_index":"equipment","_id":"Пистолет"}}
{"title":"Пистолет","count":12,"type":"Страйкбол","weight":370,"price":2100,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Стропы"}}
{"title":"Стропы","count":7,"type":"Альпинизм","weight":1800,"price":9900,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Дождевик"}}
{"title":"Дождевик","count":10,"type":"Поход","weight":140,"price":1100,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Дротик"}}
{"title":"Дротик","count":9,"type":"Дартс","weight":8,"price":65,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Маска защитная"}}
{"title":"Маска защитная","count":14,"type":"Страйкбол","weight":550,"price":1900,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Мяч"}}
{"title":"Мяч","count":3,"type":"Настольный теннис","weight":3,"price":70,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Гидрокостюм"}}
{"title":"Гидрокостюм","count":12,"type":"Дайвинг","weight":1500,"price":4490,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Ледоруб"}}
{"title":"Ледоруб","count":8,"type":"Альпинизм","weight":450,"price":7760,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Сноуборд"}}
{"title":"Сноуборд","count":13,"type":"Сноубординг","weight":2560,"price":5780,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Компенсатор"}}
{"title":"Компенсатор","count":11,"type":"Дайвинг","weight":1100,"price":28000,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Ракетка"}}
{"title":"Ракетка","count":2,"type":"Настольный теннис","weight":200,"price":1345,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Нагрудник"}}
{"title":"Нагрудник","count":13,"type":"Страйкбол","weight":2900,"price":5560,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Спальник"}}
{"title":"Спальник","count":5,"type":"Поход","weight":1300,"price":1790,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Мишень"}}
{"title":"Мишень","count":1,"type":"Дартс","weight":4630,"price":3120,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Удочка"}}
{"title":"Удочка","count":2,"type":"Рыбалка","weight":80,"price":1500,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Сетка"}}
{"title":"Сетка","count":1,"type":"Настольный теннис","weight":760,"price":2450,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Компас"}}
{"title":"Компас","count":1,"type":"Поход","weight":50,"price":180,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Ботинки (пары)"}}
{"title":"Ботинки (пары)","count":15,"type":"Сноубординг","weight":980,"price":8700,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Мангал"}}
{"title":"Мангал","count":1,"type":"Поход","weight":23000,"price":7900,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Акваланг"}}
{"title":"Акваланг","count":8,"type":"Дайвинг","weight":9800,"price":31500,"holder_phone":"0003","brands":[{"name":"2K","country":"USA"}]}

{"index":{"_index":"equipment","_id":"Палатка"}}
{"title":"Палатка","count":6,"type":"Поход","weight":2400,"price":3330,"holder_phone":"0003"}

{"index":{"_index":"equipment","_id":"Маска для плавания"}}
{"title":"Маска для плавания","count":12,"type":"Дайвинг","weight":575,"price":1560,"holder_phone":"0001"}

{"index":{"_index":"equipment","_id":"Ласты"}}
{"title":"Ласты","count":9,"type":"Дайвинг","weight":220,"price":2950,"holder_phone":"0002"}

{"index":{"_index":"equipment","_id":"Рюкзак"}}
{"title":"Рюкзак","count":10,"type":"Поход","weight":1000,"price":2900,"holder_phone":"0002","brands":[{"name":"Nike","country":"USA"},{"name":"Alpina","country":"China"}]}
'
```

#### Вывод максимальной и минимальной цены, средней взвешенной цены, общей стоимости и набора часто используемых показателей:

```shell
curl --location --request POST 'localhost:41554/equipment/_search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "aggs": {
        "max_price": {
            "max": {
                "field": "price"
            }
        },
        "min_price": {
            "min": {
                "field": "price"
            }
        },
        "avg_price": {
            "weighted_avg": {
                "value": {
                    "field": "price"
                },
                "weight": {
                    "field": "count"
                }
            }
        },
        "total_price": {
            "sum": {
                "script": "return doc['\''price'\''].value * doc['\''count'\''].value"
            }
        },
        "price_extended_stats": {
            "extended_stats": {
                "field": "price"
            }
        }
    },
    "size": 0
}'
```

"Аналог" в SQL:

```sql
select min(price)                      as min_price,
       max(price),
       sum(count),
       sum(count * price)              as total_price,
       sum(count * price) / sum(count) as weighted_price
from equipment;
```

#### Вывод максимальной и минимальной цены, средней взвешенной цены, общей стоимости с группировкой по типу:

```shell
curl --location --request POST 'localhost:41554/equipment/_search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "aggs": {
        "by_type": {
            "terms": {
                "field": "type"
            },
            "aggs": {
                "max_price": {
                    "max": {
                        "field": "price"
                    }
                },
                "min_price": {
                    "min": {
                        "field": "price"
                    }
                },
                "avg_price": {
                    "weighted_avg": {
                        "value": {
                            "field": "price"
                        },
                        "weight": {
                            "field": "count"
                        }
                    }
                },
                "total_price": {
                    "sum": {
                        "script": "return doc['\''price'\''].value * doc['\''count'\''].value"
                    }
                }
            }
        }
    },
    "size": 0
}'
```

Аналог в SQL:

```sql
select min(price)                      as min_price,
       max(price),
       sum(count),
       sum(count * price)              as total_price,
       sum(count * price) / sum(count) as weighted_price
from equipment
group by type;
```

#### Группировка по диапазону (гистограмма):

```shell
curl --location --request POST 'localhost:41554/equipment/_search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "aggs": {
        "by_price": {
            "histogram": {
                "field": "price",
                "interval": 5000
            },
            "aggs": {
                "total_price": {
                    "sum": {
                        "script": "return doc['\''price'\''].value * doc['\''count'\''].value"
                    }
                }
            }
        }
    },
    "size": 0
}'
```

Аналог в SQL:

```sql
select floor(price / 5000) as group_key, floor(price / 5000) * 5000, (floor(price / 5000) + 1) * 5000, sum(price * count)
from equipment
group by group_key
order by group_key;
```
