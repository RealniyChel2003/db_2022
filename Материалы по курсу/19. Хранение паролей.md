На случай утечки базы данных можно в определённой степени обезопасить пароли пользователей. Для этого вместо "сырых" паролей
нужно хранить результат применения к паролю некоторой хэш-функции. При авторизации к вводимому паролю применяется эта же хэш-функция
и сравнивается с тем значением, что лежит в базе.

Для лучшего понимания как это работает можно решить обратную задачу - узнать какой пароль скрывается за хэшем, находящимся в базе данных.

Дано: алгоритм хэширования `md5`, хэш пароля `e2f7e41f088f6471727b8e10f9873de1`

Код на python для подбора пароля этого хэша:
```python
from itertools import product
import hashlib

# символы, используемые для подбора
alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '38e9c8d131b3e650a006e06b5b60a55c'

found = False
pass_length = 0

while not found:
    pass_length += 1

    # генерация всех возможных комбинаций паролей длинны pass_length
    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        if hashlib.md5(password.encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
```

Алгоритм подбора будет аналогичным и для других хэш-функций, меняется только сама хэш-функция.

Более того, можно проверять сразу все пароли, хранящиеся в базе данных. Для демонстрации хватит и нескольких, но этот
же алгоритм будет работать и на любом другом количестве паролей.

```python
from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hashes = {'38e9c8d131b3e650a006e06b5b60a55c', '13823cd05de9220f77a7258cdefb2bf0', '4ed817499059b6737232e5123ea57fd3'}

pass_length = 0

while len(hashes) > 0:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)
        md5hash = hashlib.md5(password.encode('utf-8')).hexdigest()

        if md5hash in hashes:
            print(f'password = {password}, hash = {md5hash}')
            hashes.remove(md5hash)

        if len(hashes) == 0:
            break
```

Можно воспрепятствовать проверке сразу всех паролей, если при генерации хэша пароля прибавить к нему уникальную строку (эта строка в криптографии называется `salt`). 
Т.е. алгоритм генерации хэша будет выглядеть так: `hash = hash_func(pass + salt)`

Соответственно в базе для каждого пользователя будет хранится уже 2 значения, `hash` и `salt`.

Решение задачи подбора пароля при алгоритме хэширования `md5`, хэше пароля `5d6785f5711a374fd88d678c0dc71de1` и соли `e19014f3-b3a6-404a-8e2f-8b75fc997dee`:
```python
from itertools import product
import hashlib

alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()+'

hash = '5d6785f5711a374fd88d678c0dc71de1'
salt = 'e19014f3-b3a6-404a-8e2f-8b75fc997dee'

found = False
pass_length = 0

while not found:
    pass_length += 1

    combinations = product(alphabet, repeat=pass_length)
    
    for combination in combinations:
        password = ''.join(combination)

        if hashlib.md5((password + salt).encode('utf-8')).hexdigest() == hash:
            print(f'password = {password}')
            found = True
            break
```

Таким образом использование соли НЕ усложняет подбор одного конкретного пароля, но препятствует:
1. При переборе узнать сразу все пароли по имеющимся хэшам. Каждый пароль нужно подбирать отдельно.
2. Использовать rainbow tables - это базы всех сливавшихся когда-либо паролей с посчитанными для них хэшами наиболее популярными хэш-функциями. Т.е. даже если пользователь использует слитый пароль - злоумышленнику все равно придется его подбирать.
