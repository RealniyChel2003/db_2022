create table equipment
(
    id    int generated always as identity primary key,
    title text,
    color text
);
