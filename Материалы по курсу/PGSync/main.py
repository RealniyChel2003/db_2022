from flask import Flask, request
from elasticsearch import Elasticsearch
import os
import json
import logging

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


def get_elastic_connection():
    host = os.getenv('ELASTIC_HOST') or '127.0.0.1'
    port = os.getenv('ELASTIC_PORT')

    return Elasticsearch(hosts=f'http://{host}:{port}')

@app.route('/holders/search_by_equipment')
def search_by_equipment():
    try:
        title = request.args.get('title')

        query = {
            "term": {
                "equipment.title": f"{title}"
            }
        }

        with get_elastic_connection() as es:
            resp = es.search(index='holder', query=query)

        res = resp['hits']

        return res
    except Exception as ex:
        logging.error(repr(ex), exc_info=True)
        return {'message': 'Bad Request'}, 400
