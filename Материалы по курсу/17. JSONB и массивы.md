Тип данных jsonb, пример столбца с массивом объектов:

```sql
drop table if exists employees cascade;
create table employees
(
    id        int generated always as identity,
    education jsonb
);

insert into employees (education)
values ('[
  {
    "name": "СПбГУ",
    "completion_date": 2015,
    "degree": "bachelor"
  },
  {
    "name": "МГУ",
    "completion_date": 2020,
    "degree": "magister"
  }
]'),
       ('[
         {
           "name": "КФУ",
           "completion_date": 2014,
           "degree": "magister"
         },
         {
           "name": "ВШЭ",
           "completion_date": 2019,
           "degree": "phd"
         }
       ]');

-- добавление индекса
create index employee_education on employees using gin (education);

-- поиск по jsonb и проверка того, что индекс действительно работает:
set enable_seqscan = false;

explain
select *
from employees
where education @> '[{"name": "МГУ"}]';

explain
select *
from employees
where education @> '[{"completion_date": 2019}]';

-- Bitmap Heap Scan on employees ...
```

Тип данных массив:

```sql
drop table if exists employees cascade;
create table employees
(
    id      int generated always as identity,
    degrees text[]
);

insert into employees (degrees)
values ('{bachelor, magister}'),
       ('{phd}');

-- добавление индекса
create index employee_degrees on employees using gin (degrees);

-- поиск по массиву и проверка того, что индекс действительно работает
set enable_seqscan = false;

explain
select *
from employees
where degrees && array ['phd'];
```

Так же как и с обычными типами данных, sql код, в котором используется тип json/jsonb, нужно эскейпить,
иначе он будет подвержен инъекциям.

Пример на python того, как это делать правильно:
```python
...
from psycopg2.extras import Json
...

query_param = [{"name": name}]
query = """
select *
from employees
where education @> %s;
"""
cur.execute(query, (Json(query_param), ))
```
